php -c /etc/php/7.2/fpm/php.ini bin/magento maintenance:enable &&
php -c /etc/php/7.2/fpm/php.ini bin/magento cron:remove &&
php -c /etc/php/7.2/fpm/php.ini bin/magento setup:upgrade &&
php -c /etc/php/7.2/fpm/php.ini bin/magento setup:di:compile &&
php -c /etc/php/7.2/fpm/php.ini bin/magento setup:static-content:deploy ru_RU uk_UA en_US --jobs 2 -f &&
php -c /etc/php/7.2/fpm/php.ini bin/magento cron:install &&
php -c /etc/php/7.2/fpm/php.ini bin/magento maintenance:disable
