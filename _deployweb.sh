php -c php.ini bin/magento maintenance:enable &&
php -c php.ini bin/magento cron:remove &&
php -c php.ini bin/magento setup:upgrade &&
php -c php.ini bin/magento setup:di:compile &&
php -c php.ini bin/magento setup:static-content:deploy ru_RU uk_UA en_US --jobs 2 -f &&
php -c php.ini bin/magento cron:install &&
php -c php.ini bin/magento maintenance:disable
