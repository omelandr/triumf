<?php

declare(strict_types = 1);

namespace Triumf\FeedbackForm\Model\System\Config;

use Magento\Framework\Option\ArrayInterface;

/**
 * Class Yesno
 * @package Triumf\FeedbackForm\Model\System\Config
 */
class Yesno implements ArrayInterface
{
    const YES = 1;
    const NO = 0;

    /**
     * @return array
     */
    public function toOptionArray()
    {
        $options = [
            self::YES => __('Yes'),
            self::NO => __('No')
        ];
        return $options;
    }
}
