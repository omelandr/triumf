<?php

declare(strict_types = 1);

namespace Triumf\FeedbackForm\Model\System\Config;

use Magento\Framework\Option\ArrayInterface;

/**
 * Class Status
 * @package Triumf\FeedbackForm\Model\System\Config
 */
class Status implements ArrayInterface
{
    const ENABLED = 1;
    const DISABLED = 2;

    /**
     * @return array
     */
    public function toOptionArray()
    {
        $options = [
            self::ENABLED => __('New'),
            self::DISABLED => __('Called')
        ];
        return $options;
    }

    /**
     * @return array
     */
    public static function getAvailableStatuses()
    {
        return [
            self::ENABLED => __('New')
            , self::DISABLED => __('Called'),
        ];
    }
}
