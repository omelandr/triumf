<?php

declare(strict_types = 1);

namespace Triumf\FeedbackForm\Model\Source;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class IsActive
 * @package Triumf\FeedbackForm\Model\Source
 */
class IsActive implements OptionSourceInterface
{
    /**
     * @var \Triumf\FeedbackForm\Model\FeedbackForm
     */
    protected $feedbackformModel;

    /**
     * IsActive constructor.
     * @param \Triumf\FeedbackForm\Model\FeedbackForm $feedbackformModel
     */
    public function __construct(\Triumf\FeedbackForm\Model\FeedbackForm $feedbackformModel)
    {
        $this->feedbackformModel = $feedbackformModel;
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        $options[] = ['label' => '', 'value' => ''];
        $availableOptions = $this->feedbackformModel->getAvailableStatuses();

        foreach ($availableOptions as $key => $value) {
            $options[] = [
                'label' => $value,
                'value' => $key,
            ];
        }

        return $options;
    }
}