<?php

declare(strict_types = 1);

namespace Triumf\FeedbackForm\Model\Source;

/**
 * Class FeedbackForms
 * @package Triumf\FeedbackForm\Model\Source
 */
class FeedbackForms implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @var \Triumf\FeedbackForm\Model\FeedbackForm
     */
    protected $feedbackformModel;

    /**
     * FeedbackForms constructor.
     * @param \Triumf\FeedbackForm\Model\FeedbackForm $feedbackformModel
     */
    public function __construct(\Triumf\FeedbackForm\Model\FeedbackForm $feedbackformModel)
    {
        $this->feedbackformModel = $feedbackformModel;
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        $options = [];
        $feedbackforms = $this->feedbackformModel->getCollection()
            ->addFieldToFilter('status', '1');
        foreach ($feedbackforms as $feedbackform) {
            $options[] = [
                'label' => $feedbackform->getName(),
                'value' => $feedbackform->getId(),
            ];
        }
        return $options;
    }
}