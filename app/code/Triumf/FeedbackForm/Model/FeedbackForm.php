<?php

declare(strict_types = 1);

namespace Triumf\FeedbackForm\Model;

use Triumf\FeedbackForm\Helper\Data;
use Triumf\FeedbackForm\Model\Resource\FeedbackForm as FeedbackFormResource;
use Triumf\FeedbackForm\Model\Resource\FeedbackForm\Collection;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use Magento\Framework\UrlInterface;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class FeedbackForm
 * @package Triumf\FeedbackForm\Model
 */
class FeedbackForm extends \Magento\Framework\Model\AbstractModel
{
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 2;

    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var UrlInterface
     */
    protected $_url;

    /**
     * @var Data
     */
    protected $_feedbackformHelper;

    /**
     * FeedbackForm constructor.
     * @param Context $context
     * @param Registry $registry
     * @param StoreManagerInterface $storeManager
     * @param FeedbackFormResource|null $resource
     * @param Collection|null $resourceCollection
     * @param UrlInterface $url
     * @param Data $feedbackformHelper
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        StoreManagerInterface $storeManager,
        FeedbackFormResource $resource = null,
        Collection $resourceCollection = null,
        UrlInterface $url,
        Data $feedbackformHelper,
        array $data = []
    )
    {
        $this->_storeManager = $storeManager;
        $this->_url = $url;
        $this->_feedbackformHelper = $feedbackformHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * @return array
     */
    public function getAvailableStatuses()
    {
        return [self::STATUS_ENABLED => __('New'), self::STATUS_DISABLED => __('Called')];
    }
    
    /**
     * @inheritdoc
     */
    protected function _construct()
    {
        parent::_construct();
        $this->_init('Triumf\FeedbackForm\Model\Resource\FeedbackForm');
    }
}
