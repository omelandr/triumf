<?php

declare(strict_types = 1);

namespace Triumf\FeedbackForm\Model\Resource\FeedbackForm;

use \Triumf\FeedbackForm\Model\Resource\AbstractCollection;

/**
 * Class Collection
 * @package Triumf\FeedbackForm\Model\Resource\FeedbackForm
 */
class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'feedbackform_id';
    
    /**
     * @var
     */
    protected $_previewFlag;

    /**
     * @inheritdoc
     */
    protected function _construct()
    {
        $this->_init('Triumf\FeedbackForm\Model\FeedbackForm', 'Triumf\FeedbackForm\Model\Resource\FeedbackForm');
        $this->_map['fields']['feedbackform_id'] = 'main_table.feedbackform_id';
        $this->_map['fields']['store'] = 'store_table.store_id';
    }

    /**
     * @return array
     */
    public function toOptionIdArray()
    {
        $res = [];
        $existingIdentifiers = [];
        foreach ($this as $item) {
            $identifier = $item->getData('url_key');
            $data['value'] = $identifier;
            $data['label'] = $item->getData('name');
            if (in_array($identifier, $existingIdentifiers)) {
                $data['value'] .= '|' . $item->getData('feedbackform_id');
            } else {
                $existingIdentifiers[] = $identifier;
            }
            $res[] = $data;
        }
        return $res;
    }

    /**
     * @param bool $flag
     * @return $this
     */
    public function setFirstStoreFlag($flag = false)
    {
        $this->_previewFlag = $flag;
        return $this;
    }

    /**
     * @param $store
     * @param bool $withAdmin
     * @return $this
     */
    public function addStoreFilter($store, $withAdmin = true)
    {
        return $this;
    }

    /**
     * @return $this
     */
    protected function _afterLoad()
    {
        $this->_previewFlag = false;

        return parent::_afterLoad();
    }

    /**
     * @inheritdoc
     */
    /*protected function _renderFiltersBefore()
    {
        $this->joinStoreRelationTable('triumf_feedbackform_store', 'feedbackform_id');
    } */
}
