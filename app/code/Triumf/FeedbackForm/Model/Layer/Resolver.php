<?php

declare(strict_types = 1);

namespace Triumf\FeedbackForm\Model\Layer;

/**
 * Class Resolver
 * @package Triumf\FeedbackForm\Model\Layer
 */
class Resolver extends \Magento\Catalog\Model\Layer\Resolver
{
    /**
     * @return \Magento\Catalog\Model\Layer|mixed
     */
    public function get()
    {
        if (!isset($this->layer)) {
            $this->layer = $this->objectManager->create($this->layersPool['feedbackform']);
        }
        return $this->layer;
    }
}