<?php

declare(strict_types = 1);

namespace Triumf\FeedbackForm\Model\Layer;

use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Model\Resource;

/**
 * Class FeedbackForm
 * @package Triumf\FeedbackForm\Model\Layer
 */
class FeedbackForm extends \Magento\Catalog\Model\Layer
{
    /**
     * @return array
     */
    public function getProductCollection()
    {
        $feedbackform = $this->getCurrentFeedbackForm();
        if (isset($this->_productCollections[$feedbackform->getId()])) {
            $collection = $this->_productCollections;
        } else {
            $collection = $feedbackform->getProductCollection();
            $this->prepareProductCollection($collection);
            $this->_productCollections[$feedbackform->getId()] = $collection;
        }
        return $collection;
    }

    /**
     * @return mixed
     */
    public function getCurrentFeedbackForm()
    {
        $feedbackform = $this->getData('current_feedbackform');
        if ($feedbackform === null) {
            $feedbackform = $this->registry->registry('current_feedbackform');
            if ($feedbackform) {
                $this->setData('current_feedbackform', $feedbackform);
            }
        }
        return $feedbackform;
    }
}