<?php
namespace Triumf\FeedbackForm\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\App\Request\Http;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Store\Model\StoreManagerInterface;
use Triumf\FeedbackForm\Model\FeedbackForm as ModelFeedbackForm;

class Index extends Action
{
    protected $resultJsonFactory;
    protected $request;
    protected $scopeConfig;
    protected $_storeManager;
    protected $temp_id;
    protected $modelForm;

    const XML_PATH_EMAIL_TEMPLATE_FIELD  = 'feedback/general/request_template_field_id';

    public function __construct(Context $context,
        JsonFactory $JsonFactory,
        Http $request,
        ScopeConfigInterface $scopeConfig,
        TransportBuilder $transportBuilder,
        StoreManagerInterface $storeManager,
        ModelFeedbackForm $modelForm
        
    )
    {
        parent::__construct($context);
        $this->resultJsonFactory = $JsonFactory;
        $this->request = $request;
        $this->scopeConfig = $scopeConfig;
        $this->transportBuilder = $transportBuilder;
        $this->_storeManager = $storeManager;
        $this->modelForm = $modelForm;
    }

    public function getStoreId()
    {
        return $this->_storeManager->getStore()->getId();
    }

    public function getParam($paramName)
    {
        return $this->request->getParam($paramName);
    }

    public function getSalesSender()
    {
        // Customer Support
        $name = $this->scopeConfig->getValue(
            'feedback/general/name',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );

        $email = $this->scopeConfig->getValue(
            'feedback/general/email',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );

        $sender = [
            'name' => $name,
            'email' => $email
        ];

        return $sender;
    }

    public function jsonResult($array)
    {
    	$result = $this->resultJsonFactory->create();
        $result->setData($array);
        return $result;
    }

    public function getTemplateId($xmlPath)
    {
        return $this->scopeConfig->getValue($xmlPath, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function execute()
    {
        $errorMessage = false;

        // Post parameters from form
        $formType  = $this->getParam('formType');

        $name  = $this->getParam('name');
    	$phone = $this->getParam('phone');

        $emailValidator = new \Zend_Validate_EmailAddress();
    	$notEmptyValidator = new \Zend_Validate_NotEmpty();



        if (!$notEmptyValidator->isValid($name) ||
            !$notEmptyValidator->isValid($phone)) {
            foreach ($notEmptyValidator->getMessages() as $message) {
                $errorMessage = $message;
            }
        }

        if ($errorMessage !== false) {
            return $this->jsonResult(['message' => $errorMessage, 'messageType' => 'error']);
        }

        // Get sender name and email from Magento 2 Admin config
        $sender = $this->getSalesSender();

        // URL of product page
        $referer = "";
        if (array_key_exists('HTTP_REFERER', $_SERVER)) {
            $referer = $_SERVER['HTTP_REFERER'];
        }

        $this->request->setPostValue("producturl", $referer);

        // Prepare variable object for send
        $post = $this->request->getPostValue();
        $postObject = new \Magento\Framework\DataObject();
        $postObject->setData($post);

        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;

        if($sender['email']){
            $emailToArray[] = $sender['email'];
        }

        // Send messages

        foreach ($emailToArray as $emailTo) {
            try {
                $transport = $this->transportBuilder
                                  ->setTemplateIdentifier($this->getTemplateId(self::XML_PATH_EMAIL_TEMPLATE_FIELD))
                                  ->setTemplateOptions(
                                      [
                                          'area' => \Magento\Framework\App\Area::AREA_ADMINHTML,
                                          'store' => $this->getStoreId(),
                                      ]
                                  )
                                  ->setTemplateVars(['data' => $postObject])
                                  ->setFrom($sender)
                                  ->addTo($emailTo)
                                  ->getTransport();

                $transport->sendMessage();
                $post['phone_callback'] = $post['phone'];
                $model = $this->modelForm->setData($post);
                $model->save();
            } catch (\Exception $e) {
                $errorMessage = $e->getMessage();
            }
        }

        if ($errorMessage !== false) {
            return $this->jsonResult(['message' => $errorMessage, 'messageType' => 'error']);
        } else {
            return $this->jsonResult(['message' => __('Thanks for call request'), 'messageType' => 'success']);
        }
    }
}
