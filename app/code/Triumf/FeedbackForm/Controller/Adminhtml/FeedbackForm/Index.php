<?php

declare(strict_types = 1);

namespace Triumf\FeedbackForm\Controller\Adminhtml\FeedbackForm;

/**
 * Class Index
 * @package Triumf\FeedbackForm\Controller\Adminhtml\FeedbackForm
 */
class Index extends \Triumf\FeedbackForm\Controller\Adminhtml\FeedbackForm
{
    /**
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Triumf_FeedbackForm::manage_feedbackform');
        $resultPage->getConfig()->getTitle()->prepend(__('Manage Feedback Form'));
        $resultPage->addBreadcrumb(__('Manage Feedback Form'), __('Manage Feedback Form'));
        return $resultPage;
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Triumf_FeedbackForm::manage_feedbackform');
    }
}
