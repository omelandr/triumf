<?php

declare(strict_types = 1);

namespace Triumf\FeedbackForm\Controller\Adminhtml\FeedbackForm;

/**
 * Class Grid
 * @package Triumf\FeedbackForm\Controller\Adminhtml\FeedbackForm
 */
class Grid extends \Triumf\FeedbackForm\Controller\Adminhtml\FeedbackForm
{
    public function execute()
    {
        $this->_view->loadLayout(false);
        $this->_view->renderLayout();
    }
}
