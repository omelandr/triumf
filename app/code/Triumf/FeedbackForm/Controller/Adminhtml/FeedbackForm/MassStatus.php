<?php

declare(strict_types = 1);

namespace Triumf\FeedbackForm\Controller\Adminhtml\FeedbackForm;

use Magento\Backend\App\Action\Context;
use Magento\Framework\Registry;
use Magento\Framework\View\LayoutFactory;
use Magento\Framework\View\Result\PageFactory;
use Triumf\FeedbackForm\Model\FeedbackForm as ModelFeedbackForm;
use Triumf\FeedbackForm\Helper\Data;
use Magento\Framework\App\Response\Http\FileFactory;

/**
 * Class MassStatus
 * @package Triumf\FeedbackForm\Controller\Adminhtml\FeedbackForm
 */
class MassStatus extends \Triumf\FeedbackForm\Controller\Adminhtml\FeedbackForm
{

    /**
     * @var ModelFeedbackForm
     */
    private $modelFeedbackForm;

    /**
     * MassStatus constructor.
     * @param Context $context
     * @param Registry $coreRegistry
     * @param FileFactory $fileFactory
     * @param Data $viewHelper
     * @param LayoutFactory $layoutFactory
     * @param PageFactory $resultPageFactory
     * @param ModelFeedbackForm $modelFeedbackForm
     */
    public function __construct(
        Context $context, 
        Registry $coreRegistry, 
        FileFactory $fileFactory, 
        Data $viewHelper, 
        LayoutFactory $layoutFactory, 
        PageFactory $resultPageFactory,
        ModelFeedbackForm $modelFeedbackForm
    )
    {
        $this->modelFeedbackForm = $modelFeedbackForm;
        parent::__construct($context, $coreRegistry, $fileFactory, $viewHelper, $layoutFactory, $resultPageFactory);
    }

    /**
     * @inheritdoc
     */
    public function execute()
    {
        $feedbackformIds = $this->getRequest()->getParam('feedbackform');
        if (!is_array($feedbackformIds) || empty($feedbackformIds)) {
            $this->messageManager->addError(__('Please select feedbackform(s).'));
        } else {
            try {
                foreach ($feedbackformIds as $id) {
                    $feedbackform = $this->modelFeedbackForm->load($id);
                    $feedbackform->setStatus($this->getRequest()->getParam('status'))->save();
                }
                $this->messageManager->addSuccess(__('Total of %1 feedbackform(s) were changed status.', count($feedbackformIds)));
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Triumf_FeedbackForm::save_feedbackform');
    }
}
