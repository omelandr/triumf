<?php

declare(strict_types = 1);

namespace Triumf\FeedbackForm\Controller\Adminhtml\FeedbackForm;

use Magento\Backend\App\Action\Context;
use Triumf\FeedbackForm\Model\FeedbackForm as ModelFeedbackForm;
/**
 * Class Delete
 * @package Triumf\FeedbackForm\Controller\Adminhtml\FeedbackForm
 */
class Delete extends \Magento\Backend\App\Action
{
    /**
     * @var ModelFeedbackForm
     */
    private $modelFeedbackForm;

    /**
     * Delete constructor.
     * @param Context $context
     * @param ModelFeedbackForm $modelFeedbackForm
     */
    public function __construct(
        Context $context,
        ModelFeedbackForm $modelFeedbackForm
    )
    {
        $this->modelFeedbackForm = $modelFeedbackForm;
        parent::__construct($context);
    }

    /**
     * @return $this
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('feedbackform_id');
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($id) {
            try {
                $model = $this->modelFeedbackForm;
                $model->load($id);
                $model->delete();
                $this->messageManager->addSuccess(__('The feedbackform has been deleted.'));
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
                return $resultRedirect->setPath('*/*/edit', ['feedbackform_id' => $id]);
            }
        }
        $this->messageManager->addError(__('We can\'t find a feedbackform to delete.'));
        return $resultRedirect->setPath('*/*/');
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Triumf_FeedbackForm::delete_feedbackform');
    }
}
