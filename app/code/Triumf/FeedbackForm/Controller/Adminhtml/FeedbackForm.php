<?php

declare(strict_types = 1);

namespace Triumf\FeedbackForm\Controller\Adminhtml;

use Triumf\FeedbackForm\Helper\Data;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\Registry;
use Magento\Framework\View\LayoutFactory;
use Magento\Framework\View\Result\PageFactory;

/**
 * Class FeedbackForm
 * @package Triumf\FeedbackForm\Controller\Adminhtml
 */
abstract class FeedbackForm extends \Magento\Backend\App\Action
{
    /**
     * @var Registry|null
     */
    protected $_coreRegistry = null;

    /**
     * @var LayoutFactory
     */
    protected $layoutFactory;

    /**
     * @var FileFactory
     */
    protected $_fileFactory;

    /**
     * @var Data
     */
    protected $_viewHelper;

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * FeedbackForm constructor.
     * @param Context $context
     * @param Registry $coreRegistry
     * @param FileFactory $fileFactory
     * @param Data $viewHelper
     * @param LayoutFactory $layoutFactory
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        FileFactory $fileFactory,
        Data $viewHelper,
        LayoutFactory $layoutFactory,
        PageFactory $resultPageFactory
    )
    {
        $this->_coreRegistry = $coreRegistry;
        $this->_fileFactory = $fileFactory;
        $this->_viewHelper = $viewHelper;
        $this->layoutFactory = $layoutFactory;
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    /**
     * @return $this
     */
    protected function _initAction()
    {
        $this->_view->loadLayout();
        $this->_setActiveMenu('Triumf_FeedbackForm::manage_feedbackform')->_addBreadcrumb(__('Product FeedbackForm'), __('Product FeedbackForm'))->_addBreadcrumb(__('Manage Feedback Form'), __('Manage Feedback Form'));
        return $this;
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Triumf_FeedbackForm::feedbackform');
    }
}
