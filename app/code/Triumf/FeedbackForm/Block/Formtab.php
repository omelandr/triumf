<?php
namespace Triumf\FeedbackForm\Block;

use Magento\Framework\View\Element\Template;
use Magento\Framework\App\Config\ScopeConfigInterface;

class Formtab extends Template
{
    private $_productCollectionFactory;

    protected $_registry;

    protected $_customerSession;

    protected $scopeConfig;

    public  function __construct (
        Template\Context $context,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Framework\Registry $registry,
        ScopeConfigInterface $scopeConfig,
        \Magento\Customer\Model\Session $customerSession,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_registry = $registry;
        $this->_customerSession = $customerSession;
        $this->scopeConfig = $scopeConfig;
        $this->_productCollectionFactory = $productCollectionFactory;
    }


    public function getProduct()
    {
        return $this->_registry->registry('current_product');
    }
    
    public function getCustomerData(){
        $customerData = array('first_name' => '', 'email' => '', 'phone' => '');
        if ($this->_customerSession->isLoggedIn()) {
           $customer = $this->_customerSession->getCustomer();
            $customerData['first_name'] = $customer->getFirstname();
            $customerData['email'] = $customer->getEmail();
            $billing = $customer->getPrimaryBillingAddress();
            if ($billing) {
                $customerData['phone'] = $billing->getTelephone();
            }
        }
        return $customerData;
    }
    
    public function isActive(){
        $isActive = $this->scopeConfig->getValue(
            'feedback/general/enabled',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        return $isActive;
    }

    public function workDays(){
        $data =  $this->scopeConfig->getValue(
            'feedback/general/days',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );

        $array = [];
        if($data){
            $array = explode(',', $data);
        }
        return $array;
    }

    public function workTimes(){
        $fromTo = $this->scopeConfig->getValue(
            'feedback/general/time_from_to',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        $excludeTime = $this->excludeTimes();
        if(!empty($excludeTime)){
            $excludeTime = explode(',', $excludeTime);
        }
        $timesArray = [];
        if(!empty($fromTo)){
            $fromTo = explode('-', $fromTo);
            $from = (!empty($fromTo['0'])) ? $fromTo['0'] : false;
            $to = (!empty($fromTo['1'])) ? $fromTo['1'] : false;
            
            if($from and $to){
                for ($i = $from; $i <= $to; $i++){
                    if(!in_array($i, $excludeTime)) {
                        $timesArray[] = intval($i) . ":00";
                        $timesArray[] = intval($i) . ":30";
                    }
                }
            }
        }
        return $timesArray;
    }

    public function excludeTimes(){
        $data = $this->scopeConfig->getValue(
            'feedback/general/exclude_time',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        return $data;
    }
    
}