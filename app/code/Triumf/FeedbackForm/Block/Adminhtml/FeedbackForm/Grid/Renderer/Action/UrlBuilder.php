<?php

declare(strict_types = 1);

namespace Triumf\FeedbackForm\Block\Adminhtml\FeedbackForm\Grid\Renderer\Action;

use Magento\Framework\UrlInterface;

/**
 * Class UrlBuilder
 * @package Triumf\FeedbackForm\Block\Adminhtml\FeedbackForm\Grid\Renderer\Action
 */
class UrlBuilder
{
    /**
     * @var UrlInterface
     */
    protected $frontendUrlBuilder;

    /**
     * UrlBuilder constructor.
     * @param UrlInterface $frontendUrlBuilder
     */
    public function __construct(
        UrlInterface $frontendUrlBuilder
    )
    {
        $this->frontendUrlBuilder = $frontendUrlBuilder;
    }

    /**
     * @param $routePath
     * @param $scope
     * @param $store
     * @return string
     */
    public function getUrl($routePath, $scope, $store)
    {
        $this->frontendUrlBuilder->setScope($scope);
        $href = $this->frontendUrlBuilder->getUrl(
            $routePath,
            ['_current' => false, '_query' => '___store=' . $store]
        );
        return $href;
    }
}
