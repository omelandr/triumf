<?php

declare(strict_types = 1);

namespace Triumf\FeedbackForm\Block\Adminhtml;

/**
 * Class FeedbackForm
 * @package Triumf\FeedbackForm\Block\Adminhtml
 */
class FeedbackForm extends \Magento\Backend\Block\Widget\Grid\Container
{
    protected function _construct()
    {
        $this->_controller = 'adminhtml_feedbackform';
        $this->_blockGroup = 'Triumf_FeedbackForm';
        $this->_headerText = __('Manage Feedback Form');
        parent::_construct();
    }
}
