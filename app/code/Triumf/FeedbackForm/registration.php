<?php
/**
 * Copyright © 2016 Triumf. All rights reserved. 
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Triumf_FeedbackForm',
    __DIR__
);
