define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (
        Component,
        rendererList
    ) {
        'use strict';
        rendererList.push(
            {
                type: 'liqpaymagento_liqpay',
                component: 'Triumf_LiqPay/js/view/payment/method-renderer/liqpay'
            }
        );
        /**
         * Add view logic here if needed
         */
        return Component.extend({});
    }
);