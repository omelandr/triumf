1.0.4 (25/09/2019)
=============

**Implemented enhancements:**

**Fixed bugs:**

- Fixed bug with switching language

1.0.3 (25/09/2019)
=============

**Implemented enhancements:**

- Set language for LiqPay widget
- Refactore code style

**Fixed bugs:**

1.0.2 (20/09/2019)
=============

**Implemented enhancements:**

**Fixed bugs:**

- Updated CallBack controller

1.0.1 (20/09/2019)
=============

**Implemented enhancements:**

**Fixed bugs:**

- Fixed bug with message "Invalid Form Key" when processed redirect from LiqPay server to success page.
I was an magento 2.3.x bug.
