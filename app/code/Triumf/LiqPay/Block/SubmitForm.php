<?php

declare(strict_types = 1);

namespace Triumf\LiqPay\Block;

use Triumf\LiqPay\Model\ConfigInterface;
use Triumf\LiqPay\Model\Sdk\LiqPay;
use Magento\Framework\View\Element\BlockInterface;
use Magento\Framework\View\Element\Template;
use Magento\Framework\Exception\LocalizedException;
use Magento\Sales\Api\Data\OrderInterface;

class SubmitForm extends Template implements BlockInterface
{
    /**
     * @var OrderInterface|null
     */
    private $order = null;

    /**
     * @var LiqPay
     */
    private $liqPay;

    /**
     * @var ConfigInterface
     */
    private $config;

    /**
     * SubmitForm constructor.
     *
     * @param Template\Context  $context
     * @param LiqPay            $liqPay
     * @param ConfigInterface   $config
     * @param array             $data
     */
    public function __construct(
        Template\Context $context,
        LiqPay $liqPay,
        ConfigInterface $config,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->liqPay = $liqPay;
        $this->config = $config;

        $this->addData(
            [
                'cache_lifetime' => null
            ]
        );
    }

    /**
     * Get order
     *
     * @return OrderInterface
     * @throws LocalizedException
     */
    public function getOrder(): OrderInterface
    {
        if ($this->order === null) {
            throw new LocalizedException(__('Order is not set'));
        }
        return $this->order;
    }

    /**
     * @param OrderInterface $order
     */
    public function setOrder(OrderInterface $order)
    {
        $this->order = $order;
    }

    /**
     * @return string
     * @throws \Exception
     */
    protected function _toHtml()
    {
        $order = $this->getOrder();
        $html = $this->liqPay->cnb_form(
            [
                'action' => 'pay',
                'amount' => $order->getGrandTotal(),
                'currency' => $order->getOrderCurrencyCode(),
                'description' => $this->getDescription(),
                'order_id' => $order->getIncrementId(),
                'result_url' => $this->_urlBuilder->getUrl('liqpay/checkout/success'),
                'server_url' => $this->_urlBuilder->getUrl('liqpay/checkout/callback')
            ]
        );
        return $html;
    }

    /**
     * Get payment description
     *
     * @return string
     */
    private function getDescription(): string
    {
        $description = $this->config->getLiqPayDescription();
        $params = [
            '{order_id}' => $this->order->getIncrementId(),
        ];
        return strtr($description, $params);
    }
}
