<?php

declare(strict_types = 1);

namespace Triumf\LiqPay\Model;

use Magento\Framework\DB\Transaction;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Model\Service\InvoiceService;
use Triumf\LiqPay\Model\Sdk\LiqPay;
use Triumf\LiqPay\Model\Ui\ConfigProvider;
use Psr\Log\LoggerInterface;

class LiqPayCallback
{
    /**
     * @var OrderInterface
     */
    private $order;

    /**
     * @var LiqPay
     */
    private $liqPay;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var InvoiceService
     */
    private $invoiceService;

    /**
     * @var Transaction
     */
    private $transaction;

    /**
     * @var ConfigInterface
     */
    private $config;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * Callback constructor.
     *
     * @param OrderInterface           $order
     * @param OrderRepositoryInterface $orderRepository
     * @param InvoiceService           $invoiceService
     * @param Transaction              $transaction
     * @param ConfigInterface          $config
     * @param LiqPay                   $liqPay
     * @param LoggerInterface          $logger
     */
    public function __construct(
        OrderInterface $order,
        OrderRepositoryInterface $orderRepository,
        InvoiceService $invoiceService,
        Transaction $transaction,
        ConfigInterface $config,
        LiqPay $liqPay,
        LoggerInterface $logger
    ) {
        $this->order = $order;
        $this->liqPay = $liqPay;
        $this->orderRepository = $orderRepository;
        $this->invoiceService = $invoiceService;
        $this->transaction = $transaction;
        $this->config = $config;
        $this->logger = $logger;
    }

    /**
     * @param array $data - array with returned data
     *
     * @param $receivedSignature
     * @return null
     */
    public function processCallback($data, $receivedSignature)
    {

        $decodedData = $this->liqPay->getDecodedData($data);

        $orderId = $decodedData['order_id'] ?? null;
        $receivedPublicKey = $decodedData['public_key'] ?? null;
        $status = $decodedData['status'] ?? null;
        $amount = $decodedData['amount'] ?? null;
        $currency = $decodedData['currency'] ?? null;
        $transactionId = $decodedData['transaction_id'] ?? null;
        $senderPhone = $decodedData['sender_phone'] ?? null;

        try {
            $order = $this->getRealOrder($status, $orderId);
            if (!($order
                && $order->getId()
                && $this->isOrderLiqPayPayment($order))
            ) {
                return null;
            }

            if (!$this->liqPay->securityOrderCheck($data, $receivedPublicKey, $receivedSignature)) {
                $order->addStatusHistoryComment(__('LiqPay security check failed!'));
                $this->orderRepository->save($order);
                return null;
            }

            $historyMessage = [];
            $state = null;
            switch ($status) {
            case LiqPay::STATUS_SANDBOX:
            case LiqPay::STATUS_WAIT_COMPENSATION:
            case LiqPay::STATUS_SUCCESS:
                if ($order->canInvoice()) {
                    $invoice = $this->invoiceService->prepareInvoice($order);
                    $invoice->register()->pay();
                    $transactionSave = $this->transaction->addObject(
                        $invoice
                    )->addObject(
                        $invoice->getOrder()
                    );
                    $transactionSave->save();
                    if ($status == LiqPay::STATUS_SANDBOX) {
                        $historyMessage[] = __('Invoice #%1 created (sandbox).', $invoice->getIncrementId());
                    } else {
                        $historyMessage[] = __('Invoice #%1 created.', $invoice->getIncrementId());
                    }
                    $state = Order::STATE_PROCESSING;
                } else {
                    $historyMessage[] = __('Error during creation of invoice.');
                }
                if ($senderPhone) {
                    $historyMessage[] = __('Sender phone: %1.', $senderPhone);
                }
                if ($amount) {
                    $historyMessage[] = __('Amount: %1.', $amount);
                }
                if ($currency) {
                    $historyMessage[] = __('Currency: %1.', $currency);
                }
                break;
            case LiqPay::STATUS_FAILURE:
                $state = Order::STATE_CANCELED;
                $historyMessage[] = __('Liqpay error.');
                break;
            case LiqPay::STATUS_ERROR:
                $state = Order::STATE_CANCELED;
                $historyMessage[] = __('Liqpay error.');
                break;
            case LiqPay::STATUS_WAIT_SECURE:
                $state = Order::STATE_PROCESSING;
                $historyMessage[] = __('Waiting for verification from the Liqpay side.');
                break;
            case LiqPay::STATUS_WAIT_ACCEPT:
                $state = Order::STATE_PROCESSING;
                $historyMessage[] = __('Waiting for accepting from the buyer side.');
                break;
            case LiqPay::STATUS_WAIT_CARD:
                $state = Order::STATE_PROCESSING;
                $historyMessage[] = __('Waiting for setting refund card number into your Liqpay shop.');
                break;
            default:
                $historyMessage[] = __('Unexpected status from LiqPay server: %1', $status);
                break;
            }

            if ($transactionId) {
                $historyMessage[] = __('LiqPay transaction id %1.', $transactionId);
            }

            if (count($historyMessage)) {
                $order->addStatusHistoryComment(implode(' ', $historyMessage))
                    ->setIsCustomerNotified(true);
            }

            if ($state) {
                $order->setState($state);
                $order->setStatus($state);
                $order->save();
            }
            $this->orderRepository->save($order);
        } catch (\Exception $e) {
            $this->logger->critical($e);
        }
        return null;
    }

    /**
     * @param  $order
     * @return bool
     */
    private function isOrderLiqPayPayment($order): bool
    {
        return ($order->getPayment()->getMethod() === ConfigProvider::CODE) ? true : false;
    }

    /**
     * Get order
     *
     * @param  $status
     * @param  $orderId
     * @return mixed
     */
    private function getRealOrder($status, $orderId)
    {
        if ($status === LiqPay::STATUS_SANDBOX) {
            $testOrderSuffix = $this->config->getTestOrderSuffix();
            if (!empty($testOrderSuffix)) {
                $testOrderSuffix = LiqPay::TEST_MODE_SURFIX_DELIM . $testOrderSuffix;
                if (strlen($testOrderSuffix) < strlen($orderId)
                    && substr($orderId, -strlen($testOrderSuffix)) == $testOrderSuffix
                ) {
                    $orderId = substr($orderId, 0, strlen($orderId) - strlen($testOrderSuffix));
                }
            }
        }
        return $this->order->loadByIncrementId($orderId);
    }
}
