<?php

declare(strict_types = 1);

namespace Triumf\LiqPay\Model\Ui;

use Triumf\LiqPay\Model\ConfigInterface;
use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Framework\Session\SessionManagerInterface;

/**
 * Class ConfigProvider
 */
class ConfigProvider implements ConfigProviderInterface
{
    const CODE = 'liqpaymagento_liqpay';

    /**
     * @var ConfigInterface
     */
    private $config;

    /**
     * @var SessionManagerInterface
     */
    private $session;

    /**
     * Constructor
     *
     * @param ConfigInterface $config
     * @param SessionManagerInterface $session
     */
    public function __construct(
        ConfigInterface $config,
        SessionManagerInterface $session
    ) {
        $this->config = $config;
        $this->session = $session;
    }

    /**
     * Retrieve assoc array of checkout configuration
     *
     * @return array
     */
    public function getConfig()
    {
        return [
            'payment' => [
                self::CODE => [
                    'isActive' => $this->config->isEnabled()
                ]
            ],
        ];
    }
}
