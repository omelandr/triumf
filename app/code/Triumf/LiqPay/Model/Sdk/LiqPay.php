<?php

declare(strict_types = 1);

namespace Triumf\LiqPay\Model\Sdk;

use Triumf\LiqPay\Model\ConfigInterface;
use Magento\Framework\Locale\ResolverInterface;
use Magento\Framework\Serialize\SerializerInterface;

/**
 * Extends official LiqPay Sdk
 */
class LiqPay extends \Triumf\LiqPay\Model\LiqPay
{
    const VERSION = '3';
    const TEST_MODE_SURFIX_DELIM = '-';

    /**#@+
     * List of statuses
     */
    const STATUS_SUCCESS           = 'success';
    const STATUS_WAIT_COMPENSATION = 'wait_compensation';
    const STATUS_PROCESSING  = 'processing';
    const STATUS_FAILURE     = 'failure';
    const STATUS_ERROR       = 'error';
    const STATUS_WAIT_SECURE = 'wait_secure';
    const STATUS_WAIT_ACCEPT = 'wait_accept';
    const STATUS_WAIT_CARD   = 'wait_card';
    const STATUS_SANDBOX     = 'sandbox';
    /**#@-*/

    /**
     * @var ConfigInterface
     */
    private $config;

    /**
     * @var ResolverInterface
     */
    private $langResolver;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * LiqPay constructor.
     *
     * @param ConfigInterface $config
     * @param ResolverInterface $resolver
     * @param SerializerInterface $serializer
     */
    public function __construct(
        ConfigInterface $config,
        ResolverInterface $resolver,
        SerializerInterface $serializer
    ) {
        $this->serializer = $serializer;
        $this->config = $config;
        $this->langResolver = $resolver;
        if ($this->config->isEnabled()) {
            parent::__construct(
                $this->config->getPublicKey(),
                $this->config->getPrivateKey()
            );
        }
    }

    /**
     * Prepare params
     *
     * @param $params
     * @return array
     */
    protected function prepareParams($params): array
    {
        if (!isset($params['sandbox'])) {
            $params['sandbox'] = (int)$this->config->isTestMode();
        }
        if (!isset($params['version'])) {
            $params['version'] = static::VERSION;
        }
        if (isset($params['order_id']) && $this->config->isTestMode()) {
            $suffix = $this->config->getTestOrderSuffix();
            if (!empty($suffix)) {
                $params['order_id'] .= self::TEST_MODE_SURFIX_DELIM . $suffix;
            }
        }
        if (!isset($params['language'])) {
            $params['language'] = $this->getLanguage();
        }
        return $params;
    }

    /**
     * Getting list of supported currencies
     *
     * @return array
     */
    public function getSupportedCurrencies()
    {
        return $this->_supportedCurrencies;
    }

    /**
     * Init API SDK module
     *
     * @param string $path
     * @param array $params
     * @param int $timeout
     * @return string
     */
    public function api($path, $params = [], $timeout = 5)
    {
        $params = $this->prepareParams($params);
        return parent::api($path, $params, $timeout);
    }

    /**
     * Prepare form
     *
     * @param array $params
     * @return string
     */
    public function cnb_form($params): string
    {
        $params = $this->prepareParams($params);
        $form = parent::cnb_form($params);
        $form = $this->updateLanguage($form, $params);

        return $form;
    }

    /**
     * Decode data
     *
     * @param $data
     * @return array|bool|float|int|string|null
     */
    public function getDecodedData($data)
    {
        return $this->serializer->unserialize(base64_decode($data), true, 1024);
    }

    /**
     * Check is order has correct public / private key
     *
     * @param $data
     * @param $receivedPublicKey
     * @param $receivedSignature
     *
     * @return bool
     */
    public function securityOrderCheck(
        $data,
        $receivedPublicKey,
        $receivedSignature
    ): bool {
        if ($this->config->isSecurityCheck()) {
            $publicKey = $this->config->getPublicKey();
            if ($publicKey !== $receivedPublicKey) {
                return false;
            }
            return $this->checkSignature($receivedSignature, $data);
        } else {
            return true;
        }
    }

    /**
     * Checking signature
     *
     * @param $signature
     * @param $data
     * @return bool
     */
    public function checkSignature($signature, $data): bool
    {
        $privateKey = $this->config->getPrivateKey();
        $generatedSignature = base64_encode(
            sha1($privateKey . $data . $privateKey, true)
        );
        return $signature == $generatedSignature;
    }

    /**
     * Change language in formed string
     *
     * @param $form
     * @param $params
     * @return mixed
     */
    private function updateLanguage($form, $params): string
    {
        if (isset($params['language'])) {
            if ($params['language'] === 'uk') {
                $form = str_replace(
                    'p1ru.r',
                    'p1' . $params['language'] . '.r',
                    $form
                );
            }
        }
        return $form;
    }

    /**
     * Get language
     *
     * @return string
     */
    private function getLanguage(): string
    {
        $locale = $this->langResolver->getLocale();
        switch ($locale) {
            case ConfigInterface::CONFIG_RU_LOCALE_CODE:
                return 'ru';
                break;
            case ConfigInterface::CONFIG_UA_LOCALE_CODE:
                return 'uk';
                break;
            default:
                return 'en';
        }
    }
}
