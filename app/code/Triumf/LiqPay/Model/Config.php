<?php

declare(strict_types = 1);

namespace Triumf\LiqPay\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\Store;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Store\Model\ScopeInterface;

/**
 * LiqPay module configurations
 */
class Config implements ConfigInterface
{
    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * Config constructor.
     * @param ScopeConfigInterface $scopeConfig
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        StoreManagerInterface $storeManager
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
    }

    /**
     * {@inheritdoc}
     */
    public function isEnabled(): bool
    {
        $status = $this->scopeConfig->isSetFlag(
            self::XML_PATH_IS_ENABLED,
            ScopeInterface::SCOPE_STORE,
            $this->getStoreId()
        );

        return ($status && $this->getPublicKey() && $this->getPrivateKey())
            ? true : false;
    }

    /**
     * {@inheritdoc}
     */
    public function isTestMode(): bool
    {
        return $this->scopeConfig->isSetFlag(
            self::XML_PATH_TEST_MODE,
            ScopeInterface::SCOPE_STORE,
            $this->getStoreId()
        );
    }

    /**
     * {@inheritdoc}
     */
    public function isSecurityCheck(): bool
    {
        return $this->scopeConfig->isSetFlag(
            self::XML_PATH_CALLBACK_SECURITY_CHECK,
            ScopeInterface::SCOPE_STORE,
            $this->getStoreId()
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getPublicKey(): ?string
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_PUBLIC_KEY,
            ScopeInterface::SCOPE_STORE,
            $this->getStoreId()
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getPrivateKey(): ?string
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_PRIVATE_KEY,
            ScopeInterface::SCOPE_STORE,
            $this->getStoreId()
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getTestOrderSuffix(): string
    {
        $value = $this->scopeConfig->getValue(
            self::XML_PATH_TEST_ORDER_SURFIX,
            ScopeInterface::SCOPE_STORE,
            $this->getStoreId()
        );
        return ($value) ?? '';
    }

    /**
     * {@inheritdoc}
     */
    public function getLiqPayDescription(): string
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_DESCRIPTION,
            ScopeInterface::SCOPE_STORE,
            $this->getStoreId()
        );
    }

    /**
     * @return int
     */
    private function getStoreId(): int
    {
        try {
            return (int)$this->storeManager->getStore()->getId();
        } catch (NoSuchEntityException $e) {
            return (int)Store::DEFAULT_STORE_ID;
        }
    }
}
