<?php

declare(strict_types = 1);

namespace Triumf\LiqPay\Model;

/**
 * Interface ConfigInterface
 * @package Triumf\LiqPay\Model
 */
interface ConfigInterface
{
    /**#@+
     * Xml path of payment method configs
     */
    const XML_PATH_IS_ENABLED  = 'payment/liqpaymagento_liqpay/active';
    const XML_PATH_PUBLIC_KEY  = 'payment/liqpaymagento_liqpay/public_key';
    const XML_PATH_PRIVATE_KEY = 'payment/liqpaymagento_liqpay/private_key';
    const XML_PATH_TEST_MODE = 'payment/liqpaymagento_liqpay/sandbox';
    const XML_PATH_TEST_ORDER_SURFIX = 'payment/liqpaymagento_liqpay/sandbox_order_suffix';
    const XML_PATH_DESCRIPTION = 'payment/liqpaymagento_liqpay/description';
    const XML_PATH_CALLBACK_SECURITY_CHECK = 'payment/liqpaymagento_liqpay/security_check';
    /**#@-*/

    /**#@+
     * Localization code constants
     */
    const CONFIG_UA_LOCALE_CODE = "uk_UA";
    const CONFIG_RU_LOCALE_CODE = "ru_RU";
    /**#@-*/

    /**
     * Is enabled
     *
     * @return bool
     */
    public function isEnabled(): bool;

    /**
     * Check is test mode
     *
     * @return bool
     */
    public function isTestMode(): bool;

    /**
     * Check is security
     *
     * @return bool
     */
    public function isSecurityCheck(): bool;

    /**
     * Get public key
     *
     * @return string|null
     */
    public function getPublicKey(): ?string;

    /**
     * Get private key
     *
     * @return string|null
     */
    public function getPrivateKey(): ?string;

    /**
     * Get suffix of test orders
     *
     * @return string
     */
    public function getTestOrderSuffix(): string;

    /**
     * Get description for order
     *
     * @return string
     */
    public function getLiqPayDescription(): string;
}
