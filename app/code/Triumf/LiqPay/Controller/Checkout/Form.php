<?php

declare(strict_types = 1);

namespace Triumf\LiqPay\Controller\Checkout;

use Triumf\LiqPay\Block\SubmitForm;
use Triumf\LiqPay\Model\ConfigInterface;
use Triumf\LiqPay\Model\Ui\ConfigProvider;
use Magento\Checkout\Model\Session;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\View\LayoutFactory;
use Magento\Sales\Api\Data\OrderInterface;

/**
 * Class Form
 *
 * @package Triumf\LiqPay\Controller\Checkout
 */
class Form extends Action implements HttpPostActionInterface
{
    /**
     * @var Session
     */
    private $checkoutSession;

    /**
     * @var ConfigInterface
     */
    private $config;

    /**
     * @var LayoutFactory
     */
    private $layoutFactory;

    /**
     * Form constructor.
     *
     * @param Context         $context         - context instance
     * @param Session         $checkoutSession
     * @param ConfigInterface $config
     * @param LayoutFactory   $layoutFactory
     */
    public function __construct(
        Context $context,
        Session $checkoutSession,
        ConfigInterface $config,
        LayoutFactory $layoutFactory
    ) {
        parent::__construct($context);
        $this->checkoutSession = $checkoutSession;
        $this->config = $config;
        $this->layoutFactory = $layoutFactory;
    }

    /**
     * Dispatch request
     *
     * @return ResultInterface
     */
    public function execute(): ResultInterface
    {
        if (!$this->getRequest()->isPost()) {
            $data = [
                'status' => 'error'
            ];
        } else {
            try {
                if (!$this->config->isEnabled()) {
                    throw new \Exception(__('Payment is not allowed.'));
                }
                $data = $this->formLiqPayForm();
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage(
                    $e,
                    __('Something went wrong, please try again later')
                );
                $this->getCheckoutSession()->restoreQuote();
                $data = [
                    'status' => 'error',
                    'redirect' => $this->_url->getUrl('checkout/cart'),
                ];
            }
        }
        $result = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        return $result->setData($data);
    }

    /**
     * Form liq pay form
     *
     * @return array
     * @throws LocalizedException
     */
    private function formLiqPayForm(): array
    {
        $order = $this->getCheckoutSession()->getLastRealOrder();
        if (!($order && $order->getId())) {
            throw new LocalizedException(__('Order not found'));
        }

        if ($this->isOrderLiqPayPayment($order)) {
            $formBlock = $this->layoutFactory
                ->create()
                ->createBlock(SubmitForm::class);
            $formBlock->setOrder($order);
            $data = [
                'status' => 'success',
                'content' => $formBlock->toHtml(),
            ];
        } else {
            throw new LocalizedException(
                __('Order payment method is not a LiqPay payment method')
            );
        }
        return $data;
    }

    /**
     * Check is order pay payment
     *
     * @param OrderInterface $order - order
     *
     * @return bool
     */
    private function isOrderLiqPayPayment(OrderInterface $order): bool
    {
        return ($order->getPayment()->getMethod() === ConfigProvider::CODE)
            ? true : false;
    }

    /**
     * Return checkout session object
     *
     * @return Session
     */
    private function getCheckoutSession(): Session
    {
        return $this->checkoutSession;
    }
}
