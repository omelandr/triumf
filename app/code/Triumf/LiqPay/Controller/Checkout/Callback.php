<?php

declare(strict_types = 1);

namespace Triumf\LiqPay\Controller\Checkout;

use Triumf\LiqPay\Model\ConfigInterface;
use Triumf\LiqPay\Model\LiqPayCallback;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\App\Request\InvalidRequestException;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Psr\Log\LoggerInterface;

class Callback extends Action implements CsrfAwareActionInterface
{
    /**
     * @var ConfigInterface
     */
    private $config;

    /**
     * @var LiqPayCallback
     */
    private $liqPayCallback;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * Callback constructor.
     *
     * @param Context         $context
     * @param ConfigInterface $config
     * @param LiqPayCallback  $liqPayCallback
     * @param LoggerInterface $logger
     */
    public function __construct(
        Context $context,
        ConfigInterface $config,
        LiqPayCallback $liqPayCallback,
        LoggerInterface $logger
    ) {
        parent::__construct($context);
        $this->config = $config;
        $this->logger = $logger;
        $this->liqPayCallback = $liqPayCallback;
    }

    /**
     * @param  RequestInterface $request
     * @return InvalidRequestException|null
     */
    public function createCsrfValidationException(
        RequestInterface $request
    ): ?InvalidRequestException {
        return null;
    }

    /**
     * @param RequestInterface $request
     *
     * @return bool
     */
    public function validateForCsrf(RequestInterface $request): bool
    {
        return true;
    }

    /**
     * Dispatch request
     *
     * @return ResultInterface
     */
    public function execute(): ResultInterface
    {
        $post = $this->getRequest()->getParams();
        $result = $this->resultFactory->create(ResultFactory::TYPE_JSON);

        if (!(isset($post['data']) && isset($post['signature']))) {
            $this->logger->error(
                __(
                    ' In the response from LiqPay server '
                    . 'there are no POST parameters "data"'
                    . ' and "signature"'
                )
            );
        } else {
            $this->liqPayCallback->processCallback($post['data'], $post['signature']);
        }
        return $result->setData([]);
    }
}
