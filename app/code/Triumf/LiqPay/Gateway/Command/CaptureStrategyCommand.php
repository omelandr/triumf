<?php

declare(strict_types = 1);

namespace Triumf\LiqPay\Gateway\Command;

use Magento\Payment\Gateway\Command\CommandPoolInterface;
use Magento\Payment\Gateway\CommandInterface;
use Magento\Payment\Gateway\Helper\ContextHelper;

class CaptureStrategyCommand implements CommandInterface
{
    /**
     * Authorize and capture command
     */
    const SALE = 'sale';

    /**
     * @var CommandPoolInterface
     */
    private $commandPool;

    /**
     * CaptureStrategyCommand constructor.
     * @param CommandPoolInterface $commandPool
     */
    public function __construct(
        CommandPoolInterface $commandPool
    ) {
        $this->commandPool = $commandPool;
    }

    /**
     * @inheritdoc
     */
    public function execute(array $commandSubject)
    {
        $command = $this->getCommand($commandSubject);
        $this->commandPool->get($command)->execute($commandSubject);
    }

    /**
     * Get execution command name.
     *
     * @param $commandSubject
     * @return string
     * @throws \Exception
     */
    private function getCommand($commandSubject)
    {
        $payment = $commandSubject['payment']->getPayment();
        try {
            $payment->setTransactionId('liqpay-' . $commandSubject['payment']->getOrder()->getId())->setIsTransactionClosed(0);
        } catch (\Exception $e) {
            throw new \Exception(__('Payment capturing error.'));
        }
        ContextHelper::assertOrderPayment($payment);

        return self::SALE;
    }
}
