<?php

declare(strict_types = 1);

namespace Triumf\LiqPay\Gateway\Validator;

use Triumf\LiqPay\Model\Sdk\LiqPay;
use Magento\Payment\Gateway\Validator\AbstractValidator;
use Magento\Payment\Gateway\Validator\ResultInterfaceFactory;

class CurrencyValidator extends AbstractValidator
{
    /**
     * @var LiqPay
     */
    private $liqPay;

    /**
     * CurrencyValidator constructor.
     * @param ResultInterfaceFactory $resultFactory
     * @param LiqPay $liqPay
     */
    public function __construct(
        ResultInterfaceFactory $resultFactory,
        LiqPay $liqPay
    ) {
        $this->liqPay = $liqPay;
        parent::__construct($resultFactory);
    }

    /**
     * @param array $validationSubject
     * @return \Magento\Payment\Gateway\Validator\ResultInterface
     */
    public function validate(array $validationSubject)
    {
        $isValid = true;
        $supportedCurrencies = $this->liqPay->getSupportedCurrencies();

        if (!in_array($validationSubject['currency'], $supportedCurrencies)) {
            $isValid = false;
        }
        return $this->createResult($isValid);
    }
}
