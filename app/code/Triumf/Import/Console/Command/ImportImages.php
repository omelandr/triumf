<?php

namespace Triumf\Import\Console\Command;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\State;
use Magento\Framework\Exception\LocalizedException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ImportImages
 *
 * @package Triumf\Import\Console\Command
 */
class ImportImages extends Command
{
    /**
     * @var ScopeConfigInterface
     */
    private $_config;

    /**
     * @var \Triumf\Import\Model\ImageUpdater
     */
    private $import;

    /**
     * @var State
     */
    private $state;

    /**
     * Import constructor.
     * @param ScopeConfigInterface $config
     * @param \Triumf\Import\Model\ImageUpdater $import
     * @param State $state
     */
    public function __construct(
        ScopeConfigInterface $config,
        \Triumf\Import\Model\ImageUpdater $import,
        State $state
    ) {
        $this->_config = $config;
        $this->import = $import;
        $this->state = $state;
        parent::__construct();
    }

    /**
     * @inheritDoc
     */
    protected function configure()
    {
        $this->setName('triumf:images:import');
        parent::configure();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return null|int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->state->setAreaCode(\Magento\Framework\App\Area::AREA_ADMINHTML);
        } catch (LocalizedException $exception) {

        }
        $output->write('Start Import!');
        $this->import->run();
        $output->write('Finish Import!');
    }
}
