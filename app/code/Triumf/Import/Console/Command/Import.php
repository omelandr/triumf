<?php

namespace Triumf\Import\Console\Command;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class Import
 *
 * @package Triumf\Import\Console\Command
 */
class Import extends Command
{
    /**
     * @var ScopeConfigInterface
     */
    private $_config;

    /**
     * @var \Triumf\Import\Model\Import
     */
    private $import;

    /**
     * Import constructor.
     * @param ScopeConfigInterface $config
     * @param \Triumf\Import\Model\Import $import
     */
    public function __construct(
        ScopeConfigInterface $config,
        \Triumf\Import\Model\Import $import
    ) {
        $this->_config = $config;
        $this->import = $import;
        parent::__construct();
    }

    /**
     * @inheritDoc
     */
    protected function configure()
    {
        $this->setName('triumf:product:import');
        parent::configure();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return null|int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->write("Start Import!\n");
        $this->import->run();
        $output->write("Finish Import!\n");
    }
}
