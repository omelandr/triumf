<?php

namespace Triumf\Import\Setup\Patch\Data;

use Magento\Catalog\Model\Product;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\Patch\DataPatchInterface;

/**
 * Class UpdateRealSkuAttribute
 *
 * @package Triumf\Import\Setup\Patch\Data
 */
class UpdateRealSkuAttribute implements DataPatchInterface
{
    /**
     * @var \Magento\Eav\Setup\EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * UpdateRealSkuAttribute constructor.
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(
        EavSetupFactory $eavSetupFactory
    ) {
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        $eavSetup = $this->eavSetupFactory->create();

        if ($eavSetup->getAttribute(Product::ENTITY, "real_sku")) {
            $eavSetup->updateAttribute(
                Product::ENTITY,
                'real_sku',
                'is_html_allowed_on_front',
                0
            );
            $eavSetup->updateAttribute(
                Product::ENTITY,
                'real_sku',
                'is_visible_on_front',
                0
            );
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases(): array
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies(): array
    {
        return [];
    }
}
