<?php

namespace Triumf\Import\Api;

/**
 * Interface StockStatusImporterInterface
 *
 * @package Triumf\Import\Api
 */
interface StockStatusImporterInterface
{
    const STOCK_STATUS_TABLE_NAME = 'cataloginventory_stock_status';

    const INVENTORY_SOURCE_ITEM_TABLE_NAME = 'inventory_source_item';

    /**
     * @param array $stockData
     * @return mixed
     */
    public function import(array $stockData);
}
