<?php

declare(strict_types=1);

namespace Triumf\Import\Model;

use Triumf\Import\Model\Processor\DataProcessor;
use Triumf\Import\Model\Processor\FileProcessor;

/**
 * Class Import
 *
 * @package Triumf\Import\Model
 */
class Import
{
    /**
     * @var Processor\FileProcessor
     */
    private $fileProcessor;

    /**
     * @var DataProcessor
     */
    private $dataProcessor;

    /**
     * Import constructor.
     * @param FileProcessor $fileProcessor
     * @param DataProcessor $dataProcessor
     */
    public function __construct(
        FileProcessor $fileProcessor,
        DataProcessor $dataProcessor
    ) {
        $this->fileProcessor = $fileProcessor;
        $this->dataProcessor = $dataProcessor;
    }

    /**
     * @throws \Exception
     */
    public function run()
    {
        $fileData = $this->fileProcessor->getFileData();
        $this->dataProcessor->update($fileData);
    }
}
