<?php

namespace Triumf\Import\Model\Processor;

use Magento\Eav\Model\Config;
use Magento\Eav\Model\Entity\Attribute\AbstractAttribute;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\App\Area;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\App\State;
use Magento\Framework\Config\ScopeInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Filter\FilterManager;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Psr\Log\LoggerInterface;

/**
 * Class AbstractHandler
 *
 * @package Hlorka\PromImport\Model\DataProcessor\Handler
 */
abstract class AbstractProcessor
{

    /**
     * @var ResourceConnection
     */
    protected $connection;

    /**
     * @var Config
     */
    protected $eavAttribute;

    /**
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * @var LoggerInterface
     */
    protected $_logger;

    /**
     * @var State
     */
    protected $state;

    /**
     * The application scope manager.
     *
     * @var ScopeInterface
     */
    private $scope;
    /**
     * Core data
     *
     * @var FilterManager
     */
    protected $filter;

    /**
     * @var DateTime
     */
    protected $date;

    /**
     * AbstractHandler constructor.
     * @param ResourceConnection $connection
     * @param Config $eavAttribute
     * @param DataObjectHelper $dataObjectHelper
     * @param LoggerInterface $logger
     * @param State $state
     * @param ScopeInterface $scope
     * @param FilterManager $filterManager
     * @param DateTime $date
     */
    public function __construct(
        ResourceConnection $connection,
        Config $eavAttribute,
        DataObjectHelper $dataObjectHelper,
        LoggerInterface $logger,
        State $state,
        ScopeInterface $scope,
        FilterManager $filterManager,
        DateTime $date
    ) {
        $this->connection = $connection;
        $this->eavAttribute = $eavAttribute;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->_logger = $logger;
        $this->state = $state;
        $this->scope = $scope;
        $this->filter = $filterManager;
        $this->date = $date;
    }

    /**
     * @param array $data
     */
    abstract public function update(array $data);

    /**
     * @param $callback
     * @param $params
     * @return mixed
     * @throws \Exception
     */
    protected function initAdminState($callback, $params)
    {
        return $this->state->emulateAreaCode(
            Area::AREA_ADMINHTML,
            function () use ($callback, $params) {
                return call_user_func_array($callback, $params);
            }
        );
    }

    /**
     * Format translit data from string
     *
     * @param string $str
     * @return string
     */
    public function translitString($str)
    {
        return str_replace('-', '_', $this->filter->translitUrl($str));
    }

    /**
     *
     * @param string $str
     * @return string
     */
    public function translitStringToSku($str)
    {
        $res = preg_replace("/[^\s]/", "-", $str);
        return preg_replace('/[[:space:]]+/', '-', $res);
    }

    /**
     * @param $entityType
     * @param $attributeCode
     * @param $value
     * @return string|null
     */
    protected function loadDataByField($entityType, $attributeCode, $value)
    {
        if ($attribute = $this->getAttributeByCode($entityType, $attributeCode)) {
            $entityTableName = $this->connection->getTableName($entityType . '_entity_' . $attribute->getBackendType());
            $connection = $this->connection->getConnection();
            $bind = [
                'attribute_id' => $attribute->getAttributeId(),
                'store_id' => 0,
                'value' => $value
            ];
            $sql = $connection->select()
                ->from($entityTableName)
                ->reset(\Magento\Framework\DB\Select::COLUMNS)
                ->columns(['entity_id'])
                ->where(
                    'attribute_id = :attribute_id
                 AND value LIKE :value
                 AND store_id = :store_id'
                );
            return $connection->fetchOne($sql, $bind);
        }
        return null;
    }

    /**
     * @param $entityType
     * @param $entityId
     * @param array $bind - should contain array in format ['attribute_code' => 'attribute_value']
     * @return void
     */
    protected function updateAttributeValue($entityType, $entityId, array $bind)
    {
        if (empty($bind)) {
            return;
        }
        foreach ($bind as $attributeCode => $attributeValue) {
            $attribute = $this->getAttributeByCode($entityType, $attributeCode);
            if (empty($attribute)) {
                continue;
            }
            $entityTableName = $this->connection->getTableName($entityType . '_entity_' . $attribute->getBackendType());
            $connection = $this->connection->getConnection();
            $bind = ['value' => $attributeValue];
            $connection->update(
                $entityTableName,
                $bind,
                'attribute_id =' . $attribute->getAttributeId() . ' AND entity_id = ' . $entityId
            );
        }
    }

    /**
     * @param $entity
     * @param $code
     * @return AbstractAttribute|null
     */
    protected function getAttributeByCode($entity, $code)
    {
        try {
            return $this->eavAttribute->getAttribute($entity, $code);
        } catch (LocalizedException $exception) {
            return null;
        }
    }
}
