<?php

declare(strict_types=1);

namespace Triumf\Import\Model\Processor;

use Magento\Catalog\Model\Product\Type;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory as MagentoCategoryCollection;
use Magento\CatalogImportExport\Model\Import\Product\SkuProcessor;
use Magento\CatalogInventory\Api\StockItemCriteriaInterfaceFactory;
use Magento\CatalogInventory\Api\StockItemRepositoryInterface;
use Magento\Eav\Model\Config;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\App\State;
use Magento\Framework\Config\ScopeInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Filter\FilterManager;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Psr\Log\LoggerInterface;
use Triumf\Import\Model\ProductUpdater;

/**
 * Class DataProcessor
 *
 * @package Triumf\Import\Model\Processor
 */
class DataProcessor extends AbstractProcessor
{
    const DEFAULT_ROOT_CATEGORY_ID = 2;

    /**
     * @var AttributeProcessor
     */
    private $attributeProcessor;

    /**
     * @var ProductUpdater
     */
    private $productUpdater;

    /**
     * @var MagentoCategoryCollection
     */
    private $categoriesCollectionFactory;

    /**
     * @var array
     */
    private $categories = [];

    /**
     * @var StockItemRepositoryInterface
     */
    private $stockRepository;

    /**
     * @var array
     */
    protected $attributeBaseDataMap = [
        'barcode' => [
            'attribute_code' => 'barcode',
            'type' => 'text',
            'is_global' => '1',
            'is_html_allowed_on_front' => 0,
            'is_visible_on_front' => 0,
            'used_in_product_listing' => 0,
            'frontend_label' => 'Barcode',
        ],
        'weight_rock' => [
            'attribute_code' => 'weight_rock',
            'type' => 'text',
            'is_global' => '1',
            'frontend_label' => 'Rock Weight',
        ],
        'data_inc' => [
            'attribute_code' => 'data_inc',
            'type' => 'text',
            'frontend_label' => 'Data Inc',
            'is_global' => '1',
            'is_html_allowed_on_front' => 0,
            'is_visible_on_front' => 0,
            'used_in_product_listing' => 0,
        ],
        'seller_store' => [
            'attribute_code' => 'seller_store',
            'type' => 'multiselect',
            'is_global' => '1',
            'frontend_label' => 'Store',
            'is_html_allowed_on_front' => 0,
            'is_visible_on_front' => 0,
            'used_in_product_listing' => 0,
        ],
        'real_sku' => [
            'attribute_code' => 'real_sku',
            'type' => 'text',
            'is_global' => '1',
            'frontend_label' => 'Real Sku',
            'is_html_allowed_on_front' => 0,
            'is_visible_on_front' => 0,
            'used_in_product_listing' => 1,
        ]
    ];

    /**
     * @var array
     */
    protected $productSku = [];

    /**
     * @var array
     */
    protected $_productsData = [];

    /**
     * @var array
     */
    protected $_configurableProductsData = [];

    /**
     * @var string[]
     */
    protected $categoriesMap = [
        "Підвіска" => "ПОДВЕСКИ",
        "Каблучка" => "КОЛЬЦА",
        "Обручка" => "КОЛЬЦА",
        "Жемчуг" => "КОЛЬЦА",
        "Ланцюжок" => "ЦЕПОЧКИ",
        "Колье" => "ЦЕПОЧКИ",
        "Часы" => "БРАСЛЕТЫ",
        "Браслет" => "БРАСЛЕТЫ",
        "шармы" => "БРАСЛЕТЫ",
        "Сережки" => "СЕРЬГИ",
        "Каучук" => "ЦЕПОЧКИ",
        "Прикраси" => "АКСЕССУАРЫ",
        "Брелок" => "АКСЕССУАРЫ",
        "Брошь" => "АКСЕССУАРЫ",
        "Футляр" => "АКСЕССУАРЫ",
        "Крест" => "АКСЕССУАРЫ",
        "Шнур нитка" => "АКСЕССУАРЫ",
        "Булавка" => "АКСЕССУАРЫ",
        "Пірсінг" => "АКСЕССУАРЫ",
        "пирсинг" => "АКСЕССУАРЫ",
        "Ладанка" => "АКСЕССУАРЫ",
        "Нож" => "АКСЕССУАРЫ",
        "Икона" => "АКСЕССУАРЫ",
        "ложка" => "АКСЕССУАРЫ",
        "Вилка" => "АКСЕССУАРЫ"
    ];

    /**
     * @var StockItemCriteriaInterfaceFactory
     */
    private $stockItemCriteriaInterfaceFactory;

    /**
     * @var SkuProcessor
     */
    private $skuProcessor;

    private $_oldSkus = [];

    /**
     * DataProcessor constructor.
     * @param ResourceConnection $connection
     * @param Config $eavAttribute
     * @param DataObjectHelper $dataObjectHelper
     * @param LoggerInterface $logger
     * @param State $state
     * @param ScopeInterface $scope
     * @param FilterManager $filterManager
     * @param DateTime $date
     * @param AttributeProcessor $attributeProcessor
     * @param ProductUpdater $productUpdater
     * @param MagentoCategoryCollection $collectionFactory
     * @param StockItemRepositoryInterface $stockItemRepository
     * @param StockItemCriteriaInterfaceFactory $stockItemCriteriaInterfaceFactory
     * @param SkuProcessor $skuProcessor
     */
    public function __construct(
        ResourceConnection $connection,
        Config $eavAttribute,
        DataObjectHelper $dataObjectHelper,
        LoggerInterface $logger,
        State $state,
        ScopeInterface $scope,
        FilterManager $filterManager,
        DateTime $date,
        AttributeProcessor $attributeProcessor,
        ProductUpdater $productUpdater,
        MagentoCategoryCollection $collectionFactory,
        StockItemRepositoryInterface $stockItemRepository,
        StockItemCriteriaInterfaceFactory $stockItemCriteriaInterfaceFactory,
        SkuProcessor $skuProcessor
    ) {
        parent::__construct(
            $connection,
            $eavAttribute,
            $dataObjectHelper,
            $logger,
            $state,
            $scope,
            $filterManager,
            $date
        );
        $this->attributeProcessor = $attributeProcessor;
        $this->productUpdater = $productUpdater;
        $this->categoriesCollectionFactory = $collectionFactory;
        $this->stockRepository = $stockItemRepository;
        $this->stockItemCriteriaInterfaceFactory = $stockItemCriteriaInterfaceFactory;
        $this->skuProcessor = $skuProcessor;
        $this->_oldSkus = $this->skuProcessor->getOldSkus();
    }

    public function update(array $data)
    {
        $this->disableStocks();
        $this->checkAndAddAttributes();
        $this->recollectConfigurable($data);
        $attributesList = [];
        $updateArray = [];
        foreach ($this->_productsData as $configurablePackId => &$simpleProducts) {
            $this->productSku[$configurablePackId] = true;
            $counter = 1;
            $variations = [];
            foreach ($simpleProducts as &$simpleProduct) {
                while (isset($this->productSku[$simpleProduct['sku']])) {
                    $this->getNewSku($simpleProduct, $counter);
                }
                $this->productSku[$simpleProduct['sku']] = true;
                if (empty($attributesList)) {
                    $attributesList = array_keys($simpleProduct);
                }

                $variationAttribute = '';
                $variationAttributeValue = '';
                if (!empty($simpleProduct['size'])) {
                    $variationAttribute = 'size';
                    $variationAttributeValue = $simpleProduct['size'];
                } elseif (!empty($simpleProduct['length'])) {
                    $variationAttribute = 'length';
                    $variationAttributeValue = $simpleProduct['length'];
                } else {
                    $simpleProduct['visibility'] = 'catalog, search';
                }
                if (!empty($variationAttribute) && !empty($variationAttributeValue)) {
                    array_push($variations, 'sku=' . $simpleProduct['sku'] . ',' . $variationAttribute . '=' . $variationAttributeValue);
                } else {
                    if (empty($this->_oldSkus[$simpleProduct['sku']]) && !empty($this->categories['НОВОЕ'])) {
                        $simpleProduct['categories'] = [$this->categories['НОВОЕ']];
                    }
                }
            }
            $updateArray = array_merge($updateArray, $simpleProducts);
            if (!empty($variations)) {
                $this->_configurableProductsData[$configurablePackId] = $simpleProducts[0];
                $this->_configurableProductsData[$configurablePackId]['configurable_variations'] = implode('|', $variations);
                $this->_configurableProductsData[$configurablePackId]['visibility'] = 'catalog, search';
                $this->_configurableProductsData[$configurablePackId]['product_type'] = 'configurable';
                $this->_configurableProductsData[$configurablePackId]['sku'] = $configurablePackId;
                $this->_configurableProductsData[$configurablePackId]['url_key'] = $configurablePackId . '-' . $this->_configurableProductsData[$configurablePackId]['barcode'];
                $this->_configurableProductsData[$configurablePackId]['length'] = "0";
                $this->_configurableProductsData[$configurablePackId]['size'] = "";
                $this->_configurableProductsData[$configurablePackId]['barcode'] = '';
                if (empty($this->_oldSkus[$configurablePackId]) && !empty($this->categories['НОВОЕ'])) {
                    $simpleProduct['categories'] = [$this->categories['НОВОЕ']];
                }
            }
        }
        foreach (array_chunk($updateArray, 500) as $chunk) {
            $this->_logger->info('Starting data validation');
            $errorAggregator = $this->productUpdater->initRequiredData($chunk)
                ->validateData();
            if ($this->productUpdater->isDataValid()) {
                $this->_logger->info('Init data import for bunch');
                $this->productUpdater->importData();
            }
            foreach ($errorAggregator->getAllErrors() as $error) {
                $this->_logger->critical($error->getErrorMessage());
            }
        }

        if (!empty($this->_configurableProductsData)) {
            foreach (array_chunk($this->_configurableProductsData, 500) as $configurableChunk) {
                $this->_logger->info('Starting Configurable data validation');
                $errorAggregator = $this->productUpdater->initRequiredData($configurableChunk)
                    ->validateData();
                if ($this->productUpdater->isDataValid()) {
                    $this->_logger->info('Init Configurable data import for bunch');
                    $this->productUpdater->importData();
                }
                foreach ($errorAggregator->getAllErrors() as $error) {
                    $this->_logger->critical($error->getErrorMessage());
                }
            }
        }
    }

    /**
     * Disable stocks for all product before update
     * Only products that are in file considered as actual
     */
    protected function disableStocks()
    {
        $criteria = $this->stockItemCriteriaInterfaceFactory->create();
        $stocksRepository = $this->stockRepository->getList($criteria);
        if (!$stocksRepository->getTotalCount()) {
            return;
        }
        $connection = $this->connection->getConnection();
        echo "Start Disable Stocks. \n";
        $connection->beginTransaction();
        foreach ($stocksRepository->getItems() as $stockItem) {
            $stockItem->setQty(0);
            $this->stockRepository->save($stockItem);
        }
        $connection->commit();
        echo "Finish Disable Stocks. \n";
    }

    /**
     * @return void
     */
    protected function checkAndAddAttributes()
    {
        $this->attributeProcessor->update($this->attributeBaseDataMap);
    }

    /**
     * @param $simpleProduct
     * @param $counter
     */
    protected function getNewSku(&$simpleProduct, &$counter)
    {
        if (!empty($simpleProduct['size'])) {
            $newSku = $simpleProduct['sku'] . '-' . $simpleProduct['size'];
        } elseif (!empty($simpleProduct['length'])) {
            $newSku = $simpleProduct['sku'] . '-' . $simpleProduct['length'];
        } else {
            $newSku = $simpleProduct['sku'] . '-' . $counter;
            $counter++;
        }
        $simpleProduct['sku'] = $newSku;
    }

    /**
     * @param $data
     */
    protected function recollectConfigurable($data)
    {
        foreach ($data as $row) {
            if (empty($row['articul'])) {
                continue;
            }
            $configurableSku = $this->translitString($row['articul']);
            if (!empty($row['store'])) {
                $configurableSku .= '_' . $this->translitString($row['store']);
            }
            $this->_productsData[$configurableSku][] = $this->hydrateData($row);
        }
    }

    /**
     * @param $row
     * @return array
     */
    private function hydrateData($row)
    {
        $hydratedRow = [];
        $row['_attribute_set'] = $this->getAttributeSetIdByGroup($row['group']);
        $row['type_id'] = Type::TYPE_SIMPLE;
        foreach ($row as $attributeCode => $attributeData) {
            if (isset($this->attributeProcessor->getAttributeMap()[$attributeCode])) {
                $mapData = $this->attributeProcessor->getAttributeMap()[$attributeCode];
                if (empty($attributeData)) {
                    $attributeData = ' ';
                }
                if (is_array($mapData)) {
                    if (isset($mapData['split'])) {
                        foreach ($mapData['split'] as $dataType => $code) {
                            if ($dataType == 'text') {
                                $value = trim(preg_replace('/[^ a-zа-яёі]/ui', '', $attributeData));
                                if ($value == 'Срібло') {
                                    $value = 'Серебро';
                                }
                                $hydratedRow[$code] = $value;
                            } elseif ($dataType == 'int') {
                                $value = trim(preg_replace("/[^ \d]/", "", $attributeData));
                                $hydratedRow[$code] = $value;
                            }
                        }
                    } elseif (isset($mapData['group'])) {
                        $attributeCode = $this->getAttributeCodeByGroupType($row['group'], $mapData['group']);
                        if ($attributeCode && !empty(trim($attributeData))) {
                            $hydratedRow[$attributeCode] = preg_replace('/[^0-9.]/', '', $attributeData);
                        }
                    }
                } else {
                    if ($mapData == 'weight_rock' && floatval($attributeData) == 0) {
                        $attributeData = "";
                    }
                    $hydratedRow[$mapData] = $attributeData;
                }
            } else {
                if ($attributeCode === 'articul') {
                    $hydratedRow['real_sku'] = $attributeData;
                    if (!empty($row['store'])) {
                        $attributeData = $this->translitString($attributeData . '-' . $row['store']);
                    }
                }
                $hydratedRow[$attributeCode] = $attributeData;
            }
        }
        if ($hydratedRow['qty'] > 0) {
            $hydratedRow['is_in_stock'] = 1;
        }
        $hydratedRow['barcode'] = $hydratedRow['sku'];

        foreach ($hydratedRow as $attributeCode => $attributeValue) {
            $this->attributeProcessor->checkAndCreateOption($attributeCode, $attributeValue);
        }
        $hydratedRow['visibility'] = 'not visible individually';
        $hydratedRow['product_websites'] = 'base';
        if (empty($hydratedRow['size']) && empty($hydratedRow['length'])) {
            $hydratedRow['visibility'] = 'catalog, search';
        }
        $hydratedRow['product_type'] = Type::TYPE_SIMPLE;
        $hydratedRow['url_key'] = $hydratedRow['sku'] . '-' . $hydratedRow['barcode'];
        $hydratedRow['categories'] = $this->getCategoryByGroupName($hydratedRow['name']);
        $hydratedRow['type_product'] = "";
        return $hydratedRow;
    }

    /**
     * @param $group
     * @return mixed|string
     */
    private function getAttributeSetIdByGroup($group)
    {
        $attributeSets = $this->attributeProcessor->getLoadedAttributeSets();
        $attributeSetMap = $this->attributeProcessor->getAttributeSetMap();
        if (!empty($group) && !empty($attributeSetMap[$group])) {
            return $attributeSets[$attributeSetMap[$group]] ?? "Default";
        }
        return "Default";
    }

    /**
     * @param $productType
     * @param $map
     * @return int|string|null
     */
    private function getAttributeCodeByGroupType($productType, $map)
    {
        $productType = $this->translitString($productType);
        foreach ($map as $attributeCode => $groupsArr) {
            if (in_array($productType, $groupsArr)) {
                return $attributeCode;
            }
        }
        return null;
    }

    /**
     * @param $name
     * @return array
     */
    private function getCategoryByGroupName($name)
    {
        if (empty($this->categories)) {
            $this->loadCategoryData();
        }
        if (isset($this->categoriesMap[$name])) {
            $destCategoryName = $this->categoriesMap[$name];
            if (isset($this->categories[$destCategoryName])) {
                return [$this->categories[$destCategoryName]];
            }
        }
        return [];
    }

    /**
     * Load existing categories data
     */
    private function loadCategoryData() :void
    {
        $magentoCategories = $this->categoriesCollectionFactory->create()
            ->setStoreId(0)
            ->addFieldToSelect('name');
        if (!empty($magentoCategories)) {
            $this->collectTreeData($magentoCategories);
        }
    }

    /**
     * @param $categories
     * @return void
     */
    private function collectTreeData($categories)
    {
        foreach ($categories as $childCategory) {
            if ($childCategory->getId() > self::DEFAULT_ROOT_CATEGORY_ID && $childCategory->getLevel() == 2) {
                $this->categories[$childCategory->getName()] = $childCategory->getId();
            }
        }
    }
}
