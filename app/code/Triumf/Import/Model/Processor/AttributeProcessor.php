<?php

declare(strict_types=1);

namespace Triumf\Import\Model\Processor;

use Magento\Catalog\Api\AttributeSetRepositoryInterface;
use Magento\Catalog\Api\ProductAttributeRepositoryInterface;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory as AttributeCollectionFactory;
use Magento\Eav\Api\AttributeOptionManagementInterface;
use Magento\Eav\Api\Data\AttributeOptionInterfaceFactory;
use Magento\Eav\Model\AttributeManagement;
use Magento\Eav\Model\Config;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\App\State;
use Magento\Framework\Config\ScopeInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Filter\FilterManager;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Swatches\Helper\Data;
use Psr\Log\LoggerInterface;

/**
 * Class DataProcessor
 *
 * @package Triumf\Import\Model\Processor
 */
class AttributeProcessor extends AbstractProcessor
{
    protected $loadedAttributes = [];

    /**
     * @var SearchCriteriaInterface
     */
    private $searchCriteria;

    /**
     * @var AttributeSetRepositoryInterface
     */
    private $attributeSetRepository;

    /**
     * @var AttributeCollectionFactory
     */
    private $attributeCollectionFactory;

    /**
     * @var AttributeOptionInterfaceFactory
     */
    private $optionFactory;

    /**
     * @var \Magento\Eav\Api\Data\AttributeOptionLabelInterfaceFactory
     */
    private $optionLabelFactory;

    /**
     * @var AttributeOptionManagementInterface
     */
    private $attributeOptionManagement;

    /**
     * @var Data
     */
    private $swatchHelper;

    /**
     * @var array
     */
    protected $attributeMap = [
        'barcode' => 'sku',
        'group' => 'name',
        'inserts' => 'the_presence_of_an_insert',
        'metall' => [
            'split' => [
                'text' => 'type_metal',
                'int' => 'sample'
            ]
        ],
        'wgh' => 'weight',
        'wgh_rocks' => 'weight_rock',
        'producer' => 'manufacturer',
        'size' => [
            'group' => [
                'size' => ['pidviska', 'kabluchka', 'obruchka', 'serezhki'],
                'length' => ['lancjuzhok', '"kol_e', 'chasy', 'braslet', 'sharmy']
            ],
        ],
        'store' => 'seller_store'
    ];

    /**
     * @var string[]
     */
    protected $attributeSetMap = [
        "Підвіска" => "10",
        "Каблучка" => "9",
        "Обручка" => "9",
        "Жемчуг" => "12",
        "Ланцюжок" => "12",
        "Колье" => "12",
        "Часы" => "12",
        "Браслет" => "12",
        "шармы" => "12",
        "Сережки" => "10",
        "Каучук" => "12",
        "Прикраси" => "10",
        "Брелок" => "10",
        "Брошь" => "10",
        "Футляр" => "10",
        "Крест" => "10",
        "Шнур нитка" => "10",
        "Булавка" => "10",
        "Пірсінг" => "10",
        "пирсинг" => "10",
        "Ладанка" => "10",
        "Нож" => "10",
        "Икона" => "10",
        "ложка" => "10",
        "Вилка" => "10"
    ];

    /**
     * @var ProductAttributeRepositoryInterface
     */
    private $productAttributeRepository;

    /**
     * @var AttributeManagement
     */
    private $attributeManagement;

    /**
     * @var array
     */
    private $_attributeSetData = [];

    /**
     * AttributeProcessor constructor.
     * @param ResourceConnection $connection
     * @param Config $eavAttribute
     * @param DataObjectHelper $dataObjectHelper
     * @param LoggerInterface $logger
     * @param State $state
     * @param ScopeInterface $scope
     * @param FilterManager $filterManager
     * @param DateTime $date
     * @param SearchCriteriaInterface $searchCriteria
     * @param AttributeSetRepositoryInterface $attributeSetRepository
     * @param AttributeCollectionFactory $attributeCollectionFactory
     * @param AttributeOptionInterfaceFactory $optionInterfaceFactory
     * @param \Magento\Eav\Api\Data\AttributeOptionLabelInterfaceFactory $attributeOptionLabelInterfaceFactory
     * @param AttributeOptionManagementInterface $attributeOptionManagement
     * @param Data $swatchHelper
     * @param ProductAttributeRepositoryInterface $productAttributeRepository
     * @param AttributeManagement $attributeManagement
     */
    public function __construct(
        ResourceConnection $connection,
        Config $eavAttribute,
        DataObjectHelper $dataObjectHelper,
        LoggerInterface $logger,
        State $state,
        ScopeInterface $scope,
        FilterManager $filterManager,
        DateTime $date,
        SearchCriteriaInterface $searchCriteria,
        AttributeSetRepositoryInterface $attributeSetRepository,
        AttributeCollectionFactory $attributeCollectionFactory,
        AttributeOptionInterfaceFactory $optionInterfaceFactory,
        \Magento\Eav\Api\Data\AttributeOptionLabelInterfaceFactory $attributeOptionLabelInterfaceFactory,
        AttributeOptionManagementInterface $attributeOptionManagement,
        Data $swatchHelper,
        ProductAttributeRepositoryInterface $productAttributeRepository,
        AttributeManagement $attributeManagement
    ) {
        parent::__construct(
            $connection,
            $eavAttribute,
            $dataObjectHelper,
            $logger,
            $state,
            $scope,
            $filterManager,
            $date
        );
        $this->searchCriteria = $searchCriteria;
        $this->attributeSetRepository = $attributeSetRepository;
        $this->attributeCollectionFactory = $attributeCollectionFactory;
        $this->optionFactory = $optionInterfaceFactory;
        $this->optionLabelFactory = $attributeOptionLabelInterfaceFactory;
        $this->attributeOptionManagement = $attributeOptionManagement;
        $this->swatchHelper = $swatchHelper;
        $this->productAttributeRepository = $productAttributeRepository;
        $this->attributeManagement = $attributeManagement;
    }

    /**
     * @return string[]
     */
    public function getAttributeSetMap()
    {
        return $this->attributeSetMap;
    }

    /**
     * Receive attributes list with params for update/create
     * format:
     *  array("attribute_code" =>
     *      "options" => array(option_name_1, option_name_2, ...., option_name_n),
     *      "type" => array('select', 'multiselect' ....),
     *      ...
     *  );
     * If attribute exist - will check params to insert/update
     * Else - will try to create with given params and options
     *
     * @param array $data
     */
    public function update(array $data)
    {
        if (empty($data)) {
            return;
        }
        foreach ($data as $attributeCode => $attributeData) {
            if (!isset($this->loadedAttributes[$attributeCode])) {
                $this->createNewAttribute($attributeCode, $attributeData);
            }
            if (isset($attributeData['options'])) {
                foreach ($attributeData['options'] as $optionName) {
                    $this->checkAndCreateOption($attributeCode, $optionName);
                }
            }
        }
    }

    /**
     * @param $attributeCode
     * @param $attributeData
     */
    protected function createNewAttribute($attributeCode, $attributeData)
    {
        if (empty($attributeData['type'])) {
            $attributeData['type'] = 'text';
        }
        $basicData = $this->retrieveDataByType($attributeData['type']);
        $attributeData = array_merge($basicData, $attributeData);
        unset($attributeData['type']);
        $attribute = $this->getOrCreateMagentoAttribute($attributeData);
        if ($attribute && $attribute->getAttributeId()) {
            $this->loadedAttributes[$attribute->getAttributeCode()] = true;
            if ($attribute->usesSource()) {
                $this->loadedAttributes[$attribute->getAttributeCode()] = [];
            }
            return $attribute->getAttributeCode();
        }
        return null;
    }

    /**
     * @param $attributeData
     * @return \Magento\Catalog\Api\Data\ProductAttributeInterface|\Magento\Eav\Model\Entity\Attribute\AbstractAttribute|null
     * @throws LocalizedException
     */
    protected function getOrCreateMagentoAttribute($attributeData)
    {
        $attribute = $this->eavAttribute->getAttribute(Product::ENTITY, $attributeData['attribute_code']);
        if (!empty($attribute) && $attribute->getAttributeId()) {
            return $attribute;
        }

        $attribute->setData($attributeData);
        try {
            $attribute = $this->productAttributeRepository->save($attribute);
            $this->attributeManagement->assign(
                $attributeData['entity_type_id'],
                4,
                7,
                $attribute->getAttributeCode(),
                100
            );
            $this->attributeManagement->assign(
                $attributeData['entity_type_id'],
                9,
                32,
                $attribute->getAttributeCode(),
                100
            );
        } catch (NoSuchEntityException | \Exception $exception) {
            $this->_logger->error(__('Can not save attribute with code: %1', $attributeData['attribute_code']));
            $this->_logger->error($exception->getMessage());
        }
        return null;
    }

    /**
     * @param $type
     * @return array|string[]
     */
    private function retrieveDataByType($type)
    {
        switch ($type) {
            case 'boolean':
                $typeData = [
                    'backend_type' => 'int',
                    'frontend_input' => 'boolean',
                    'source_model' => \Magento\Eav\Model\Entity\Attribute\Source\Boolean::class
                ];
                break;
            case 'select':
                $typeData = [
                    'backend_type' => 'int',
                    'frontend_input' => 'select',
                    'source_model' => \Magento\Eav\Model\Entity\Attribute\Source\Table::class
                ];
                break;
            case 'multiselect':
                $typeData = [
                    'backend_type' => 'text',
                    'frontend_input' => 'multiselect',
                    'source_model' => \Magento\Eav\Model\Entity\Attribute\Source\Table::class,
                    'backend' => 'Magento\Eav\Model\Entity\Attribute\Backend\ArrayBackend',
                ];
                break;
            default:
                $typeData = [
                    'backend_type' => 'varchar',
                    'frontend_input' => 'text',
                    'source_model' => null
                ];
        }

        $defaultData = [
            'entity_type_id'                => Product::ENTITY,
            'is_global'                     => 0,
            'is_user_defined'               => 1,
            'is_unique'                     => 0,
            'is_required'                   => 0,
            'is_searchable'                 => 0,
            'is_visible_in_advanced_search' => 0,
            'is_comparable'                 => 0,
            'is_filterable'                 => 0,
            'is_filterable_in_search'       => 0,
            'is_used_for_promo_rules'       => 0,
            'is_html_allowed_on_front'      => 1,
            'is_visible_on_front'           => 1,
            'used_in_product_listing'       => 1,
            'used_for_sort_by'              => 0
        ];
        if ($typeData['frontend_input'] !== 'text') {
            $defaultData['is_filterable'] = $defaultData['is_filterable_in_search'] = 1;
        }
        return array_merge($typeData, $defaultData);
    }

    /**
     * @return void
     */
    public function loadAttributes()
    {
        if (!empty($this->loadedAttributes)) {
            return;
        }
        $attributeCodes = [];
        foreach ($this->attributeMap as $attributeCode) {
            if (is_array($attributeCode)) {
                foreach ($attributeCode as $spitType => $codesList) {
                    foreach ($codesList as $code) {
                        $attributeCodes[] = $code;
                    }
                }
            } else {
                $attributeCodes[] = $attributeCode;
            }
        }
        $collection = $this->attributeCollectionFactory->create()
            ->addFieldToFilter('attribute_code', ['in' => $attributeCodes]);

        foreach ($collection as $attribute) {
            if ($attribute->usesSource()) {
                $options = [];
                $optionsList = $attribute->getOptions();
                if (!empty($optionsList)) {
                    foreach ($optionsList as $option) {
                        if (!empty(trim($option->getLabel())) && !empty(trim($option->getValue()))) {
                            $options[strtolower($option->getValue())] = $option->getLabel();
                        }
                    }
                }
                $this->loadedAttributes[$attribute->getAttributeCode()] = $options;
            } else {
                $this->loadedAttributes[$attribute->getAttributeCode()] = true;
            }
        }
    }

    /**
     * @return array
     */
    public function reloadAttribute()
    {
        $this->loadedAttributes = [];
        $this->loadAttributes();
        return $this->loadedAttributes;
    }

    /**
     * @param $attributeCode
     * @return void
     */
    public function addAttribute($attributeCode)
    {
        $this->loadedAttributes[$attributeCode] = [];
    }

    /**
     * @param $attributeCode
     * @param $optionValue
     * @param $default
     */
    public function checkAndCreateOption($attributeCode, $optionValue, $default = false)
    {
        if (!empty($this->loadedAttributes[$attributeCode]) && !empty($this->loadedAttributes[$attributeCode][strtolower($optionValue)])) {
            return $this->loadedAttributes[$attributeCode][strtolower($optionValue)];
        }
        $attribute = $this->eavAttribute->getAttribute(Product::ENTITY, $attributeCode);
        if (empty($attribute)
            || empty($attribute->getAttributeId())
            || !$attribute->usesSource()
        ) {
            return null;
        }
        $requiredOption = null;
        /* Load all option for selected attribute and mark required option for current use */
        foreach ($attribute->getOptions() as $option) {
            if (empty($option->getValue()) || empty($option->getLabel())) {
                continue;
            }
            if (strtolower($option->getLabel()) === strtolower($optionValue)) {
                $requiredOption = $option->getValue();
            }
            $this->loadedAttributes[$attributeCode][strtolower($option->getLabel())] = $option->getValue();
        }
        if (null === $requiredOption) {
            return $this->createAttributeOption($attribute, $optionValue, $default);
        }
        return null;
    }

    /**
     * @param $attribute
     * @param $optionName
     * @return string|null
     */
    private function createAttributeOption($attribute, $optionName, $default)
    {
        if ($this->swatchHelper->isSwatchAttribute($attribute) && $optionName == ' ') {
            $optionName = "0";
        }
        //$optionName = trim($optionName);
        /** @var \Magento\Eav\Model\Entity\Attribute\OptionLabel $optionLabel */
        $optionLabel = $this->optionLabelFactory->create();
        $optionLabel->setStoreId(0);
        $optionLabel->setLabel($optionName);

        $option = $this->optionFactory->create();
        $option->setLabel($optionName);
        $option->setStoreLabels([$optionLabel]);
        $option->setSortOrder(0);
        $option->setIsDefault($default);
        try {
            $optionId = $this->attributeOptionManagement->add(
                \Magento\Catalog\Model\Product::ENTITY,
                $attribute->getAttributeId(),
                $option
            );
            if ($optionId && $optionId !== 'id_new_option') {
                $this->loadedAttributes[$attribute->getAttributeCode()][strtolower($optionName)] = $optionId;
                if ($this->swatchHelper->isSwatchAttribute($attribute)) {
                    $this->addSwatchData($attribute, $optionId, $optionName);
                }
                return $optionId;
            }
        } catch (LocalizedException $exception) {
            $this->_logger->error($exception);
        }
        return null;
    }

    /**
     * @param $attribute
     * @param $optionId
     * @param $optionName
     */
    private function addSwatchData($attribute, $optionId, $optionName)
    {
        $swatchType = 0;
        $value = $optionName;
        if ($this->swatchHelper->isVisualSwatch($attribute)) {
            $swatchType = 1;
            $value = '#FFF';
        }
        $swatchTable = $this->connection->getTableName('eav_attribute_option_swatch');
        $swatchData = [
            'option_id' => str_replace('id_', '', $optionId),
            'type' => $swatchType,
            'store_id' => 0,
            'value' => $value
        ];

        $this->connection->getConnection()
            ->insertOnDuplicate(
                $swatchTable,
                $swatchData
            );
    }

    /**
     * @return array
     */
    public function getAttributeMap()
    {
        return $this->attributeMap;
    }

    /**
     * @return void
     */
    protected function loadAttributeSets()
    {
        $attributeSets = $this->attributeSetRepository->getList($this->searchCriteria);
        if ($attributeSets->getTotalCount()) {
            foreach ($attributeSets->getItems() as $attributeSet) {
                $this->_attributeSetData[$attributeSet->getAttributeSetId()] = $attributeSet->getAttributeSetName();
            }
        }
    }

    /**
     * @return array
     */
    public function getLoadedAttributeSets()
    {
        if (!empty($this->_attributeSetData)) {
            return $this->_attributeSetData;
        }
        $this->loadAttributeSets();
        return $this->_attributeSetData;
    }

    /**
     * @return array
     */
    public function getLoadedAttributes()
    {
        if (!empty($this->loadedAttributes)) {
            return $this->loadedAttributes;
        }
        $this->loadAttributes();
        return $this->loadedAttributes;
    }
}
