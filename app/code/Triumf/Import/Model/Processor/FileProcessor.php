<?php

declare(strict_types=1);

namespace Triumf\Import\Model\Processor;

use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\File\Csv;
use Magento\Framework\Filesystem\DirectoryList;
use Magento\Framework\Filesystem\Driver\File;
use Magento\Framework\HTTP\Client\CurlFactory;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\Xml\Parser;
use Psr\Log\LoggerInterface;

/**
 * Class FileProcessor
 * @package Triumf\Import\Model\UpdateProcessor
 */
class FileProcessor
{
    const IMPORT_DIR_NAME = 'triumf_import';
    const IMAGES_DIR_NAME = 'images';
    const IMPORT_FILE_NAME = 'import.csv';

    /**
     * @var Parser
     */
    private $parser;

    /**
     * @var DirectoryList
     */
    private $directoryList;

    /**
     * @var File
     */
    private $file;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var CurlFactory
     */
    private $curlFactory;

    /**
     * @var Json
     */
    private $json;

    /**
     * @var Csv
     */
    private $csv;

    /**
     * Processor constructor.
     * @param Parser $parser
     * @param DirectoryList $directoryList
     * @param File $file
     * @param LoggerInterface $logger
     * @param CurlFactory $curlFactory
     * @param Json $json
     * @param Csv $csv
     */
    public function __construct(
        Parser $parser,
        DirectoryList $directoryList,
        File $file,
        LoggerInterface $logger,
        CurlFactory $curlFactory,
        Json $json,
        Csv $csv
    ) {
        $this->parser = $parser;
        $this->directoryList = $directoryList;
        $this->file = $file;
        $this->logger = $logger;
        $this->curlFactory = $curlFactory;
        $this->json = $json;
        $this->csv = $csv;
    }

    /**
     * @param string $fileName
     * @return array
     * @throws \Exception
     */
    public function getFileData($fileName = '')
    {
        if (empty($fileName)) {
            $currentDate = date('d.m.Y');
            $fileName = 'uploads-' . $currentDate . '.csv';
        }
        $filePath = $this->getFullPath($fileName);
        if ($filePath === false) {
            $this->logger->error(__('Can not load file %1. File does not exist or directory is not readable.', $fileName));
            return [];
        }
        return $this->loadFileData($filePath);
    }

    /**
     * @param $filePath
     * @return array
     * @throws \Exception
     */
    protected function loadFileData($filePath)
    {
        $fileContent = $this->csv->setDelimiter(';')
            ->getData($filePath);
        if (empty($fileContent)) {
            return [];
        }
        $header = array_shift($fileContent);
        foreach ($fileContent as &$row) {
            $row = array_combine($header, $row);
        }
        return $fileContent;
    }

    /**
     * @param $fileName
     * @return false|string
     */
    private function getFullPath($fileName)
    {
        try {
            $filePath =  $this->getBaseDirPath() . DIRECTORY_SEPARATOR . $fileName;
            return $this->file->isExists($filePath) ? $filePath : false;
        } catch (FileSystemException $exception) {
            $this->logger->error($exception);
            return false;
        }
    }

    /**
     * @return string[]
     * @throws FileSystemException
     */
    public function getImagesList()
    {
        $dir =  $this->getBaseDirPath() . DIRECTORY_SEPARATOR . self::IMAGES_DIR_NAME;
        return $this->file->readDirectory($dir);
    }

    /**
     * @param $sourceFileName
     * @param $destinationFileName
     * @return bool
     */
    public function renameFile($sourceFileName, $destinationFileName)
    {
        try {
            return $this->file->rename($sourceFileName, $destinationFileName);
        } catch (FileSystemException $exception) {
            $this->logger->error($exception);
            return false;
        }
    }

    /**
     * @return string
     * @throws FileSystemException
     */
    public function getBaseDirPath()
    {
        return $this->directoryList->getPath('media')
            . DIRECTORY_SEPARATOR
            . self::IMPORT_DIR_NAME;
    }

    /**
     * @param $filePath
     * @return void
     */
    public function removeFile($filePath) :void
    {
        try {
            $this->file->deleteFile($filePath);
        } catch (FileSystemException $exception) {
            $message = sprintf('Can not remove file %s. Error: %d', $filePath, $exception->getMessage());
            $this->logger->error($message);
        }
    }

    /**
     * @param $dirPath
     * @return void
     */
    public function removeDir($dirPath) :void
    {
        if (!$this->file->isDirectory($dirPath)) {
            return;
        }
        try {
            $this->file->deleteDirectory($dirPath);
        } catch (FileSystemException $exception) {
            $message = sprintf('Can not remove directory %s. Error: %d', $dirPath, $exception->getMessage());
            $this->logger->error($message);
        }
    }

}
