<?php

declare(strict_types=1);

namespace Triumf\Import\Model\Processor;

use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Action;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\CatalogImportExport\Model\Import\Product\MediaGalleryProcessor;
use Magento\Eav\Model\Config;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\App\State;
use Magento\Framework\Config\ScopeInterface;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Filesystem\DirectoryList;
use Magento\Framework\Filesystem\Driver\File;
use Magento\Framework\Filter\FilterManager;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Psr\Log\LoggerInterface;

/**
 * Class ImagesProcessor
 *
 * @package Triumf\Import\Model\Processor
 */
class ImagesProcessor extends AbstractProcessor
{
    const ATTRIBUTE_CODE = 'barcode';

    const DEFAULT_BUNCH_SIZE = 500;

    const BASE_MEDIA_IMAGES_DIR = 'catalog/product';

    /**
     * @var File
     */
    private $file;

    /**
     * @var Action
     */
    private $action;

    /**
     * @var DirectoryList
     */
    private $directoryList;

    /**
     * @var array
     */
    private $allowedImages = [];

    /**
     * @var string[]
     */
    private $allowedImagesExtension = ['jpeg', 'jpg', 'png'];

    /**
     * @var string[]
     */
    private $imageAttributeCodes = ['image', 'small_image', 'thumbnail'];

    /**
     * @var array
     */
    private $productsBunch = [];

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var Product\Gallery\Processor
     */
    private $processor;

    /**
     *
     * @var MediaProcessor
     */
    private $mediaProcessor;

    public function __construct(
        ResourceConnection $connection,
        Config $eavAttribute,
        DataObjectHelper $dataObjectHelper,
        LoggerInterface $logger,
        State $state,
        ScopeInterface $scope,
        FilterManager $filterManager,
        DateTime $date,
        File $file,
        Action $action,
        DirectoryList $directoryList,
        CollectionFactory $collectionFactory,
        \Triumf\Import\Model\Processor\MediaProcessor $mediaGalleryProcessor
    ) {
        parent::__construct(
            $connection,
            $eavAttribute,
            $dataObjectHelper,
            $logger,
            $state,
            $scope,
            $filterManager,
            $date
        );
        $this->file = $file;
        $this->action = $action;
        $this->directoryList = $directoryList;
        $this->connection = $connection->getConnection();
        $this->collectionFactory = $collectionFactory;
        $this->mediaProcessor = $mediaGalleryProcessor;
    }

    /**
     * @param array $data
     */
    public function update(array $data)
    {
        if (empty($data)) {
            return;
        }
        $this->getAllowedImagesList($data);
        if (empty($this->allowedImages)) {
            return;
        }
        $this->getProductIdsList();
        if (!empty($this->productsBunch)) {
            $mediaGallery = [];
            foreach ($this->allowedImages as $barcode => $imagePath) {
                if (empty($this->productsBunch[$barcode])) {
                    continue;
                }
                $productSku = $this->productsBunch[$barcode];
                $newImagePath = $this->moveImageToMedia($imagePath);
                $mediaGallery[0][$productSku][$newImagePath] = [
                    'attribute_id' => $this->getMediaGalleryAttributeId(),
                    'label' => $barcode,
                    'position' => 1,
                    'disabled' => '0',
                    'value' => $newImagePath,
                ];
                $imageLabels = [
                    'image' => $newImagePath,
                    'small_image' => $newImagePath,
                    'thumbnail' => $newImagePath
                ];
                $this->mediaProcessor->saveImageLabels([$productSku], $imageLabels);
            }
            $this->mediaProcessor->saveMediaGallery($mediaGallery);
        }
    }

    /**
     * @return \Magento\Eav\Model\Entity\Attribute\AbstractAttribute|null
     */
    private function getMediaGalleryAttributeId()
    {
        try {
            $attribute =  $this->eavAttribute->getAttribute(Product::ENTITY, 'media_gallery');
            return $attribute->getAttributeId();
        } catch (LocalizedException $exception) {
            return null;
        }
    }


    /**
     * @param $imagePath
     * @return string|null
     * @throws FileSystemException
     */
    private function moveImageToMedia($imagePath)
    {
        $info = pathinfo($imagePath);
        $firstChar = mb_substr($info['filename'], 0, 1, "UTF-8");
        $secondChar = mb_substr($info['filename'], 1, 1, "UTF-8");
        $dirPath = DIRECTORY_SEPARATOR . $firstChar . DIRECTORY_SEPARATOR . $secondChar . DIRECTORY_SEPARATOR;
        $mediaDir = $this->directoryList->getPath('media') . DIRECTORY_SEPARATOR . self::BASE_MEDIA_IMAGES_DIR;
        $newDir = $mediaDir . $dirPath;
        try {
            $this->file->createDirectory($newDir);
            $this->file->copy($imagePath, $newDir . $info['basename']);
        } catch (FileSystemException $exception) {
            return null;
        }
        $this->file->deleteFile($imagePath);
        return $dirPath . $info['basename'];
    }

    /**
     * @return \Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    protected function getProductsCollection()
    {
        return $this->collectionFactory->create();
    }

    /**
     * @return void
     */
    private function getProductIdsList()
    {
        try {
            $barcodeAttribute = $this->eavAttribute->getAttribute(Product::ENTITY, self::ATTRIBUTE_CODE);
        } catch (LocalizedException $exception) {
            return;
        }
        if (!empty($barcodeAttribute) && $barcodeAttribute->getAttributeId()) {
            foreach (array_chunk(array_keys($this->allowedImages), self::DEFAULT_BUNCH_SIZE) as $allowedChunk) {
                $select = $this->connection->select()
                    ->from($barcodeAttribute->getBackendTable())
                    ->where('attribute_id=?', $barcodeAttribute->getAttributeId())
                    ->where('value IN (?)', $allowedChunk);

                $result = $this->connection->fetchAll($select);
                if (!empty($result)) {
                    foreach ($result as $row) {
                        $this->productsBunch[$row['value']] = $row['entity_id'];
                    }
                }
            }
        }
    }

    /**
     * @param $data
     * @return array
     * @throws FileSystemException
     */
    private function getAllowedImagesList($data)
    {
        $this->allowedImages = [];
        foreach ($data as $imagePath) {
            $pathInfo = pathinfo($imagePath);
            $imageExtension = $pathInfo['extension'];
            if (!in_array(strtolower($imageExtension), $this->allowedImagesExtension)) {
                $this->file->deleteFile($imagePath);
                continue;
            }
            $this->allowedImages[$pathInfo['filename']] = $imagePath;
        }
        return $this->allowedImages;
    }
}
