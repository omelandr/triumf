<?php

declare(strict_types=1);

namespace Triumf\Import\Model;

use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Exception\CouldNotSaveException;
use Triumf\Import\Api\StockStatusImporterInterface;
use Psr\Log\LoggerInterface;

/**
 * Class StockStatusImporter
 *
 * @package Hlorka\PromImport\Model\UpdateProcessor
 */
class StockStatusImporter implements StockStatusImporterInterface
{
    /**
     * Stock Item Resource Factory
     *
     * @var ResourceConnection $resourceConnection
     */
    private $resourceConnection;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * StockItemImporter constructor
     *
     * @param ResourceConnection $resourceConnection
     * @param LoggerInterface $logger
     */
    public function __construct(
        ResourceConnection $resourceConnection,
        LoggerInterface $logger
    ) {
        $this->resourceConnection = $resourceConnection;
        $this->logger = $logger;
    }

    /**
     * @inheritdoc
     */
    public function import(array $stockData)
    {
        if (empty($stockData)) {
            $this->logger->notice(__('Stock data is empty!'));
            return;
        }
        $connection = $this->resourceConnection->getConnection();
        if ($connection->isTableExists(self::INVENTORY_SOURCE_ITEM_TABLE_NAME)) {
            $sourceItemTable = $connection->getTableName(self::INVENTORY_SOURCE_ITEM_TABLE_NAME);
            $stockItems = [];
            try {
                foreach ($stockData as $itemSku => $stockItem) {
                    $stockItems[] = [
                        'sku' => $itemSku,
                        'source_code' => 'default',
                        'quantity' => $stockItem['qty'],
                        'status' => 1
                    ];
                }
                $connection->insertOnDuplicate($sourceItemTable, $stockItems);
            } catch (\Exception $e) {
                $this->logger->error($e->getMessage());
            }
        }
        if ($connection->isTableExists(self::STOCK_STATUS_TABLE_NAME)) {
            $entityTable = $connection->getTableName(self::STOCK_STATUS_TABLE_NAME);
            try {
                $stockImportData = array_map(
                    function ($stockItemData) {
                        $stockItemData['stock_status'] = 1;
                        return array_intersect_key(
                            $stockItemData,
                            array_flip(['product_id', 'website_id', 'stock_id', 'qty', 'stock_status'])
                        );
                    },
                    array_values($stockData)
                );
                $connection->insertOnDuplicate($entityTable, $stockImportData);
            } catch (\Exception $e) {
                $this->logger->error($e->getMessage());
            }
        }
    }
}
