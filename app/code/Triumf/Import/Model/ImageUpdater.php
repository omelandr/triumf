<?php

declare(strict_types=1);

namespace Triumf\Import\Model;

use Triumf\Import\Model\Processor\ImagesProcessor;

/**
 * Class ImageUpdater
 *
 * @package Triumf\Import\Model
 */
class ImageUpdater
{
    /**
     * @var Processor\FileProcessor
     */
    private $fileProcessor;

    /**
     * @var ImagesProcessor
     */
    private $imagesProcessor;

    /**
     * ImageUpdater constructor.
     * @param Processor\FileProcessor $fileProcessor
     * @param ImagesProcessor $imagesProcessor
     */
    public function __construct(
        \Triumf\Import\Model\Processor\FileProcessor $fileProcessor,
        ImagesProcessor $imagesProcessor
    ) {
        $this->fileProcessor = $fileProcessor;
        $this->imagesProcessor = $imagesProcessor;
    }

    public function run()
    {
        $images = $this->fileProcessor->getImagesList();
        $this->imagesProcessor->update($images);
    }
}
