<?php

namespace Triumf\Customization\Setup;


use Magento\Cms\Model\BlockFactory;
use Magento\Eav\Api\AttributeManagementInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;
use Magento\Catalog\Model\Config as CatalogConfig;

class UpgradeData implements UpgradeDataInterface
{
    private $eavSetupFactory;
    private $blockFactory;
    private $config;
    private $attributeManagementInterface;

    public function __construct(\Magento\Eav\Setup\EavSetupFactory $eavSetupFactory,
                                BlockFactory $blockFactory,
                                CatalogConfig $config,
                                AttributeManagementInterface $attributeManagementInterface
    )
    {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->blockFactory = $blockFactory;
        $this->config = $config;
        $this->attributeManagementInterface = $attributeManagementInterface;
    }

    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        if (version_compare($context->getVersion(), '1.1.0', '<')) {
            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY, 'top_sales', [
                    'type' => 'int',
                    'backend' => 'Magento\Eav\Model\Entity\Attribute\Backend\ArrayBackend',
                    'frontend' => '',
                    'label' => 'Top Sales',
                    'input' => 'boolean',
                    'group' => 'General',
                    'class' => 'custom_attribute_name',
                    'source' => 'Magento\Eav\Model\Entity\Attribute\Source\Boolean',
                    'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                    'visible' => true,
                    'required' => false,
                    'user_defined' => false,
                    'default' => '0',
                    'searchable' => false,
                    'filterable' => false,
                    'comparable' => false,
                    'visible_on_front' => false,
                    'used_in_product_listing' => true,
                    'unique' => false
                ]
            );
        }

        if (version_compare($context->getVersion(), '1.2.0', '<')) {

            $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Category::ENTITY,
                'custom_url',
                array(
                    'type' => 'varchar',
                    'label' => 'Custom Url',
                    'input' => 'text',
                    'source' => '',
                    'visible' => true,
                    'backend' => '',
                    'default' => null,
                    'required' => false,
                    'sort_order' => 100,
                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                    'group' => 'General Information',
                    'used_in_product_listing' => false,
                    'visible_on_front' => false,
                    'is_used_in_grid' => true,
                    'is_visible_in_grid' => true
                ));
        }

        if (version_compare($context->getVersion(), '1.3.0', '<')) {
            $cmsBlockData = [
                'title' => 'Header links',
                'identifier' => 'header_links',
                'content' => '<div id="header_new_links">
                                 <ul class="header-links">
                                     <li class="header-new-links"><a href="akcii.html">АКЦИИ</a></li>
                                     <li class="header-new-links"><a href="payment-and-delivery">ОПЛАТА И ДОСТАВКА</a></li>
                                     <li class="header-new-links"><a href="about-company">О КОМПАНИИ</a></li>
                                     <li class="header-new-links"><a href="shops">МАГАЗИНЫ</a></li>
                                 </ul>
                              </div>',
                'stores' => [0],
                'is_active' => 1,
                'sort_order' => 0
            ];
            $this->blockFactory->create()->setData($cmsBlockData)->save();
        }

        if (version_compare($context->getVersion(), '1.4.0', '<')) {
            $cmsBlockData = [
                'title' => 'Footer links',
                'identifier' => 'footer_links',
                'content' => '<div id="footer_new_links">
                                 <ul class="footer-links">
                                     <li class="footer-new-links"><a href="akcii.html">АКЦИИ</a></li>
                                     <li class="footer-new-links"><a href="payment-and-delivery">ОПЛАТА И ДОСТАВКА</a></li>
                                     <li class="footer-new-links"><a href="about-company">О КОМПАНИИ</a></li>
                                     <li class="footer-new-links"><a href="shops">МАГАЗИНЫ</a></li>
                                 </ul>
                              </div>',
                'stores' => [0],
                'is_active' => 1,
                'sort_order' => 0
            ];
            $this->blockFactory->create()->setData($cmsBlockData)->save();
        }

        if (version_compare($context->getVersion(), '1.5.0', '<')) {
            $cmsBlockData = [
                'title' => 'Footer links address',
                'identifier' => 'footer_links_address',
                'content' => '<div id="footer_links_address" xmlns="http://www.w3.org/1999/html">
                                	<div class="email-address-links">
						                <p><span>info@triumf.lg.ua</span></p>
					                </div>
                                    <div class="city-address-links">
                                        <span>г. Рубежное, Луганская обл.,</span>
                                        <br><span>Украина ул. Строителей, 10-А</span></br>
                                    </div>
                              	</div>',
                'stores' => [0],
                'is_active' => 1,
                'sort_order' => 0
            ];
            $this->blockFactory->create()->setData($cmsBlockData)->save();
        }

        if (version_compare($context->getVersion(), '1.6.0', '<')) {
            $cmsBlockData = [
                'title' => 'Footer links logo',
                'identifier' => 'footer_links_logo',
                'content' => '<div id="footer_logo"><img src="{{media url="logo.jpg"}}" alt="" width="100" height="100"></div>',
                'stores' => [0],
                'is_active' => 1,
                'sort_order' => 0
            ];
            $this->blockFactory->create()->setData($cmsBlockData)->save();
        }

        if (version_compare($context->getVersion(), '1.7.0', '<')) {
            $cmsBlockData = [
                'title' => 'Footer links phone',
                'identifier' => 'footer_links_phone',
                'content' => '<div id="callback_phone_number">
                                    <div id="phone-number">
                                        <span>+38(06453)502-90</span><br>
                                        <span>+38(06453)542-56</span><br>
                                        <span>+38(050)884-27-94</span><br>
                                        <span>+38(093)603-14-37</span><br>
                                    </div>
                                    <div id="show-feedback-form">
                                        <span>ПЕРЕЗВОНИТЕ МНЕ</span>
                                    </div>
                               </div>',
                'stores' => [0],
                'is_active' => 1,
                'sort_order' => 0
            ];
            $this->blockFactory->create()->setData($cmsBlockData)->save();
        }

        if (version_compare($context->getVersion(), '1.8.0', '<')) {
            $cmsBlockData = [
                'title' => 'Footer links memo',
                'identifier' => 'footer_links_memo',
                'content' => '<div id="links_memo">
                                    <span class="links_create">СОЗДАНО</span><br>
                                    <span class="links_create_memo">MEMO</span>
                               </div>',
                'stores' => [0],
                'is_active' => 1,
                'sort_order' => 0
            ];
            $this->blockFactory->create()->setData($cmsBlockData)->save();
        }

        if (version_compare($context->getVersion(), '1.9.0', '<')) {
            $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY, 'video', [
                    'type' => 'varchar',
                    'backend' => '',
                    'frontend' => '',
                    'label' => 'Youtube video ID',
                    'input' => 'text',
                    'group' => 'General',
                    'class' => 'video',
                    'source' => '',
                    'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                    'visible' => true,
                    'required' => false,
                    'user_defined' => false,
                    'default' => '',
                    'searchable' => false,
                    'filterable' => false,
                    'comparable' => false,
                    'visible_on_front' => false,
                    'used_in_product_listing' => false,
                    'unique' => false
                ]
            );

            $entityTypeId = $eavSetup->getEntityTypeId(\Magento\Catalog\Model\Product::ENTITY);
            $attributeSetIds = $eavSetup->getAllAttributeSetIds($entityTypeId);
            foreach ($attributeSetIds as $attributeSetId) {
                if ($attributeSetId) {
                    $group_id = $this->config->getAttributeGroupId($attributeSetId, 'General');

                    $this->attributeManagementInterface->assign(
                        'catalog_product',
                        $attributeSetId,
                        $group_id,
                        'video',
                        999
                    );
                }
            }
        }

        if (version_compare($context->getVersion(), '1.9.0', '<')) {
            $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY, 'video', [
                    'type' => 'varchar',
                    'backend' => '',
                    'frontend' => '',
                    'label' => 'Youtube video ID',
                    'input' => 'text',
                    'group' => 'General',
                    'class' => 'video',
                    'source' => '',
                    'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                    'visible' => true,
                    'required' => false,
                    'user_defined' => false,
                    'default' => '',
                    'searchable' => false,
                    'filterable' => false,
                    'comparable' => false,
                    'visible_on_front' => false,
                    'used_in_product_listing' => false,
                    'unique' => false
                ]
            );

            $entityTypeId = $eavSetup->getEntityTypeId(\Magento\Catalog\Model\Product::ENTITY);
            $attributeSetIds = $eavSetup->getAllAttributeSetIds($entityTypeId);
            foreach ($attributeSetIds as $attributeSetId) {
                if ($attributeSetId) {
                    $group_id = $this->config->getAttributeGroupId($attributeSetId, 'General');

                    $this->attributeManagementInterface->assign(
                        'catalog_product',
                        $attributeSetId,
                        $group_id,
                        'video',
                        999
                    );
                }
            }
        }


        if (version_compare($context->getVersion(), '2.0.0', '<')) {

            $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Category::ENTITY,
                'actions_title',
                array(
                    'type' => 'varchar',
                    'label' => 'Actions title',
                    'input' => 'text',
                    'source' => '',
                    'visible' => true,
                    'backend' => '',
                    'default' => null,
                    'required' => false,
                    'sort_order' => 101,
                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                    'group' => 'General Information',
                    'used_in_product_listing' => false,
                    'visible_on_front' => false,
                    'is_used_in_grid' => true,
                    'is_visible_in_grid' => true
                ));
        }

        if (version_compare($context->getVersion(), '2.0.1', '<')) {

            $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Category::ENTITY,
                'actions_date',
                array(
                    'type' => 'varchar',
                    'label' => 'Actions Period',
                    'input' => 'text',
                    'source' => '',
                    'visible' => true,
                    'backend' => '',
                    'default' => null,
                    'required' => false,
                    'sort_order' => 102,
                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                    'group' => 'General Information',
                    'used_in_product_listing' => false,
                    'visible_on_front' => false,
                    'is_used_in_grid' => true,
                    'is_visible_in_grid' => true
                ));
        }

        $setup->endSetup();
    }
}
