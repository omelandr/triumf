<?php
namespace Triumf\Customization\Block;
class CategoriesCollection extends \Magento\Framework\View\Element\Template
{
    protected $_categoryHelper;
    protected $categoryFlatConfig;
    protected $_categoryFactory;
    protected $_categoryCollection;
    protected $_eavConfig;
    protected $_registry;
    protected $_storeManager;
    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Catalog\Helper\Category $categoryHelper
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Catalog\Helper\Category $categoryHelper,
        \Magento\Catalog\Model\Indexer\Category\Flat\State $categoryFlatState,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollection,
        \Magento\Framework\Registry $registry,
        \Magento\Eav\Model\Config $eavConfig,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->_categoryHelper = $categoryHelper;
        $this->categoryFlatConfig = $categoryFlatState;
        $this->_categoryFactory = $categoryFactory;
        $this->_categoryCollection = $categoryCollection;
        $this->_registry = $registry;
        $this->_eavConfig = $eavConfig;
        $this->_storeManager = $storeManager;
        parent::__construct($context);
    }

    public function getImageUrl($category){
        $category = $this->_categoryFactory->create()->load($category->getId());
        return $category->getImageUrl();
    }

    /**
     * Return categories helper
     */
    public function getCategoryHelper()
    {
        return $this->_categoryHelper;
    }

    public function getChildCategories()
    {
        $collection = $this->_categoryCollection->create()
            ->addAttributeToSelect('*')
            ->setStore($this->_storeManager->getStore())
            ->addAttributeToFilter('is_active','1')
            ->addAttributeToFilter('level', array('eq' => 2))
            ->addAttributeToFilter('include_in_menu','1')
            ->addAttributeToSort('position', 'asc');

        $categoriesData = array();
        if(count($collection)){
            $categoriesData = array();
            foreach ($collection as $category){
                $categoriesData[$category->getEntityId()] = $this->_categoryFactory->create()->load($category->getEntityId());
            }
        }

        return $categoriesData;
    }
}
