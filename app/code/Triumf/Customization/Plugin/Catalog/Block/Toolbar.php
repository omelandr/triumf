<?php

namespace Triumf\Customization\Plugin\Catalog\Block;

class Toolbar
{
    /**
     * Plugin
     *
     * @param \Magento\Catalog\Block\Product\ProductList\Toolbar $subject
     * @param \Closure $proceed
     * @param \Magento\Framework\Data\Collection $collection
     * @return \Magento\Catalog\Block\Product\ProductList\Toolbar
     */
    protected $_collection = null;

    public function aroundSetCollection(
        \Magento\Catalog\Block\Product\ProductList\Toolbar $subject,
        \Closure $proceed,
        $collection
    )
    {
        $currentOrder = $subject->getCurrentOrder();
        $result = $proceed($collection);

        if ($currentOrder) {
            if ($currentOrder == 'high_to_low') {
                $subject->getCollection()->setOrder('price', 'desc');
            } elseif ($currentOrder == 'low_to_high') {
                $subject->getCollection()->setOrder('price', 'asc');
            } elseif ($currentOrder == 'news_from_date') {
                $subject->getCollection()->setOrder('news_from_date', 'desc');
            } elseif ($currentOrder == 'bestsellers') {
                $collection->getSize();
                $collection->getSelect()->joinLeft(
                    'sales_order_item',
                    'e.entity_id = sales_order_item.product_id',
                    array('qty_ordered'=>'SUM(sales_order_item.qty_ordered)'))
                    ->group('e.entity_id')
                    ->order('qty_ordered desc');
            }
        }

        return $result;
    }
}
