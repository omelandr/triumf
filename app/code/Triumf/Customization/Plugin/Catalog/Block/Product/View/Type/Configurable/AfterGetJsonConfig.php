<?php

namespace Triumf\Customization\Plugin\Catalog\Block\Product\View\Type\Configurable;

use Magento\ConfigurableProduct\Block\Product\View\Type\Configurable;
use Magento\Framework\Serialize\Serializer\Json;

/**
 * Class AfterGetJsonConfig
 *
 * @package Triumf\Customization\Plugin\Catalog\Block\Product\View\Type
 */
class AfterGetJsonConfig
{

    /**
     * @var Json
     */
    protected $serializer;

    /**
     * AfterGetJsonConfig constructor.
     * @param Json $serializer
     */
    public function __construct(
        Json $serializer
    ) {
        $this->serializer = $serializer;
    }

    public function afterGetJsonConfig(Configurable $subject, $result)
    {
        $weightData = [];
        $currentProduct = $subject->getProduct();
        if (!empty($currentProduct->getWeight())) {
            $weightData['globalWeight'] = $currentProduct->getWeight();
        }
        foreach ($subject->getAllowProducts() as $child) {
            if ($child->getWeight() !== null) {
                $weightData[$child->getId()] = $child->getWeight();
            }
        }
        if (!empty($weightData)) {
            $decodedResult = $this->serializer->unserialize($result);
            $decodedResult['weightData'] = $weightData;
            $result = $this->serializer->serialize($decodedResult);
        }
        return $result;
    }
}
