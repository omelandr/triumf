<?php
namespace Triumf\Customization\Plugin\Catalog\Model;

class Config
{
    public function afterGetAttributeUsedForSortByArray(
        \Magento\Catalog\Model\Config $catalogConfig,
        $options
    )
    {
        $options['news_from_date'] = __('New');
        $options['bestsellers'] = __('Best Seller');
        $options['low_to_high'] = __('Price ascending');
        $options['high_to_low'] = __('Price descending');
        if(!empty($options['position'])){
            unset($options['position']);
        }
        return $options;
    }
}
