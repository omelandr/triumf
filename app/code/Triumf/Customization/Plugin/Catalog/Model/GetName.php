<?php

namespace Triumf\Customization\Plugin\Catalog\Model;

/**
 * Class GetName
 *
 * @package Triumf\Customization\Plugin\Catalog\Model
 */
class GetName
{
    /**
     * @param \Magento\Catalog\Api\Data\ProductInterface $productInterface
     * @param $name
     * @return mixed|string
     */
    public function afterGetName(
        \Magento\Catalog\Api\Data\ProductInterface $productInterface,
        $name
    ) {
        if ($productInterface->getRealSku() && strpos($name, $productInterface->getRealSku()) === false) {
            $name .= ' ' . $productInterface->getRealSku();
        }
        return $name;
    }
}
