<?php

namespace Triumf\Customization\Plugin\Carrier;

use Magento\Quote\Api\Data\ShippingMethodExtensionFactory;

/**
 * Class Description
 *
 */
class Description
{
    /**
     * @var ShippingMethodExtensionFactory
     */
    protected $extensionFactory;

    /**
     * Description constructor.
     * @param ShippingMethodExtensionFactory $extensionFactory
     */
    public function __construct(
        ShippingMethodExtensionFactory $extensionFactory
    )
    {
        $this->extensionFactory = $extensionFactory;
    }

    /**
     * @param $subject
     * @param $result
     * @param $rateModel
     * @return mixed
     */
    public function afterModelToDataObject($subject, $result, $rateModel)
    {
        if ("flatrate" == $result->getMethodCode()) {
            $extensionAttribute = $result->getExtensionAttributes() ?
                $result->getExtensionAttributes()
                :
                $this->extensionFactory->create();
            $extensionAttribute->setMethodDescription(__('*Адрес доставки пропишите в коментарий'));
            $result->setExtensionAttributes($extensionAttribute);
        }
        return $result;
    }
}
