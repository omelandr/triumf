<?php
namespace Triumf\Customization\Helper;

use Magento\Framework\Stdlib\DateTime\TimezoneInterface;

class Data extends \Magento\Framework\Url\Helper\Data
{
    /**
     * @var TimezoneInterface
     */
    protected $localeDate;

    public function __construct(
        TimezoneInterface $localeDate
    )
    {
        $this->localeDate = $localeDate;
    }

    public function isProductNew($product)
    {
        $newsFromDate = $product->getNewsFromDate();
        $newsToDate = $product->getNewsToDate();
        if (!$newsFromDate && !$newsToDate) {
            return false;
        }

        return $this->localeDate->isScopeDateInInterval(
            $product->getStore(),
            $newsFromDate,
            $newsToDate
        );
    }

    /**
     * @param $_product
     * @return string|null
     */
    public function displayDiscountLabel($_product)
    {

        
        $originalPrice = $_product->getPrice();
        $finalPrice = $_product->getFinalPrice();
        
        if($_product->getTypeId() == 'configurable' && $finalPrice) {
            /*$originalPrice = $_product->getMinPrice();
            foreach ($_product->getExtensionAttributes() as $attr) {
                var_dump($attr->getData());
            }*/
            $originalPrice = $_product->getPriceInfo()
                ->getPrice('regular_price')
                ->getValue();

            $finalPrice = $_product->getPriceInfo()
                ->getPrice('final_price')
                ->getValue();
        }

        if(empty($originalPrice) || empty($finalPrice)) {
            return null;
        }

        $percentage = null;
        if ($originalPrice > $finalPrice) {
            $percentage = number_format(($originalPrice - $finalPrice) * 100 / $originalPrice,0);
        }

        return $percentage ? '-'.$percentage.'%' : null;
    }
}
