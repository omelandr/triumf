<?php

/**
 * Altima Nova Poshta Extension
 *
 * Altima Web Systems.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is available through the world-wide-web at this URL:
 * https://shop.altima.net.au/tos
 * For this package used API from https://devcenter.novaposhta.ua/docs/services/
 *
 * @category   Altima
 * @package    Altima_Novaposhta
 * @author     Altima Web Systems https://altimawebsystems.com/
 * @license    https://shop.altima.net.au/tos
 * @email      support@altima.net.au
 * @copyright  Copyright (c) 2019 Altima Web Systems (https://altimawebsystems.com/)
 */

namespace Altima\Novaposhta\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallSchema implements InstallSchemaInterface
{

    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @throws \Zend_Db_Exception
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        /**
         * Create table 'altima_novaposhta_cities'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('altima_novaposhta_cities')
        )->addColumn(
            'id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'Entity Id'
        )->addColumn(
            'city_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['nullable' => false],
            'api Id'
        )
        ->addColumn(
            'city_name',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            50,
            [],
            'name of the city'
        )->addColumn(
            'city_name_ru',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            50,
            [],
            'name of the city in ukraine'
        )->addColumn(
            'ref',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            50,
            [''],
            'ref'
        )->addColumn(
            'departments_amount',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['nullable' => true],
            'departments amount'
        )->addColumn(
            'streets_amount',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['nullable' => true],
            'streets amount'
        )->setComment(
            'cities from api'
        );
        $installer->getConnection()->createTable($table);

        /**
         * Create table 'altima_novaposhta_streets'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('altima_novaposhta_streets')
        )->addColumn(
            'id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'Entity Id'
        )->addColumn(
            'city_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['nullable' => false],
            'api Id'
        )
        ->addColumn(
            'description',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'name of the street'
        )->addColumn(
            'streets_type',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            30,
            [],
            'type'
        )->addColumn(
            'streetsType_ref',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            30,
            [],
            'type ref'
        )->addColumn(
            'ref',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            50,
            [''],
            'ref'
        )->setComment(
            'streets from api'
        );
        $installer->getConnection()->createTable($table);

        /**
         * Create table 'altima_novaposhta_warehouse'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('altima_novaposhta_warehouse')
        )->addColumn(
            'id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'Entity Id'
        )->addColumn(
            'city_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['nullable' => false,],
            'api Id'
        )
        ->addColumn(
            'warehouse_name',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'name of the city'
        )->addColumn(
            'warehouse_name_ru',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'name ru'
        )->addColumn(
            'ref',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            50,
            [],
            'ref'
        )->setComment(
            'warehouses from api'
        );
        $installer->getConnection()->createTable($table);

        /**
         * Add fields to table 'quote_address'
         */
        $eavTable = $installer->getTable('quote_address');
        $columns = [
            'carrier_city_dep' => [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'nullable' => false,
                'comment' => 'Altima Novaposhta Carrier City Department',
            ],
            'carrier_department' => [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'nullable' => false,
                'comment' => 'Altima Novaposhta Carrier Department',
            ],
            'carrier_city_str' => [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'nullable' => false,
                'comment' => 'Altima Novaposhta Carrier City Street',
            ],
            'carrier_street' => [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'nullable' => false,
                'comment' => 'Altima Novaposhta Carrier Street',
            ]
        ];

        $connection = $installer->getConnection();
        foreach ($columns as $name => $definition) {
            $connection->addColumn($eavTable, $name, $definition);
        }

        $installer->endSetup();
    }

}