<?php

/**
 * Altima Nova Poshta Extension
 *
 * Altima Web Systems.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is available through the world-wide-web at this URL:
 * https://shop.altima.net.au/tos
 * For this package used API from https://devcenter.novaposhta.ua/docs/services/
 *
 * @category   Altima
 * @package    Altima_Novaposhta
 * @author     Altima Web Systems https://altimawebsystems.com/
 * @license    https://shop.altima.net.au/tos
 * @email      support@altima.net.au
 * @copyright  Copyright (c) 2019 Altima Web Systems (https://altimawebsystems.com/)
 */

namespace Altima\Novaposhta\Model;

use Magento\Framework\Api\Search\FilterGroup;
use Altima\Novaposhta\Api\CityRepositoryInterface;
use Altima\Novaposhta\Model\ResourceModel\City as CityResource;
use Altima\Novaposhta\Model\ResourceModel\City\Collection;
use Altima\Novaposhta\Model\ResourceModel\City\CollectionFactory;
use Altima\Novaposhta\Api\Data\CitySearchResultsInterfaceFactory;
use Magento\Framework\Api\SearchCriteriaInterface;
use Altima\Novaposhta\Api\Data\CityInterfaceFactory as CityDataFactory;

/**
 * CityRepository class
 */
class CityRepository implements CityRepositoryInterface
{

    /**
     * @var customResource
     */
    private $cityResource;

    /**
     * @var customFactory
     */
    private $cityFactory;

    /**
     * @var CityDataFactory
     */
    private $cityDataFactory;

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var CustomSearchResultsInterfaceFactory
     */
    private $searchResultsFactory;

    /**
     * @param CityResource $cityResource
     * @param CityFactory $cityFactory
     * @param CollectionFactory $collectionFactory
     * @param CitySearchResultsInterfaceFactory $searchResultsFactory
     * @param Collection $citycollection
     * @param CityDataFactory $cityDataFactory
     */
    public function __construct(
        CityResource $cityResource,
        CityFactory $cityFactory,
        CollectionFactory $collectionFactory,
        CitySearchResultsInterfaceFactory $searchResultsFactory,
        Collection $citycollection,
        CityDataFactory $cityDataFactory
    ) {
        $this->cityResource = $cityResource;
        $this->cityFactory = $cityFactory;
        $this->collectionFactory = $collectionFactory;
        $this->citycollection = $citycollection;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->cityDataFactory = $cityDataFactory;
    }

    /**
     * @param \Altima\Novaposhta\Api\Data\CityInterface|\Altima\Novaposta\Api\Data\CityInterface $city
     * @return int
     */
    public function save(\Altima\Novaposhta\Api\Data\CityInterface $city)
    {
        $this->cityResource->save($city);
        return $city->getId();
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return \Altima\Novaposhta\Api\Data\CitySearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        $collection = $this->cityFactory->create()->getCollection();
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);
        $this->applySearchCriteriaToCollection($searchCriteria, $collection);
        $cities = $this->convertCollectionToDataItemsArray($collection);
        $searchResults->setTotalCount($collection->getSize());
        $searchResults->setItems($cities);
        return $searchResults;
    }

    /**
     * @param Collection $collection
     * @return array
     */
    private function convertCollectionToDataItemsArray(Collection $collection)
    {
        $examples = array_map(function (City $city) {
            $dataObject = $this->cityDataFactory->create();
            $dataObject->setId($city->getId());
            $dataObject->setCityId($city->getCityId());
            $dataObject->setCityName($city->getCityName());
            $dataObject->setCityNameRu($city->getCityNameRu());
            $dataObject->setRef($city->getRef());
            return $dataObject;
        }, $collection->getItems());

        return $examples;
    }

    /**
     * @param FilterGroup $filterGroup
     * @param Collection $collection
     */
    private function addFilterGroupToCollection(FilterGroup $filterGroup, Collection $collection)
    {
        $fields = [];
        $conditions = [];
        foreach ($filterGroup->getFilters() as $filter) {
            $condition = $filter->getConditionType() ?
                $filter->getConditionType() :
                'eq';
            $fields[] = $filter->getField();
            $conditions[] = [$condition => $filter->getValue()];
        }
        if ($fields) {
            $collection->addFieldToFilter($fields, $conditions);
        }
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @param Collection $collection
     * @return void
     */
    private function applySearchCriteriaToCollection(SearchCriteriaInterface $searchCriteria, Collection $collection)
    {
        $this->applySearchCriteriaFiltersToCollection(
            $searchCriteria,
            $collection
        );
        $this->applySearchCriteriaSortOrdersToCollection(
            $searchCriteria,
            $collection
        );
        $this->applySearchCriteriaPagingToCollection(
            $searchCriteria,
            $collection
        );
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @param Collection $collection
     * @return void
     */
    private function applySearchCriteriaFiltersToCollection(SearchCriteriaInterface $searchCriteria, Collection $collection)
    {
        foreach ($searchCriteria->getFilterGroups() as $group) {
            $this->addFilterGroupToCollection($group, $collection);
        }
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @param Collection $collection
     * @return void
     */
    private function applySearchCriteriaSortOrdersToCollection(SearchCriteriaInterface $searchCriteria, Collection $collection)
    {
        $sortOrders = $searchCriteria->getSortOrders();
        if ($sortOrders) {
            $isAscending = $sortOrders->getDirection() == SearchCriteriaInterface::SORT_ASC;
            foreach ($sortOrders as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    $isAscending ? 'ASC' : 'DESC'
                );
            }
        }
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @param Collection $collection
     * @return void
     */
    private function applySearchCriteriaPagingToCollection(SearchCriteriaInterface $searchCriteria, Collection $collection)
    {
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());
    }

}