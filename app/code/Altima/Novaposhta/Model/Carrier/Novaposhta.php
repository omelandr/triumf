<?php

/**
 * Altima Nova Poshta Extension
 *
 * Altima Web Systems.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is available through the world-wide-web at this URL:
 * https://shop.altima.net.au/tos
 * For this package used API from https://devcenter.novaposhta.ua/docs/services/
 *
 * @category   Altima
 * @package    Altima_Novaposhta
 * @author     Altima Web Systems https://altimawebsystems.com/
 * @license    https://shop.altima.net.au/tos
 * @email      support@altima.net.au
 * @copyright  Copyright (c) 2019 Altima Web Systems (https://altimawebsystems.com/)
 */

namespace Altima\Novaposhta\Model\Carrier;

use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Shipping\Model\Rate\Result;
use Altima\Novaposhta\Api\WarehouseRepositoryInterface;
use Altima\Novaposhta\Api\CityRepositoryInterface;
use Magento\Framework\Locale\Resolver;
use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\SearchCriteriaBuilder;

/**
 * Novaposhta class
 */
class Novaposhta extends \Magento\Shipping\Model\Carrier\AbstractCarrier implements
    \Magento\Shipping\Model\Carrier\CarrierInterface
{

    /**
     * @var string
     */
    protected $_code = 'novaposhta';

    /**
     * @var CityRepositoryInterface
     */
    private $cityRepository;

    /**
     * @var Resolver
     */
    private $resolver;

    /**
     * @var FilterBuilder
     */
    private $filterBuilder;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var \Magento\Customer\Model\Session
     */
    private $customerSession;

    /**
     * @var \Magento\Customer\Model\Address
     */
    private  $modelAdress;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    private  $_checkoutSession;

    /**
     * @param \Magento\Customer\Model\Address $modelAdress
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Shipping\Model\Rate\ResultFactory $rateResultFactory
     * @param \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory
     * @param WarehouseRepositoryInterface $warehouseRepository
     * @param Resolver $resolver
     * @param FilterBuilder $filterBuilder
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param CityRepositoryInterface $cityRepository
     * @param \Magento\Framework\HTTP\ZendClientFactory $httpClientFactory
     * @param \Magento\Checkout\Model\Session $_checkoutSession
     * @param array $data
     */
    public function __construct(
        \Magento\Customer\Model\Address $modelAdress,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Shipping\Model\Rate\ResultFactory $rateResultFactory,
        \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory,
        WarehouseRepositoryInterface $warehouseRepository,
        Resolver $resolver,
        FilterBuilder $filterBuilder,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        CityRepositoryInterface $cityRepository,
        \Magento\Framework\HTTP\ZendClientFactory $httpClientFactory,
        \Magento\Checkout\Model\Session $_checkoutSession,
        array $data = []
    ) {
        $this->modelAdress =  $modelAdress;
        $this->customerSession = $customerSession;
        $this->_rateResultFactory = $rateResultFactory;
        $this->_rateMethodFactory = $rateMethodFactory;
        $this->warehouseRepository = $warehouseRepository;
        $this->resolver = $resolver;
        $this->filterBuilder = $filterBuilder;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->cityRepository = $cityRepository;
        $this->scopeConfig = $scopeConfig;
        $this->_httpClientFactory = $httpClientFactory;
        $this->_checkoutSession = $_checkoutSession;
        parent::__construct($scopeConfig, $rateErrorFactory, $logger, $data);
    }

    /**
     * @return array
     */
    public function getAllowedMethods()
    {
        return ['novaposhta' => $this->getConfigData('name')];
    }

    /**
     * @param RateRequest $request
     * @return bool|Result
     */
    public function collectRates(RateRequest $request)
    {
        if (!$this->getConfigFlag('active')) {
            return false;
        }

        $senderCity = $this->scopeConfig->getValue('carriers/novaposhta/citylist', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $weightUnit = $this->scopeConfig->getValue('carriers/novaposhta/weightunit', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

        $senderCityModel = $this->getCityByName($senderCity, 'UA_ua');

        $shippingWeight = $request->getPackageWeight();
        $subtotal = $request->getBaseSubtotalInclTax();

        $result = $this->_rateResultFactory->create();

        $amount = $this->_calculatePrice($senderCityModel, $this->getCityRecipient($request, 'WarehouseWarehouse'), $shippingWeight, $weightUnit, $subtotal, 'WarehouseWarehouse');
        if (is_array($amount) && !empty($amount['errorCodes'][0])) {
            $carrierData = ['methodCode' => 'novaposhta', 'methodTitle' => __('nova_poshta_delivered_department'), 'carrierTitle' => $this->_getNovaPoshtaError($amount)];
        } else {
            $carrierData = ['methodCode' => 'novaposhta', 'methodTitle' => __('nova_poshta_delivered_department'), 'carrierTitle' => __('nova_poshta_title')];
        }
        $method = $this->setShipMethod($carrierData, $amount);
        $result->append($method);

        /*$toHomeAmount = $this->_calculatePrice($senderCityModel, $this->getCityRecipient($request, 'WarehouseDoors'), $shippingWeight, $weightUnit, $subtotal, 'WarehouseDoors');
        if (is_array($toHomeAmount) && !empty($toHomeAmount['errorCodes'][0])) {
            $carrierData = ['methodCode' => 'novaposhtahome', 'methodTitle' => __('nova_poshta_delivered_home'), 'carrierTitle' => $this->_getNovaPoshtaError($toHomeAmount)];
        } else {
            $carrierData = ['methodCode' => 'novaposhtahome', 'methodTitle' => __('nova_poshta_delivered_home'), 'carrierTitle' => __('nova_poshta_title')];
        }
        $method = $this->setShipMethod($carrierData, $toHomeAmount);
        $result->append($method);*/

        return $result;
    }

    /**
     * @param RateRequest $request
     * @param string $serviceType
     * @return object
     */
    private function getCityRecipient($request, $serviceType) {
        $city = null;

        switch ($serviceType) {
            case 'WarehouseWarehouse':
                $city = $this->_checkoutSession->getQuote()->getShippingAddress()->getData('carrier_city_dep');
                break;
            case 'WarehouseDoors':
                $city = $this->_checkoutSession->getQuote()->getShippingAddress()->getData('carrier_city_str');
                break;
        }

        if (!$city) {
            $city = $request->getDestCity();
            if ($city == null) {
                $billingID = $this->customerSession->getCustomer()->getDefaultBilling();
                $address = $this->modelAdress->load($billingID);
                $data = $address->getData();
                if (isset($data['city'])) {
                    $city = $data['city'];
                } else {
                    $city = 'Киев';
                }
            }
        }

        $loc = $this->resolver->getLocale();
        if ($loc != 'ru_RU' && $loc != 'uk_UA' && $loc != 'UA_ua') {
            $loc = 'ru_RU';
        }

        $cityModel = $this->getCityByName($city, $loc);
        if (!$cityModel) {
            $cityModel = $this->getCityByName(($loc == 'uk_UA' || $loc == 'UA_ua' ? 'Київ' : 'Киев'), $loc);
        }

        return $cityModel;
    }

    /**
     * @param string $name
     * @param string $loc
     * @return string|boolean
     */
    private function getCityByName($name, $loc)
    {
        $fied_name = ($loc == 'ru_RU') ? 'city_name_ru' : 'city_name';
        $filters = $this->filterBuilder
                ->setConditionType('eq')
                ->setField($fied_name)
                ->setValue($name)
                ->create();
        $this->searchCriteriaBuilder->addFilters([$filters]);

        $city = $this->cityRepository->getList(
            $this->searchCriteriaBuilder->create()
        )->getItems();

        if (count($city) > 0) {
            reset($city);
            $first_key = key($city);
            return $city[$first_key];
        } else {
            return false;
        }
    }

    /**
     * @var $carrierData array of carrier information
     * @param $price
     * @return \Magento\Quote\Model\Quote\Address\RateResult\Method
     */
    private function setShipMethod(array $carrierData, $price)
    {
        $method = $this->_rateMethodFactory->create();
        $method->setCarrier('novaposhta');
        if ($price == 0) {
            $err = __('nova_poshta_tariff_calculation_error');
        } else {
            $err = '';
        }
        $method->setCarrierTitle($carrierData['carrierTitle']. ' '. $err);
        $method->setMethod($carrierData['methodCode']);
        $method->setMethodTitle($carrierData['methodTitle']);
        $method->setPrice($price);
        $method->setCost($price);
        return $method;
    }

    /**
     * @param Object $senderCityModel
     * @param Object $cityModel
     * @param int $shippingWeight
     * @param string $weightUnit
     * @param int $subtotal
     * @param string $serviceType
     * @return int
     */
    private function _calculatePrice($senderCityModel, $cityModel, $shippingWeight, $weightUnit, $subtotal, $serviceType)
    {
        $apiKey = $this->scopeConfig->getValue('carriers/novaposhta/apikey', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $weight = ($weightUnit == 'kg') ? $shippingWeight : $shippingWeight / 1000;
        $client = $this->_httpClientFactory->create();
        $client->setUri('https://api.novaposhta.ua/v2.0/json/');
        $request = [
            'modelName' => 'InternetDocument',
            'calledMethod' => 'getDocumentPrice',
            'apiKey' => $apiKey,
            'methodProperties' => [
                'CitySender' => $senderCityModel->getRef(),
                'CityRecipient' => $cityModel->getRef(),
                'Weight' => $weight,
                'ServiceType' => $serviceType,
                'Cost' => $subtotal
            ]
        ];

        $client->setConfig(['maxredirects' => 0, 'timeout' => 30]);
        $client->setRawData(utf8_encode(json_encode($request)));
        $response = json_decode($client->request(\Zend_Http_Client::POST)->getBody());

        if (!is_object($response)) {
            return false;
        }
        if ($response->success === true) {
            return $response->data[0]->Cost;
        } else {
            return ['errors' => $response->errors, 'errorCodes' => $response->errorCodes];
        }
    }

    /**
     * @param array $errors
     * @return string
     */
    private function _getNovaPoshtaError($errors) {
        if ($errors['errorCodes'][0] == 20000200068) {
            $errorString = __($errors['errorCodes'][0]);
            if ($errors['errorCodes'][0] == $errorString) {
                return $errors['errors'][0];
            } else {
                return $errorString;
            }
        }

        return __('nova_poshta_delivery_calculation_error');
    }

}
