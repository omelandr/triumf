<?php

/**
 * Altima Nova Poshta Extension
 *
 * Altima Web Systems.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is available through the world-wide-web at this URL:
 * https://shop.altima.net.au/tos
 * For this package used API from https://devcenter.novaposhta.ua/docs/services/
 *
 * @category   Altima
 * @package    Altima_Novaposhta
 * @author     Altima Web Systems https://altimawebsystems.com/
 * @license    https://shop.altima.net.au/tos
 * @email      support@altima.net.au
 * @copyright  Copyright (c) 2019 Altima Web Systems (https://altimawebsystems.com/)
 */

namespace Altima\Novaposhta\Model\Config\Source;

/**
 * CityList class
 */
class CityList implements \Magento\Framework\Option\ArrayInterface
{

    /**
     * {@inheritdoc}
     */
    public function toOptionArray()
    {
        return [
            [
                'value' => 'Вінниця',
                'label' => 'Вінниця',
            ],
            [
                'value' => 'Дніпро',
                'label' => 'Дніпро',
            ],
            [
                'value' => 'Київ',
                'label' => 'Київ',
            ],
            [
                'value' => 'Львів',
                'label' => 'Львів',
            ],
            [
                'value' => 'Одеса',
                'label' => 'Одеса',
            ],
            [
                'value' => 'Полтава',
                'label' => 'Полтава',
            ],
            [
                'value' => 'Харків',
                'label' => 'Харків',
            ],
            [
                'value' => 'Рубіжне',
                'label' => 'Рубіжне',
            ],

        ];
    }

}