<?php

/**
 * Altima Nova Poshta Extension
 *
 * Altima Web Systems.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is available through the world-wide-web at this URL:
 * https://shop.altima.net.au/tos
 * For this package used API from https://devcenter.novaposhta.ua/docs/services/
 *
 * @category   Altima
 * @package    Altima_Novaposhta
 * @author     Altima Web Systems https://altimawebsystems.com/
 * @license    https://shop.altima.net.au/tos
 * @email      support@altima.net.au
 * @copyright  Copyright (c) 2019 Altima Web Systems (https://altimawebsystems.com/)
 */

namespace Altima\Novaposhta\Model\Quote;

/**
 * AddressPlugin class
 */
class AddressPlugin
{
    /**
     * @param \Magento\Checkout\Model\ShippingInformationManagement $subject
     * @param int $cartId
     * @param \Magento\Checkout\Api\Data\ShippingInformationInterface $addressInformation
     */
    public function beforeSaveAddressInformation(
        \Magento\Checkout\Model\ShippingInformationManagement $subject,
        $cartId,
        \Magento\Checkout\Api\Data\ShippingInformationInterface $addressInformation
    ) {
        $shippingAddress = $addressInformation->getShippingAddress();
        $ext =  $shippingAddress->getExtensionAttributes();

        $shippingAddress->setCarrierCityDep($ext->getCarrierCityDep());
        $shippingAddress->setCarrierDepartment($ext->getCarrierDepartment());
        $shippingAddress->setCarrierCityStr($ext->getCarrierCityStr());
        $shippingAddress->setCarrierStreet($ext->getCarrierStreet());
    }

}