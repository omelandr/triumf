<?php

/**
 * Altima Nova Poshta Extension
 *
 * Altima Web Systems.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is available through the world-wide-web at this URL:
 * https://shop.altima.net.au/tos
 * For this package used API from https://devcenter.novaposhta.ua/docs/services/
 *
 * @category   Altima
 * @package    Altima_Novaposhta
 * @author     Altima Web Systems https://altimawebsystems.com/
 * @license    https://shop.altima.net.au/tos
 * @email      support@altima.net.au
 * @copyright  Copyright (c) 2019 Altima Web Systems (https://altimawebsystems.com/)
 */

namespace Altima\Novaposhta\Model;

use Altima\Novaposhta\Api\Data\StreetInterface;

/**
 * Street model
 */
class Street extends \Magento\Framework\Model\AbstractExtensibleModel implements StreetInterface
{

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Altima\Novaposhta\Model\ResourceModel\Street');
    }

    /**
     * @return array
     */
    public function getCustomAttributesCodes()
    {
        return array('id', 'description', 'streets_type', 'streetsType_ref');
    }

    /**
     * @param int $city_id
     * @return $this
     */
    public function setCityId($city_id)
    {
        return $this->setData('city_id', $city_id);
    }

    /**
     * @return int
     */
    public function getCityId()
    {
        return $this->_getData('city_id');
    }

    /**
     * @param string $description
     * @return $this
     */
    public function setDescription($description)
    {
        return $this->setData('description', $description);
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->_getData('description');
    }

    /**
     * @param string $streets_type
     * @return $this
     */
    public function setStreetsType($streets_type)
    {
        return $this->setData('streets_type', $streets_type);
    }

    /**
     * @return string
     */
    public function getStreetsType()
    {
        return $this->_getData('streets_type');
    }

    /**
     * @param string $streetsType_ref
     * @return $this
     */
    public function setStreetsTypeRef($streetsType_ref)
    {
        return $this->setData('streetsType_ref', $streetsType_ref);
    }

    /**
     * @return string
     */
    public function getStreetsTypeRef()
    {
        return $this->_getData('streetsType_ref');
    }

    /**
     * @param string $ref
     * @return $this
     */
    public function setRef($ref)
    {
        return $this->setData('ref', $ref);
    }

    /**
     * @return string
     */
    public function getRef()
    {
        return $this->_getData('ref');
    }

}