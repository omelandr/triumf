<?php

/**
 * Altima Nova Poshta Extension
 *
 * Altima Web Systems.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is available through the world-wide-web at this URL:
 * https://shop.altima.net.au/tos
 * For this package used API from https://devcenter.novaposhta.ua/docs/services/
 *
 * @category   Altima
 * @package    Altima_Novaposhta
 * @author     Altima Web Systems https://altimawebsystems.com/
 * @license    https://shop.altima.net.au/tos
 * @email      support@altima.net.au
 * @copyright  Copyright (c) 2019 Altima Web Systems (https://altimawebsystems.com/)
 */

namespace Altima\Novaposhta\Model;

use Altima\Novaposhta\Api\Data\CityInterface;

/**
 * City model
 */
class City extends \Magento\Framework\Model\AbstractExtensibleModel implements CityInterface
{

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Altima\Novaposhta\Model\ResourceModel\City');
    }

    /**
     * @return array
     */
    public function getCustomAttributesCodes()
    {
        return array('id', 'city_name', 'city_name_ru', 'areae');
    }

    /**
     * @param int $city_id
     * @return $this
     */
    public function setCityId($city_id)
    {
        return $this->setData('city_id', $city_id);
    }

    /**
     * @return int
     */
    public function getCityId()
    {
        return $this->_getData('city_id');
    }

    /**
     * @param string $city_name
     * @return $this
     */
    public function setCityName($city_name)
    {
        return $this->setData('city_name', $city_name);
    }

    /**
     * @return string
     */
    public function getCityName()
    {
        return $this->_getData('city_name');
    }

    /**
     * @param string $city_name_ru
     * @return $this
     */
    public function setCityNameRu($city_name_ru)
    {
        return $this->setData('city_name_ru', $city_name_ru);
    }

    /**
     * @return string
     */
    public function getCityNameRu()
    {
        return $this->_getData('city_name_ru');
    }

    /**
     * @param string $ref
     * @return $this
     */
    public function setRef($ref)
    {
        return $this->setData('ref', $ref);
    }

    /**
     * @return string
     */
    public function getRef()
    {
        return $this->_getData('ref');
    }

    /**
     * @param int $amount
     * @return $this
     */
    public function setDepartmentsAmount($amount)
    {
        return $this->setData('departments_amount', $amount);
    }

    /**
     * @return int
     */
    public function getDepartmentsAmount()
    {
        return $this->_getData('departments_amount');
    }

    /**
     * @param int $amount
     * @return $this
     */
    public function setStreetsAmount($amount)
    {
        return $this->setData('streets_amount', $amount);
    }

    /**
     * @return int
     */
    public function getStreetsAmount()
    {
        return $this->_getData('streets_amount');
    }

}