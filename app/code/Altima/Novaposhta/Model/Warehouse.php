<?php

/**
 * Altima Nova Poshta Extension
 *
 * Altima Web Systems.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is available through the world-wide-web at this URL:
 * https://shop.altima.net.au/tos
 * For this package used API from https://devcenter.novaposhta.ua/docs/services/
 *
 * @category   Altima
 * @package    Altima_Novaposhta
 * @author     Altima Web Systems https://altimawebsystems.com/
 * @license    https://shop.altima.net.au/tos
 * @email      support@altima.net.au
 * @copyright  Copyright (c) 2019 Altima Web Systems (https://altimawebsystems.com/)
 */

namespace Altima\Novaposhta\Model;

use Altima\Novaposhta\Api\Data\WarehouseInterface;

/**
 * Warehouse model
 */
class Warehouse extends \Magento\Framework\Model\AbstractExtensibleModel implements WarehouseInterface
{

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Altima\Novaposhta\Model\ResourceModel\Warehouse');
    }

    /**
     * @return array
     */
    public function getCustomAttributesCodes()
    {
        return array('id', 'warehouse_name', 'warehouse_name_ru', 'city_name');
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->_getData('id');
    }

    /**
     * @param int $city_id
     * @return $this
     */
    public function setCityId($city_id)
    {
        return $this->setData('city_id', $city_id);
    }

    /**
     * @return int
     */
    public function getCityId()
    {
        return $this->_getData('city_id');
    }

    /**
     * @param string $warehouse_name
     * @return $this
     */
    public function setName($name)
    {
        return $this->setData('warehouse_name', $name);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->_getData('warehouse_name');
    }

    /**
     * @param string $warehouse_name_ru
     * @return $this
     */
    public function setNameRu($warehouse_name_ru)
    {
        return $this->setData('warehouse_name_ru', $warehouse_name_ru);
    }

    /**
     * @return string
     */
    public function getNameRu()
    {
        return $this->_getData('warehouse_name_ru');
    }

    /**
     * @param string $ref
     * @return $this
     */
    public function setRef($ref)
    {
        return $this->setData('ref', $ref);
    }

    /**
     * @return string
     */
    public function getRef()
    {
        return $this->_getData('ref');
    }

}