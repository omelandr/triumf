<?php

/**
 * Altima Nova Poshta Extension
 *
 * Altima Web Systems.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is available through the world-wide-web at this URL:
 * https://shop.altima.net.au/tos
 * For this package used API from https://devcenter.novaposhta.ua/docs/services/
 *
 * @category   Altima
 * @package    Altima_Novaposhta
 * @author     Altima Web Systems https://altimawebsystems.com/
 * @license    https://shop.altima.net.au/tos
 * @email      support@altima.net.au
 * @copyright  Copyright (c) 2019 Altima Web Systems (https://altimawebsystems.com/)
 */

namespace Altima\Novaposhta\Model;

use Magento\Framework\Api\Search\FilterGroup;
use Altima\Novaposhta\Api\StreetRepositoryInterface;
use Altima\Novaposhta\Model\ResourceModel\Street as StreetResource;
use Altima\Novaposhta\Model\ResourceModel\Street\Collection;
use Altima\Novaposhta\Model\ResourceModel\Street\CollectionFactory;
use Altima\Novaposhta\Api\Data\StreetSearchResultsInterfaceFactory;
use Magento\Framework\Api\SearchCriteriaInterface;
use Altima\Novaposhta\Api\Data\StreetInterfaceFactory as StreetDataFactory;

/**
 * StreetRepository class
 */
class StreetRepository implements StreetRepositoryInterface
{

    /**
     * @var StreetResource
     */
    private $streetResource;

    /**
     * @var StreetFactory
     */
    private $streetFactory;

    /**
     * @var StreetDataFactory
     */
    private $streetDataFactory;

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var CustomSearchResultsInterfaceFactory
     */
    private $searchResultsFactory;

    /**
     * @param StreetResource $streetResource
     * @param StreetFactory $streetFactory
     * @param CollectionFactory $collectionFactory
     * @param StreetSearchResultsInterfaceFactory $searchResultsFactory
     * @param Collection $streetcollection
     * @param StreetDataFactory $streetDataFactory
     */
    public function __construct(
        StreetResource $streetResource,
        StreetFactory $streetFactory,
        CollectionFactory $collectionFactory,
        StreetSearchResultsInterfaceFactory $searchResultsFactory,
        Collection $streetcollection,
        StreetDataFactory $streetDataFactory
    ) {
        $this->streetResource = $streetResource;
        $this->streetFactory = $streetFactory;
        $this->collectionFactory = $collectionFactory;
        $this->streetcollection = $streetcollection;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->streetDataFactory = $streetDataFactory;
    }

    /**
     * @param \Altima\Novaposhta\Api\Data\StreetInterface|\Altima\Novaposta\Api\Data\StreetInterface $street
     * @return int
     */
    public function save(\Altima\Novaposhta\Api\Data\StreetInterface $street)
    {
        $this->streetResource->save($street);
        return $street->getId();
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return \Altima\Novaposhta\Api\Data\StreetSearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        $collection = $this->streetFactory->create()->getCollection();
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);
        $this->applySearchCriteriaToCollection($searchCriteria, $collection);
        $cities = $this->convertCollectionToDataItemsArray($collection);
        $searchResults->setTotalCount($collection->getSize());
        $searchResults->setItems($cities);
        return $searchResults;
    }

    /**
     * @param Collection $collection
     * @return array
     */
    private function convertCollectionToDataItemsArray(Collection $collection)
    {
        $examples = array_map(function (Street $street) {
            $dataObject = $this->streetDataFactory->create();
            $dataObject->setId($street->getId());
            $dataObject->setDescription($street->getDescription());
            $dataObject->setStreetsType($street->getStreetsType());
            $dataObject->setStreetsTypeRef($street->getStreetsTypeRef());
            $dataObject->setRef($street->getRef());
            return $dataObject;
        }, $collection->getItems());
        return $examples;
    }

    /**
     * @param FilterGroup $filterGroup
     * @param Collection $collection
     */
    private function addFilterGroupToCollection(FilterGroup $filterGroup, Collection $collection)
    {
        $fields = [];
        $conditions = [];
        foreach ($filterGroup->getFilters() as $filter) {
            $condition = $filter->getConditionType() ?
                $filter->getConditionType() :
                'eq';
            $fields[] = $filter->getField();
            $conditions[] = [$condition => $filter->getValue()];
        }
        if ($fields) {
            $collection->addFieldToFilter($fields, $conditions);
        }
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @param Collection $collection
     * @return void
     */
    private function applySearchCriteriaToCollection(SearchCriteriaInterface $searchCriteria, Collection $collection)
    {
        $this->applySearchCriteriaFiltersToCollection(
            $searchCriteria,
            $collection
        );
        $this->applySearchCriteriaSortOrdersToCollection(
            $searchCriteria,
            $collection
        );
        $this->applySearchCriteriaPagingToCollection(
            $searchCriteria,
            $collection
        );
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @param Collection $collection
     * @return void
     */
    private function applySearchCriteriaFiltersToCollection(SearchCriteriaInterface $searchCriteria, Collection $collection)
    {
        foreach ($searchCriteria->getFilterGroups() as $group) {
            $this->addFilterGroupToCollection($group, $collection);
        }
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @param Collection $collection
     * @return void
     */
    private function applySearchCriteriaSortOrdersToCollection(SearchCriteriaInterface $searchCriteria, Collection $collection)
    {
        $sortOrders = $searchCriteria->getSortOrders();
        if ($sortOrders) {
            $isAscending = $sortOrders->getDirection() == SearchCriteriaInterface::SORT_ASC;
            foreach ($sortOrders as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    $isAscending ? 'ASC' : 'DESC'
                );
            }
        }
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @param Collection $collection
     * @return void
     */
    private function applySearchCriteriaPagingToCollection(SearchCriteriaInterface $searchCriteria, Collection $collection)
    {
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());
    }

}