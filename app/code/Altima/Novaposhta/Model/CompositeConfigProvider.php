<?php

/**
 * Altima Nova Poshta Extension
 *
 * Altima Web Systems.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is available through the world-wide-web at this URL:
 * https://shop.altima.net.au/tos
 * For this package used API from https://devcenter.novaposhta.ua/docs/services/
 *
 * @category   Altima
 * @package    Altima_Novaposhta
 * @author     Altima Web Systems https://altimawebsystems.com/
 * @license    https://shop.altima.net.au/tos
 * @email      support@altima.net.au
 * @copyright  Copyright (c) 2019 Altima Web Systems (https://altimawebsystems.com/)
 */

namespace Altima\Novaposhta\Model;

use Magento\Checkout\Model\ConfigProviderInterface;

/**
 * CompositeConfigProvider class
 */
class CompositeConfigProvider implements ConfigProviderInterface
{

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;

    /**
     * @var \Altima\Novaposhta\Helper\Data
     */
    protected $_helper;

    /**
     * @var array
     */
    protected $_validMessages;

    /**
     * @param \Magento\Checkout\Model\Session $checkoutSession
     */
    public function __construct (
        \Magento\Checkout\Model\Session $checkoutSession,
        \Altima\Novaposhta\Helper\Data $helper
    ) {
        $this->_checkoutSession = $checkoutSession;
        $this->_helper          = $helper;
        $this->_validMessages['dev'] = base64_decode('PGRpdiBzdHlsZT0iYm9yZGVyOiAxcHggc29saWQgZ3JleTsgcGFkZGluZzogNXB4OyBtYXJnaW4tYm90dG9tOiA1cHg7IG1hcmdpbi10b3A6IDVweDsgdGV4dC1hbGlnbjogY2VudGVyIj5UaGlzIE5vdmFQb3NodGEgZGVsaXZlcnkgZXh0ZW5zaW9uIGlzIHJ1bm5pbmcgb24gYSBkZXZlbG9wbWVudCBzZXJpYWwuIERvIG5vdCB1c2UgdGhpcyBzZXJpYWwgZm9yIHByb2R1Y3Rpb24gZW52aXJvbm1lbnRzLjwvZGl2Pg==');
        $this->_validMessages['prd'] = str_replace('[DOMAIN]', $this->_helper->getServerName(), base64_decode('PGRpdiBzdHlsZT0iYm9yZGVyOiAzcHggc29saWQgcmVkOyBwYWRkaW5nOiA1cHg7IG1hcmdpbi1ib3R0b206IDE1cHg7IG1hcmdpbi10b3A6IDE1cHg7Ij5QbGVhc2UgZW50ZXIgYSB2YWxpZCBzZXJpYWwgZm9yIHRoZSBkb21haW4gIltET01BSU5dIiBpbiB5b3VyIGFkbWluaXN0cmF0aW9uIHBhbmVsLiBJZiB5b3UgZG9uJ3QgaGF2ZSBvbmUsIHBsZWFzZSBwdXJjaGFzZSBhIHZhbGlkIGxpY2Vuc2UgZnJvbSA8YSBocmVmPSJodHRwOi8vc2hvcC5hbHRpbWEubmV0LmF1Ij5odHRwOi8vc2hvcC5hbHRpbWEubmV0LmF1PC9hPjxicj48YnI+SWYgeW91IGhhdmUgZW50ZXJlZCBhIHZhbGlkIHNlcmlhbCBhbmQgc3RpbGwgZXhwZXJpZW5jZSBhbnkgcHJvYmxlbSBwbGVhc2Ugd3JpdGUgdG8gPGEgY2xhc3M9ImVtYWlsIiBocmVmPSJtYWlsdG86c3VwcG9ydEBhbHRpbWEubmV0LmF1Ij5zdXBwb3J0QGFsdGltYS5uZXQuYXU8L2E+PC9kaXY+'));
    }

    /**
     * @return array
     */
    public function getConfig()
    {
        $config = [];
        $quote = $this->_checkoutSession->getQuote();

        $res = $this->_isValid();
        switch ($res) {
            case 1:
                $config['is_valid'] = 1;
                $config['notvalid_text'] = '';
                break;
            case 2:
                $config['is_valid'] = 2;
                $config['notvalid_text'] = $this->_validMessages['dev'];
                break;
            default:
                $config['is_valid'] = 0;
                $config['notvalid_text'] = $this->_validMessages['prd'];
                break;
        }

        if ($quote) {
            if (!empty($quote->getShippingAddress()->getCarrierCityDep())) {
                $config['extension_attributes']['carrier_city_dep'] = $quote->getShippingAddress()->getCarrierCityDep();
            }
            if (!empty($quote->getShippingAddress()->getCarrierDepartment())) {
                $config['extension_attributes']['carrier_department'] = $quote->getShippingAddress()->getCarrierDepartment();
            }
            if (!empty($quote->getShippingAddress()->getCarrierCityStr())) {
                $config['extension_attributes']['carrier_city_str'] = $quote->getShippingAddress()->getCarrierCityStr();
            }
            if (!empty($quote->getShippingAddress()->getCarrierStreet())) {
                $config['extension_attributes']['carrier_street'] = $quote->getShippingAddress()->getCarrierStreet();
            }
        }

        return $config;
    }

    private function _isValid() {
        if ($this->_helper->isRun(false)) {
            return 1;
        }
        if ($this->_helper->isRun(true)) {
            return 2;
        }
        return 0;
    }

}
