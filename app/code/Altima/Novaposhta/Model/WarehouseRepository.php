<?php

/**
 * Altima Nova Poshta Extension
 *
 * Altima Web Systems.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is available through the world-wide-web at this URL:
 * https://shop.altima.net.au/tos
 * For this package used API from https://devcenter.novaposhta.ua/docs/services/
 *
 * @category   Altima
 * @package    Altima_Novaposhta
 * @author     Altima Web Systems https://altimawebsystems.com/
 * @license    https://shop.altima.net.au/tos
 * @email      support@altima.net.au
 * @copyright  Copyright (c) 2019 Altima Web Systems (https://altimawebsystems.com/)
 */

namespace Altima\Novaposhta\Model;

use Magento\Framework\Api\Search\FilterGroup;
use Altima\Novaposhta\Api\WarehouseRepositoryInterface;
use Altima\Novaposhta\Model\ResourceModel\Warehouse as WarehouseResource;
use Altima\Novaposhta\Model\WarehouseFactory;
use Altima\Novaposhta\Model\ResourceModel\Warehouse\CollectionFactory;
use Altima\Novaposhta\Model\ResourceModel\Warehouse\Collection;
use Magento\Framework\Api\SearchCriteriaInterface;
use Altima\Novaposhta\Api\Data\WarehouseInterfaceFactory as WarehouseDataFactory;

/**
 * WarehouseRepository class
 */
class WarehouseRepository implements WarehouseRepositoryInterface
{

    /**
     * @var WarehouseResource
     */
    private $warehouseResource;

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var \Altima\Novaposhta\Api\Data\WarehouseSearchResultsInterfaceFactory
     */
    private $searchResultsFactory;

    /**
     * @var WarehouseDataFactory
     */
    private $warehouseDataFactory;

    /**
     * @var WarehouseFactory
     */
    private $warehouseFactory;

    /**
     * @param WarehouseResource $warehouseResource
     * @param CollectionFactory $collectionFactory
     * @param WarehouseDataFactory $warehouseDataFactory
     * @param \Altima\Novaposhta\Api\Data\WarehouseSearchResultsInterfaceFactory $searchResultsFactory
     * @param WarehouseFactory $warehouseFactory
     */
    public function __construct(
        WarehouseResource $warehouseResource,
        CollectionFactory $collectionFactory,
        WarehouseDataFactory $warehouseDataFactory,
        \Altima\Novaposhta\Api\Data\WarehouseSearchResultsInterfaceFactory $searchResultsFactory,
        WarehouseFactory $warehouseFactory
    ) {
        $this->warehouseResource = $warehouseResource;
        $this->collectionFactory = $collectionFactory;
        $this->warehouseDataFactory = $warehouseDataFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->warehouseFactory = $warehouseFactory;
    }

    /**
     * @param \Altima\Novaposhta\Api\Data\WarehouseInterface $warehouse
     * @return int
     */
    public function save(\Altima\Novaposhta\Api\Data\WarehouseInterface $warehouse)
    {
        $this->warehouseResource->save($warehouse);
        return $warehouse->getId();
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return \Altima\Novaposhta\Api\Data\StreetSearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        $collection = $this->warehouseFactory->create()->getCollection();
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);
        $this->applySearchCriteriaToCollection($searchCriteria, $collection);
        $warehouses = $this->convertCollectionToDataItemsArray($collection);
        $searchResults->setTotalCount($collection->getSize());
        $searchResults->setItems($warehouses);

        return $searchResults;
    }

    /**
     * @param Collection $collection
     * @return array
     */
    private function convertCollectionToDataItemsArray(Collection $collection)
    {
        $examples = array_map(function (Warehouse $warehouse) {
            $dataObject = $this->warehouseDataFactory->create();
            $dataObject->setId($warehouse->getId());
            $dataObject->setCityId($warehouse->getCityId());
            $dataObject->setName($warehouse->getName());
            $dataObject->setNameRu($warehouse->getNameRu());
            $dataObject->setRef($warehouse->getRef());
            return $dataObject;
        }, $collection->getItems());

        return $examples;
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @param Collection $collection
     * @return void
     */
    private function applySearchCriteriaToCollection(SearchCriteriaInterface $searchCriteria, Collection $collection)
    {
        $this->applySearchCriteriaFiltersToCollection(
            $searchCriteria,
            $collection
        );
        $this->applySearchCriteriaSortOrdersToCollection(
            $searchCriteria,
            $collection
        );
        $this->applySearchCriteriaPagingToCollection(
            $searchCriteria,
            $collection
        );
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @param Collection $collection
     * @return void
     */
    private function applySearchCriteriaFiltersToCollection(SearchCriteriaInterface $searchCriteria, Collection $collection)
    {
        foreach ($searchCriteria->getFilterGroups() as $group) {
            $this->addFilterGroupToCollection($group, $collection);
        }
    }

    /**
     * @param FilterGroup $filterGroup
     * @param Collection $collection
     * @return void
     */
    private function addFilterGroupToCollection(FilterGroup $filterGroup, Collection $collection)
    {
        $fields = [];
        $conditions = [];
        foreach ($filterGroup->getFilters() as $filter) {
            $condition = $filter->getConditionType() ?
                $filter->getConditionType() :
                'eq';
            $fields[] = $filter->getField();
            $conditions[] = [$condition => $filter->getValue()];
        }
        if ($fields) {
            $collection->addFieldToFilter($fields, $conditions);
        }
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @param Collection $collection
     * @return void
     */
    private function applySearchCriteriaSortOrdersToCollection(SearchCriteriaInterface $searchCriteria, Collection $collection)
    {
        $sortOrders = $searchCriteria->getSortOrders();
        if ($sortOrders) {
            $isAscending = $sortOrders->getDirection() == SearchCriteriaInterface::SORT_ASC;
            foreach ($sortOrders as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    $isAscending ? 'ASC' : 'DESC'
                );
            }
        }
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @param Collection $collection
     * @return void
     */
    private function applySearchCriteriaPagingToCollection(SearchCriteriaInterface $searchCriteria, Collection $collection)
    {
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());
    }

}