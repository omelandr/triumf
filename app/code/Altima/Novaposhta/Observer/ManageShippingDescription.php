<?php

/**
 * Altima Nova Poshta Extension
 *
 * Altima Web Systems.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is available through the world-wide-web at this URL:
 * https://shop.altima.net.au/tos
 * For this package used API from https://devcenter.novaposhta.ua/docs/services/
 *
 * @category   Altima
 * @package    Altima_Novaposhta
 * @author     Altima Web Systems https://altimawebsystems.com/
 * @license    https://shop.altima.net.au/tos
 * @email      support@altima.net.au
 * @copyright  Copyright (c) 2019 Altima Web Systems (https://altimawebsystems.com/)
 */

namespace Altima\Novaposhta\Observer;

/**
 * ManageShippingDescription class
 */
class ManageShippingDescription implements \Magento\Framework\Event\ObserverInterface
{

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $customerSession;

    /**
     * @param \Magento\Customer\Model\Session $customerSession
     */
    public function __construct(
        \Magento\Customer\Model\Session $customerSession
    ) {
        $this->customerSession = $customerSession;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer) {
        $order = $observer->getEvent()->getOrder();
        $quote = $observer->getEvent()->getQuote();
        $shippingMethod = $order->getShippingMethod(true);

        if ($shippingMethod and $shippingMethod->getData('carrier_code') == 'novaposhta') {

            if ($shippingMethod->getData('method') == 'novaposhta') {
                // department delivery
                $description = __('nova_poshta_title') . ' - ' . __('nova_poshta_delivered_department');
                $description .= ": ";
                $description .= $quote->getShippingAddress()->getData('carrier_city_dep');
                $description .= ", ";
                $description .= $quote->getShippingAddress()->getData('carrier_department');

                $order->setData('shipping_description', $description);
            } else if ($shippingMethod->getData('method') == 'novaposhtahome') {
                // address delivery
                $description = __('nova_poshta_title') . ' - ' . __('nova_poshta_delivered_home');
                $description .= ": ";
                $description .= $quote->getShippingAddress()->getData('carrier_city_str');
                $description .= ", ";
                $description .= $quote->getShippingAddress()->getData('carrier_street');

                $order->setData('shipping_description', $description);
            }
        }

        return $this;
    }

}
