<?php

/**
 * Altima Nova Poshta Extension
 *
 * Altima Web Systems.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is available through the world-wide-web at this URL:
 * https://shop.altima.net.au/tos
 * For this package used API from https://devcenter.novaposhta.ua/docs/services/
 *
 * @category   Altima
 * @package    Altima_Novaposhta
 * @author     Altima Web Systems https://altimawebsystems.com/
 * @license    https://shop.altima.net.au/tos
 * @email      support@altima.net.au
 * @copyright  Copyright (c) 2019 Altima Web Systems (https://altimawebsystems.com/)
 */

namespace Altima\Novaposhta\Controller\Ajax;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Altima\Novaposhta\Api\CityRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;

/**
 * Cities controller
 */
class Cities extends \Magento\Framework\App\Action\Action
{

    /**
     * @var CityRepositoryInterface
     */
    private $cityRepository;

    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var \Magento\Framework\Locale\Resolver
     */
    private $resolver;

    /**
     * @var \Altima\Novaposhta\Model\CityFactory
     */
    private $_cityFactory;

    /**
     * @var \Magento\Framework\Api\Search\FilterGroupBuilder
     */
    private $_filterGroup;

    /**
     * @var \Magento\Framework\Api\FilterBuilder
     */
    private $_filterBuilder;

    /**
     * @var array
     */
    private $_priorityCity = ['киев'];

    /**
     * @param Context $context
     * @param CityRepositoryInterface $cityRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param Resolver $resolver
     * @param FilterBuilder $filterBuilder
     * @param \Altima\Novaposhta\Model\CityFactory $cityFactory
     * @param \Magento\Framework\Api\Search\FilterGroupBuilder $filterGroup
     */
    public function __construct(
        Context $context,
        CityRepositoryInterface $cityRepository,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Framework\Locale\Resolver $resolver,
        \Magento\Framework\Api\FilterBuilder $filterBuilder,
        \Altima\Novaposhta\Model\CityFactory $cityFactory,
        \Magento\Framework\Api\Search\FilterGroupBuilder $filterGroup
    ) {
        parent::__construct($context);
        $this->cityRepository = $cityRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->resolver = $resolver;
        $this->_filterGroup = $filterGroup;
        $this->_filterBuilder = $filterBuilder;
        $this->_cityFactory = $cityFactory;
    }

    /**
     * Index action
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $cities = $this->_getCitiesCollection();
        $to_json = $prt = [];
        $loc = $this->resolver->getLocale();

        foreach ($cities as $key => $city) {
            $cityName = ($loc == 'ru_RU') ? $city->getCityNameRu() : $city->getCityName();
            if (in_array(mb_strtolower($city->getCityNameRu()), $this->_priorityCity)) {
                $prt[] = ['label' => $cityName, 'value' => $key];
                continue;
            }
            $to_json[] = ['label' => $cityName, 'value' => $key];
        }

        if (!empty($prt)) {
            $to_json = array_merge($prt, $to_json);
        }

        return $this->resultJsonFactory->create()->setData(json_encode($to_json));
    }

    /**
     * Get cities collection
     *
     * @return array
     */
    protected function _getCitiesCollection()
    {
        $postData = json_decode(file_get_contents('php://input'));

        $mode = $postData->mode;
        if (!empty($mode)) {
            switch ($mode) {
                case 'streets':
                    $fieldName = 'streets_amount';
                    break;
                case 'departments':
                    $fieldName = 'departments_amount';
                    break;
                default:
                    $fieldName = null;
            }
            if ($fieldName) {
                $filters[] = $this->_filterBuilder
                    ->setConditionType('neq')
                    ->setField($fieldName)
                    ->setValue(0)
                    ->create();

                $filter_group = [
                    $this->_filterGroup
                        ->addFilter($filters[0])
                        ->create(),
                ];
                $this->searchCriteriaBuilder->setFilterGroups($filter_group);
            }
        }

        return $this->cityRepository->getList(
            $this->searchCriteriaBuilder->create()
        )->getItems();
    }

}
