<?php

/**
 * Altima Nova Poshta Extension
 *
 * Altima Web Systems.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is available through the world-wide-web at this URL:
 * https://shop.altima.net.au/tos
 * For this package used API from https://devcenter.novaposhta.ua/docs/services/
 *
 * @category   Altima
 * @package    Altima_Novaposhta
 * @author     Altima Web Systems https://altimawebsystems.com/
 * @license    https://shop.altima.net.au/tos
 * @email      support@altima.net.au
 * @copyright  Copyright (c) 2019 Altima Web Systems (https://altimawebsystems.com/)
 */

namespace Altima\Novaposhta\Api\Data;

/**
 * Interface StreetInterface
 * @api
 */
interface StreetInterface
{

    /**
     * @param int $city_id
     * @return $this
     */
    public function setCityId($city_id);

    /**
     * @param string $description
     * @return $this
     */
    public function setDescription($description);

    /**
     * @param string $streets_type
     * @return $this
     */
    public function setStreetsType($streets_type);

    /**
     * @param string $streetsType_ref
     * @return $this
     */
    public function setStreetsTypeRef($streetsType_ref);

    /**
     * @param string $ref
     * @return $this
     */
    public function setRef($ref);

}