<?php

/**
 * Altima Nova Poshta Extension
 *
 * Altima Web Systems.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is available through the world-wide-web at this URL:
 * https://shop.altima.net.au/tos
 * For this package used API from https://devcenter.novaposhta.ua/docs/services/
 *
 * @category   Altima
 * @package    Altima_Novaposhta
 * @author     Altima Web Systems https://altimawebsystems.com/
 * @license    https://shop.altima.net.au/tos
 * @email      support@altima.net.au
 * @copyright  Copyright (c) 2019 Altima Web Systems (https://altimawebsystems.com/)
 */

namespace Altima\Novaposhta\Api\Data;

/**
 * Interface WarehouseSearchResultsInterface
 * @api
 */
interface WarehouseSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * @api
     * @return \Altima\Novaposhta\Api\Data\CityInterface[]
     */
    public function getItems();

    /**
     * @api
     * @param \Altima\Novaposhta\Api\Data\CityInterface[] $items
     * @return $this
     */
    public function setItems(array $items = null);

}