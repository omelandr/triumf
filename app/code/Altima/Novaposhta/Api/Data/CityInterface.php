<?php

/**
 * Altima Nova Poshta Extension
 *
 * Altima Web Systems.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is available through the world-wide-web at this URL:
 * https://shop.altima.net.au/tos
 * For this package used API from https://devcenter.novaposhta.ua/docs/services/
 *
 * @category   Altima
 * @package    Altima_Novaposhta
 * @author     Altima Web Systems https://altimawebsystems.com/
 * @license    https://shop.altima.net.au/tos
 * @email      support@altima.net.au
 * @copyright  Copyright (c) 2019 Altima Web Systems (https://altimawebsystems.com/)
 */

namespace Altima\Novaposhta\Api\Data;

/**
 * Interface CityInterface
 * @api
 */
interface CityInterface
{

    /**
     * @param int $city_id
     * @return $this
     */
    public function setCityId($city_id);

    /**
     * @param string $city_name
     * @return $this
     */
    public function setCityName($city_name);

    /**
     * @param string $city_name_ru
     * @return $this
     */
    public function setCityNameRu($city_name_ru);

    /**
     * @param string $ref
     * @return $this
     */
    public function setRef($ref);

    /**
     * @param int $amount
     * @return $this
     */
    public function setDepartmentsAmount($amount);

    /**
     * @param int $amount
     * @return $this
     */
    public function setStreetsAmount($amount);

}