<?php

/**
 * Altima Nova Poshta Extension
 *
 * Altima Web Systems.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is available through the world-wide-web at this URL:
 * https://shop.altima.net.au/tos
 * For this package used API from https://devcenter.novaposhta.ua/docs/services/
 *
 * @category   Altima
 * @package    Altima_Novaposhta
 * @author     Altima Web Systems https://altimawebsystems.com/
 * @license    https://shop.altima.net.au/tos
 * @email      support@altima.net.au
 * @copyright  Copyright (c) 2019 Altima Web Systems (https://altimawebsystems.com/)
 */

namespace Altima\Novaposhta\Api\Data;

/**
 * Interface WarehouseInterface
 * @api
 */
interface WarehouseInterface
{

    /**
     * @param int $city_id
     * @return $this
     */
    public function setCityId($city_id);

    /**
     * @param string $warehouse_name
     * @return $this
     */
    public function setName($warehouse_name);

    /**
     * @param string $warehouse_name_ru
     * @return $this
     */
    public function setNameRu($warehouse_name_ru);

    /**
     * @param string $ref
     * @return $this
     */
    public function setRef($ref);

}