<?php

/**
 * Altima Nova Poshta Extension
 *
 * Altima Web Systems.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is available through the world-wide-web at this URL:
 * https://shop.altima.net.au/tos
 * For this package used API from https://devcenter.novaposhta.ua/docs/services/
 *
 * @category   Altima
 * @package    Altima_Novaposhta
 * @author     Altima Web Systems https://altimawebsystems.com/
 * @license    https://shop.altima.net.au/tos
 * @email      support@altima.net.au
 * @copyright  Copyright (c) 2019 Altima Web Systems (https://altimawebsystems.com/)
 */

namespace Altima\Novaposhta\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

/**
 * Interface StreetRepositoryInterface
 * @api
 */
interface StreetRepositoryInterface
{

    /**
     * @param \Altima\Novaposta\Api\Data\StreetInterface $request
     * @return int
     */
    public function save(Data\StreetInterface $request);

    /**
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Altima\Novaposhta\Api\Data\StreetSearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria);

}