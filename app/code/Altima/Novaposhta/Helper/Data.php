<?php

/**
 * Altima Nova Poshta Extension
 *
 * Altima Web Systems.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is available through the world-wide-web at this URL:
 * https://shop.altima.net.au/tos
 * For this package used API from https://devcenter.novaposhta.ua/docs/services/
 *
 * @category   Altima
 * @package    Altima_Novaposhta
 * @author     Altima Web Systems https://altimawebsystems.com/
 * @license    https://shop.altima.net.au/tos
 * @email      support@altima.net.au
 * @copyright  Copyright (c) 2019 Altima Web Systems (https://altimawebsystems.com/)
 */

namespace Altima\Novaposhta\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var \Magento\Framework\App\ProductMetadataInterface
     */
    protected $_productMetadata;

    /**
     * @var string
     */
    protected $_temp;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\ProductMetadataInterface $productMetadata
    ) {
        parent::__construct($context);
        $this->_scopeConfig     = $context->getScopeConfig();
        $this->_storeManager    = $storeManager;
        $this->_productMetadata = $productMetadata;
        $this->_temp            = $this->_scopeConfig->getValue('carriers/novaposhta/' . base64_decode('c2VyaWFs'), \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function isRun($dev = false) {
        return true;

        if (!$this->_temp) {
            return false;
        }

        $temp = trim($this->_temp);

        $m    = $temp[0];
        $temp = substr($temp, 1);
        if ($m) {
            $base_url = parse_url($this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB));
            $base_url = $base_url['host'];
        } else {
            $base_url = $this->getServerName();
        }

        if (!$dev) {
            $original = $this->_checkEntry($base_url, $temp);
        } else {
            $original = $this->_checkEntryDev($base_url, $temp);
        }

        if (!$original) {
            return false;
        }

        return true;
    }

    public function getServerName() {
        if ($host = @$_SERVER['HTTP_X_FORWARDED_HOST']) {
            $elements = explode(',', $host);
            $host = trim(end($elements));
        }else {
            if (!$host = $_SERVER['HTTP_HOST']) {
                if (!$host = $_SERVER['SERVER_NAME']) {
                    $host = !empty($_SERVER['SERVER_ADDR']) ? $_SERVER['SERVER_ADDR'] : '';
                }
            }
        }

        // Removing port number from host
        $host = preg_replace('/:\d+$/', '', $host);

        return trim($host);
    }

    private function _checkEntry($domain, $ser) {
        $edition = $this->_productMetadata->getEdition();

        if ($edition == 'Enterprise') {
            $key = sha1(base64_decode('bG9va2Jvb2tzbGlkZXJfZW50ZXJwcmlzZQ=='));
        } else {
            $key = sha1(base64_decode('YWx0aW1hbG9va2Jvb2tzbGlkZXI='));
        }

        $domain     = str_replace('www.', '', $domain);
        $www_domain = 'www.' . $domain;

        if (sha1($key . $domain) == $ser || sha1($key . $www_domain) == $ser) {
            return true;
        }

        return false;
    }

    private function _checkEntryDev($domain, $ser) {
        $key = sha1(base64_decode('YWx0aW1hbG9va2Jvb2tzbGlkZXJfZGV2'));

        $domain     = str_replace('www.', '', $domain);
        $www_domain = 'www.' . $domain;
        if (sha1($key . $domain) == $ser || sha1($key . $www_domain) == $ser) {
            return true;
        }

        return false;
    }

}
