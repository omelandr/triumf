<?php

/**
 * Altima Nova Poshta Extension
 *
 * Altima Web Systems.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is available through the world-wide-web at this URL:
 * https://shop.altima.net.au/tos
 * For this package used API from https://devcenter.novaposhta.ua/docs/services/
 *
 * @category   Altima
 * @package    Altima_Novaposhta
 * @author     Altima Web Systems https://altimawebsystems.com/
 * @license    https://shop.altima.net.au/tos
 * @email      support@altima.net.au
 * @copyright  Copyright (c) 2019 Altima Web Systems (https://altimawebsystems.com/)
 */

namespace Altima\Novaposhta\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class SyncStreets
 */
class SyncStreets extends Command
{

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $_config;

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $_objectManager;

    /**
     * @var \Altima\Novaposhta\Model\StreetFactory
     */
    private $_streetFactory;

    /**
     * @var \Altima\Novaposhta\Model\CityFactory
     */
    private $_cityFactory;

    /**
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $config
     * @param \Magento\Framework\ObjectManagerInterface $objectmanager
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $config,
        \Magento\Framework\ObjectManagerInterface $objectmanager
    ) {
        $this->_config = $config;
        $this->_objectManager = $objectmanager;
        $this->_streetFactory = $this->_objectManager->create('Altima\Novaposhta\Model\StreetFactory');
        $this->_cityFactory = $this->_objectManager->create('Altima\Novaposhta\Model\CityFactory');
        parent::__construct();
    }

    /**
     * @inheritDoc
     */
    protected function configure()
    {
        $this->setName('altima:sync_streets');
        parent::configure();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return null|int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $cities = $this->_getCities();
        if (empty($cities) || count($cities) == 0) {
            $output->writeln('<error>Please sync cities</error>');
            return;
        }

        $output->writeln('<info>Total cities: ' . count($cities) . '</info>');
        $num = 0;
        foreach ($cities as $city) {
            $output->writeln('<info>' . ++$num .'. Getting streets for city ' . $city['city_name'] . '</info>');
            $page = 1;
            $total = $updated = $inserted = 0;

            do {

                $result = $this->_getStreetsFromServer($page, $city['ref']);
                $response = json_decode($result, true);

                foreach ($response['data'] as $street) {
                    $total++;

                    $res = $this->_getStreetByRef($street['Ref']);
                    if (!empty($res)) { // update street

                        $this->_updateStreet($res['id'], [
                            'description'     => $street['Description'],
                            'streets_type'    => $street['StreetsType'],
                            'streetsType_ref' => $street['StreetsTypeRef'],
                        ]);
                        $updated++;

                    } else { // add new street

                        $this->_addStreet([
                            'city_id'         => $city['id'],
                            'description'     => $street['Description'],
                            'streets_type'    => $street['StreetsType'],
                            'streetsType_ref' => $street['StreetsTypeRef'],
                            'ref'             => $street['Ref'],
                        ]);
                        $inserted++;
                    }
                }

                $page++;

            } while(!empty($response['data']));

            $this->_updateStreetsAmountInCity($city['id'], $total);

            $output->writeln('<info>Report: streets added (' . $inserted . '), updated (' . $updated . ')</info>');
        }

        return;
    }

    /**
     * Update amount streets in city
     *
     * @param int $id
     * @param int $amount
     * @return void
     */
    private function _updateStreetsAmountInCity($id, $amount) {
        $city = $this->_cityFactory->create();
        $update = $city->load($id);
        if ($update->getId()) {
            $update->setStreetsAmount($amount);
            $update->save();
        }
    }

    /**
     * Add street
     *
     * @param array $data
     * @return void
     */
    private function _addStreet($data) {
        $street = $this->_streetFactory->create();
        $street->setCityId($data['city_id']);
        $street->setDescription($data['description']);
        $street->setStreetsType($data['streets_type']);
        $street->setStreetsTypeRef($data['streetsType_ref']);
        $street->setRef($data['ref']);
        $street->save();
    }

    /**
     * Update street
     *
     * @param int $id
     * @param array $data
     * @return void
     */
    private function _updateStreet($id, $data) {
        $street = $this->_streetFactory->create();
        $update = $street->load($id);
        $update->setDescription($data['description']);
        $update->setStreetsType($data['streets_type']);
        $update->setStreetsTypeRef($data['streetsType_ref']);
        $update->save();
    }

    /**
     * Get street by Ref
     *
     * @param string $ref
     * @return array
     */
    private function _getStreetByRef($ref) {
        $street = $this->_streetFactory->create();
        $res = $street->load($ref, 'ref');
        if (!$res->getId()) {
            return null;
        }

        return $res->getData();
    }

    /**
     * Get cities from DB
     *
     * @return array
     */
    private function _getCities() {
        $cityFactory = $this->_objectManager->create('Altima\Novaposhta\Model\CityFactory');
        $collection = $cityFactory->create()->getCollection();

        return $collection->getData();
    }

    /**
     * Get streets from novaposhta server
     *
     * @return json
     */
    private function _getStreetsFromServer($page, $ref) {
        $apiKey = $this->_config->getValue('carriers/novaposhta/apikey');
        $httpClientFactory = $this->_objectManager->create('Magento\Framework\HTTP\ZendClientFactory');

        $client = $httpClientFactory->create();
        $client->setUri('https://api.novaposhta.ua/v2.0/json/Address/getStreet');
        $request = [
            'modelName' => 'Address',
            'calledMethod' => 'getStreet',
            'methodProperties' => [
                'CityRef' => $ref,
                'Page' => $page
            ],
            'apiKey' => $apiKey
        ];
        $client->setConfig(['maxredirects' => 0, 'timeout' => 30]);
        $client->setRawData(utf8_encode(json_encode($request)));

        return $client->request(\Zend_Http_Client::POST)->getBody();
    }

}