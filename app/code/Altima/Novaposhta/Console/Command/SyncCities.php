<?php

/**
 * Altima Nova Poshta Extension
 *
 * Altima Web Systems.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is available through the world-wide-web at this URL:
 * https://shop.altima.net.au/tos
 * For this package used API from https://devcenter.novaposhta.ua/docs/services/
 *
 * @category   Altima
 * @package    Altima_Novaposhta
 * @author     Altima Web Systems https://altimawebsystems.com/
 * @license    https://shop.altima.net.au/tos
 * @email      support@altima.net.au
 * @copyright  Copyright (c) 2019 Altima Web Systems (https://altimawebsystems.com/)
 */

namespace Altima\Novaposhta\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class SyncCities
 */
class SyncCities extends Command
{

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $_config;

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $_objectManager;

    /**
     * @var \Altima\Novaposhta\Model\CityFactory
     */
    private $_cityFactory;

    /**
     * @var \Altima\Novaposhta\Api\CityRepositoryInterface
     */
    private $_cityRepository;

    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    private $_searchCriteriaBuilder;

    /**
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $config
     * @param \Magento\Framework\ObjectManagerInterface $objectmanager
     * @param \Altima\Novaposhta\Api\CityRepositoryInterface $cityRepository
     * @param \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $config,
        \Magento\Framework\ObjectManagerInterface $objectmanager,
        \Altima\Novaposhta\Api\CityRepositoryInterface $cityRepository,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
    ) {
        $this->_config = $config;
        $this->_objectManager = $objectmanager;
        $this->_cityFactory = $this->_objectManager->create('Altima\Novaposhta\Model\CityFactory');
        $this->_cityRepository = $cityRepository;
        $this->_searchCriteriaBuilder = $searchCriteriaBuilder;
        parent::__construct();
    }

    /**
     * @inheritDoc
     */
    protected function configure()
    {
        $this->setName('altima:sync_cities');
        parent::configure();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return null|int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $cities = $this->_getCitiesFromServer();
        $cities = json_decode($cities);

        if (property_exists($cities, 'success') && $cities->success === true) {

            $output->writeln('<info>Received '.$cities->info->totalCount.' cities from Novaposhta.</info>');

            $updated = $inserted = 0;
            $currentCitiesIds = $this->_getCitiesIdArray();
            foreach ($cities->data as $city) {
                $cityId = $city->CityID;
                if (isset($currentCitiesIds[$cityId])) {
                    $this->_updateCity($cityId, $city);
                    $updated++;
                } else {
                    $this->_addCity($city);
                    $inserted++;
                }
            }

            $output->writeln('<info>Report: cities added (' . $inserted . '), updated (' . $updated . ')</info>');

        } else {
            $output->writeln('<error>Sync Novaposhta cities error.</error>');
            if (!empty($cities->errors)) {
                $output->writeln('<error>Errors: ' . implode('; ', $cities->errors) . '</error>');
            }
            if (!empty($cities->errorCodes)) {
                $output->writeln('<error>Error Codes: ' . implode('; ', $cities->errorCodes) . '</error>');
            }

        }

        return;
    }

    /**
     * Add city
     *
     * @param Object $data
     * @return void
     */
    private function _addCity($data)
    {
        $modelCity = $this->_cityFactory->create();

        $modelCity->setCityId($data->CityID);
        $modelCity->setCityName($data->Description);
        $modelCity->setCityNameRu($data->DescriptionRu);
        $modelCity->setRef($data->Ref);
        $modelCity->setDepartmentsAmount(0);
        $modelCity->setStreetsAmount(0);
        $modelCity->save($modelCity);
    }

    /**
     * Update city
     *
     * @param int $id
     * @param Object $data
     * @return void
     */
    private function _updateCity($id, $data)
    {
        $city = $this->_cityFactory->create();
        $update = $city->load($id, 'city_id');
        if ($update->getId()) {
            $update->setCityName($data->Description);
            $update->setCityNameRu($data->DescriptionRu);
            $update->save();
        }
    }

    /**
     * Get cities ids
     *
     * @return array
     */
    private function _getCitiesIdArray()
    {
        $citiesCollection = $this->_getCitiesCollection();
        $idsArray = [];
        foreach ($citiesCollection as $key => $city_model) {
            $idsArray[$city_model->getCityId()] = '';
        }
        return $idsArray;
    }

    /**
     * Get cities collection
     *
     * @return array
     */
    protected function _getCitiesCollection()
    {
        return $this->_cityRepository->getList($this->_searchCriteriaBuilder->create())->getItems();
    }

    /**
     * Get cities from novaposhta server
     *
     * @return json
     */
    private function _getCitiesFromServer()
    {
        $apiKey = $this->_config->getValue('carriers/novaposhta/apikey');
        $httpClientFactory = $this->_objectManager->create('Magento\Framework\HTTP\ZendClientFactory');

        $client = $httpClientFactory->create();
        $client->setUri('https://api.novaposhta.ua/v2.0/json/Address/getCities');
        $request = [
            'modelName' => 'Address',
            'calledMethod' => 'getCities',
            'apiKey' => $apiKey
        ];
        $client->setConfig(['maxredirects' => 0, 'timeout' => 30]);
        $client->setRawData(utf8_encode(json_encode($request)));

        return $client->request(\Zend_Http_Client::POST)->getBody();
    }

}