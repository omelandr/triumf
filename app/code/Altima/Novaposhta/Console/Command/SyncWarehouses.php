<?php

/**
 * Altima Nova Poshta Extension
 *
 * Altima Web Systems.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is available through the world-wide-web at this URL:
 * https://shop.altima.net.au/tos
 * For this package used API from https://devcenter.novaposhta.ua/docs/services/
 *
 * @category   Altima
 * @package    Altima_Novaposhta
 * @author     Altima Web Systems https://altimawebsystems.com/
 * @license    https://shop.altima.net.au/tos
 * @email      support@altima.net.au
 * @copyright  Copyright (c) 2019 Altima Web Systems (https://altimawebsystems.com/)
 */

namespace Altima\Novaposhta\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class SyncWarehouses
 */
class SyncWarehouses extends Command
{

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $_config;

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $_objectManager;

    /**
     * @var \Altima\Novaposhta\Model\CityFactory
     */
    private $_cityFactory;

    /**
     * @var \Altima\Novaposhta\Api\CityRepositoryInterface
     */
    private $_cityRepository;

    /**
     * @var \Altima\Novaposhta\Model\WarehouseFactory
     */
    private $_warehouseFactory;

    /**
     * @var \Altima\Novaposhta\Api\WarehouseRepositoryInterface
     */
    private $_warehouseRepository;

    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    private $_searchCriteriaBuilder;

    /**
     * @var \Magento\Framework\Api\FilterBuilder
     */
    private $_filterBuilder;

    /**
     * @var \Magento\Framework\Api\Search\FilterGroupBuilder
     */
    private $_filterGroup;

    /**
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $config
     * @param \Magento\Framework\ObjectManagerInterface $objectmanager
     * @param \Altima\Novaposhta\Api\CityRepositoryInterface $cityRepository
     * @param \Altima\Novaposhta\Api\WarehouseRepositoryInterface $warehouseRepository
     * @param \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
     * @param \Magento\Framework\Api\FilterBuilder $filterBuilder
     * @param \Magento\Framework\Api\Search\FilterGroupBuilder $filterGroup
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $config,
        \Magento\Framework\ObjectManagerInterface $objectmanager,
        \Altima\Novaposhta\Api\CityRepositoryInterface $cityRepository,
        \Altima\Novaposhta\Api\WarehouseRepositoryInterface $warehouseRepository,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \Magento\Framework\Api\FilterBuilder $filterBuilder,
        \Magento\Framework\Api\Search\FilterGroupBuilder $filterGroup
    ) {
        $this->_config = $config;
        $this->_objectManager = $objectmanager;
        $this->_cityFactory = $this->_objectManager->create('Altima\Novaposhta\Model\CityFactory');
        $this->_cityRepository = $cityRepository;
        $this->_warehouseFactory = $this->_objectManager->create('Altima\Novaposhta\Model\WarehouseFactory');
        $this->_warehouseRepository = $warehouseRepository;
        $this->_searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->_filterBuilder = $filterBuilder;
        $this->_filterGroup = $filterGroup;
        parent::__construct();
    }

    /**
     * @inheritDoc
     */
    protected function configure()
    {
        $this->setName('altima:sync_warehouses');
        parent::configure();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return null|int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $citiesRefArray = $this->_getCitiesRefArray();
        if (count($citiesRefArray) == 0) {
            $output->writeln('<error>Cities not found. Please, sync them first.</error>');
            return;
        }

        $warehouses = $this->_getWarehousesFromServer();
        $warehouses = json_decode($warehouses);

        if (property_exists($warehouses, 'success') && $warehouses->success === true) {

            $output->writeln('<info>Received '.$warehouses->info->totalCount.' warehouses from Novaposhta.</info>');

            $updated = $inserted = 0;
            $currentWarehousesIds = $this->_getWarehousesRefArray();
            foreach ($warehouses->data as $warehouse) {
                $ref = $warehouse->Ref;
                if (isset($currentWarehousesIds[$ref])) {
                    $this->_updateWarehouse($ref, $warehouse);
                    $updated++;
                } else {
                    $cityID = !empty($citiesRefArray[$warehouse->CityRef]) ? $citiesRefArray[$warehouse->CityRef] : false;
                    if ($cityID !== false) {
                        $this->_addWarehouse($cityID, $warehouse);
                        $inserted++;
                    }
                }
            }

            $this->_updateWarehousesAmount();

            $output->writeln('<info>Report: warehouses added (' . $inserted . '), updated (' . $updated . ')</info>');

        } else {
            $output->writeln('<error>Sync Novaposhta warehouses error.</error>');
            if (!empty($warehouses->errors)) {
                $output->writeln('<error>Errors: ' . implode('; ', $warehouses->errors) . '</error>');
            }
            if (!empty($warehouses->errorCodes)) {
                $output->writeln('<error>Error Codes: ' . implode('; ', $warehouses->errorCodes) . '</error>');
            }

        }

        return;
    }

    /**
     * Update amount warehouses in city
     *
     * @return void
     */
    private function _updateWarehousesAmount() {
        $citiesCollection = $this->_getCitiesCollection();
        foreach ($citiesCollection as $city) {
            $warehousesAmount = $this->_getWarehouseAmountByCityId($city->getId());
            $this->_updateAmountInCity($city->getId(), $warehousesAmount);
        }
    }

    /**
     * Update amount streets in city
     *
     * @param int $id
     * @param int $amount
     * @return void
     */
    private function _updateAmountInCity($id, $amount) {
        $city = $this->_cityFactory->create();
        $update = $city->load($id);
        if ($update->getId()) {
            $update->setDepartmentsAmount($amount);
            $update->save();
        }
    }

    /**
     * Get amount warehouses by city id
     *
     * @param int $id
     * @return int
     */
    protected function _getWarehouseAmountByCityId($cityId) {
        $filters[] = $this->_filterBuilder
            ->setConditionType('eq')
            ->setField('main_table.city_id')
            ->setValue($cityId)
            ->create();

        $filter_group = [
            $this->_filterGroup->addFilter($filters[0])->create(),
        ];

        $res = $this->_warehouseRepository->getList(
            $this->_searchCriteriaBuilder->setFilterGroups($filter_group)->create()
        )->getItems();

        return count($res);
    }

    /**
     * Add warehouse
     *
     * @param int $cityId
     * @param Object $data
     * @return void
     */
    private function _addWarehouse($cityId, $data)
    {
        $modelWarehouse = $this->_warehouseFactory->create();

        $modelWarehouse->setCityId((int)$cityId);
        $modelWarehouse->setName($data->Description);
        $modelWarehouse->setNameRu($data->DescriptionRu);
        $modelWarehouse->setRef($data->Ref);
        $modelWarehouse->save($modelWarehouse);
    }

    /**
     * Update warehouse
     *
     * @param string $ref
     * @param Object $data
     * @return void
     */
    private function _updateWarehouse($ref, $data)
    {
        $warehouse = $this->_warehouseFactory->create();
        $update = $warehouse->load($ref, 'ref');
        if ($update->getId()) {
            $update->setName($data->Description);
            $update->setNameRu($data->DescriptionRu);
            $update->save();
        }
    }

    /**
     * Get cities ref array
     *
     * @return array
     */
    private function _getCitiesRefArray()
    {
        $citiesCollection = $this->_getCitiesCollection();
        $idsArray = [];
        foreach ($citiesCollection as $key => $city_model) {
            $idsArray[$city_model->getRef()] = $city_model->getId();
        }

        return $idsArray;
    }

    /**
     * Get warehouses ref array
     *
     * @return array
     */
    private function _getWarehousesRefArray()
    {
        $warehousesCollection = $this->_getWarehousesCollection();
        $idsArray = [];
        foreach ($warehousesCollection as $key => $model) {
            $idsArray[$model->getRef()] = '';
        }

        return $idsArray;
    }

    /**
     * Get cities collection
     *
     * @return array
     */
    protected function _getCitiesCollection()
    {
        return $this->_cityRepository->getList($this->_searchCriteriaBuilder->create())->getItems();
    }

    /**
     * Get warehouses collection
     *
     * @return array
     */
    protected function _getWarehousesCollection()
    {
        return $this->_warehouseRepository->getList($this->_searchCriteriaBuilder->create())->getItems();
    }

    /**
     * Get warehouses from novaposhta server
     *
     * @return json
     */
    protected function _getWarehousesFromServer()
    {
        $apiKey = $this->_config->getValue('carriers/novaposhta/apikey');
        $httpClientFactory = $this->_objectManager->create('Magento\Framework\HTTP\ZendClientFactory');

        $client = $httpClientFactory->create();
        $client->setUri('https://api.novaposhta.ua/v2.0/json/AddressGeneral/getWarehouses');
        $request = [
            'modelName' => 'AddressGeneral',
            'calledMethod' => 'getWarehouses',
            'apiKey' => $apiKey
        ];
        $client->setConfig(['maxredirects' => 0, 'timeout' => 0]);
        $client->setRawData(utf8_encode(json_encode($request)));
        return $client->request(\Zend_Http_Client::POST)->getBody();
    }

}