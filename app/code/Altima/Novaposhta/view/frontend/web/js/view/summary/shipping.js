/**
 * Altima Nova Poshta Extension
 *
 * Altima Web Systems.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is available through the world-wide-web at this URL:
 * https://shop.altima.net.au/tos
 * For this package used API from https://devcenter.novaposhta.ua/docs/services/
 *
 * @category   Altima
 * @package    Altima_Novaposhta
 * @author     Altima Web Systems https://altimawebsystems.com/
 * @license    https://shop.altima.net.au/tos
 * @email      support@altima.net.au
 * @copyright  Copyright (c) 2019 Altima Web Systems (https://altimawebsystems.com/)
 */

define(
    [
        'jquery',
        'Magento_Checkout/js/view/summary/abstract-total',
        'Magento_Checkout/js/model/quote',
        'mage/translate'
    ],
    function ($, Component, quote) {
        return Component.extend({
            defaults: {
                template: 'Magento_Checkout/summary/shipping'
            },
            quoteIsVirtual: quote.isVirtual(),
            totals: quote.getTotals(),
            getShippingMethodTitle: function() {
                if (!this.isCalculated()) {
                    return '';
                }

                var shippingAddress = quote.shippingAddress();
                var shippingMethod = quote.shippingMethod();
                var target = '';

                if (shippingMethod.carrier_code == 'novaposhta') {
                    var extensionAttributes = window.checkoutConfig.extension_attributes;

                    if (shippingMethod.method_code == 'novaposhta') {
                        if (shippingAddress.extension_attributes != undefined && shippingAddress.extension_attributes.carrier_department != undefined) {
                            target += ": ";
                            target += shippingAddress.extension_attributes.carrier_city_dep;
                            target += ", ";
                            target += shippingAddress.extension_attributes.carrier_department;
                        } else if (extensionAttributes && extensionAttributes.carrier_department) {
                            target += ": ";
                            target += extensionAttributes.carrier_city_dep;
                            target += ", ";
                            target += extensionAttributes.carrier_department;
                        }
                    } else if (shippingMethod.method_code == 'novaposhtahome') {
                        if (shippingAddress.extension_attributes != undefined && shippingAddress.extension_attributes.carrier_street != undefined) {
                            target += ": ";
                            target += shippingAddress.extension_attributes.carrier_city_str;
                            target += ", ";
                            target += shippingAddress.extension_attributes.carrier_street;
                        } else if (extensionAttributes && extensionAttributes.carrier_street) {
                            target += ": ";
                            target += extensionAttributes.carrier_city_str;
                            target += ", ";
                            target += extensionAttributes.carrier_street;
                        }
                    }
                }

                return shippingMethod ? shippingMethod.carrier_title + " - " + shippingMethod.method_title + target : '';
            },
            isCalculated: function() {
                return this.totals() && this.isFullMode() && null != quote.shippingMethod();
            },

            getValue: function() {
                if (!this.isCalculated()) {
                    return this.notCalculatedMessage;
                }
                var price =  this.totals().shipping_amount;
                if (0 == parseInt(price)) {
                    return $.mage.__('Freeshipping');
                } else {
                    return this.getFormattedPrice(price);
                }
            },
            /**
             * If is set coupon code, but there wasn't displayed discount view.
             *
             * @return {Boolean}
             */
            haveToShowCoupon: function () {
                var couponCode = this.totals()['coupon_code'];

                if (typeof couponCode === 'undefined') {
                    couponCode = false;
                }

                return couponCode && !discountView().isDisplayed();
            },

            /**
             * Returns coupon code description.
             *
             * @return {String}
             */
            getCouponDescription: function () {
                if (!this.haveToShowCoupon()) {
                    return '';
                }

                return '(' + this.totals()['coupon_code'] + ')';
            }
        });
    }
);
