/**
 * Altima Nova Poshta Extension
 *
 * Altima Web Systems.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is available through the world-wide-web at this URL:
 * https://shop.altima.net.au/tos
 * For this package used API from https://devcenter.novaposhta.ua/docs/services/
 *
 * @category   Altima
 * @package    Altima_Novaposhta
 * @author     Altima Web Systems https://altimawebsystems.com/
 * @license    https://shop.altima.net.au/tos
 * @email      support@altima.net.au
 * @copyright  Copyright (c) 2019 Altima Web Systems (https://altimawebsystems.com/)
 */

define([
    'Magento_Ui/js/form/element/abstract',
    'mage/url',
    'ko',
    'uiComponent',
    'Magento_Checkout/js/model/quote',
    'jquery',
    'jquery/ui',
    'Magento_Checkout/js/action/set-shipping-information',
    'Magento_Checkout/js/model/shipping-rate-service',
    'altima/jqueryselect2'
], function (Abstract, url, ko, Component, quote, $, ui, setShippingInformationAction, shippingRateService) {
    'use strict';

    var firstLoad = true;
    var currentSelectedMethod = '';
    var streets_carier_c_data;
    var streets_carier_c;
    var urlAjaxCities = '/novaposhta/ajax/cities';
    if (location.hostname === "localhost" || location.hostname === "127.0.0.1"){
        var urlAjaxCities = '/triumph/novaposhta/ajax/cities';
    }

    ko.bindingHandlers.cityHomeAutoComplete = {

        init: function (element, valueAccessor, allBindingsAccessor) {
            Promise.resolve(
                Promise.resolve(
                    $.ajax({
                        type: 'POST',
                        url: urlAjaxCities,
                        contentType: "application/json",
                        dataType: "json",
                        data: JSON.stringify({
                            mode: 'streets'
                        }),
                        success: function (result_list) {
                            var extensionAttributes = window.checkoutConfig.extension_attributes;
                            var settings = valueAccessor();
                            var selectedOption = settings.selected;
                            var options = JSON.parse(result_list);
                            var right_ar = [];
                            for (var key in options) {
                                right_ar.push(options[key]);
                                if (extensionAttributes !== undefined && extensionAttributes.carrier_city_str !== undefined && options[key]['label'] == extensionAttributes.carrier_city_str) {
                                    $(element).append('<option value="' + options[key]['value'] + '" selected="selected">' + options[key]['label'] + '</option>');
                                } else {
                                    $(element).append('<option value="' + options[key]['value'] + '">' + options[key]['label'] + '</option>');
                                }
                            }
                            $(element).trigger("chosen:updated");

                            if (!$(element).hasClass('select2-hidden-accessible')) {
                                streets_carier_c = $(element).select2();
                            }
                            if (!$('#carrier_street').hasClass('select2-hidden-accessible')) {
                                $('#carrier_street').select2();
                            }
                            options = right_ar;

                            if (extensionAttributes !== undefined && extensionAttributes.carrier_city_str !== undefined) {
                                $(element).trigger('change');
                            }
                            $(element).on('select2:select', function (e) {
                                streets_carier_c_data = e.params.data;
                            });
                            var shippingForm = $(element).parents('form.methods-shipping');
                            var selectedCheck = shippingForm.find('.table-checkout-shipping-method').find('input[type="radio"]:checked');
                            if (selectedCheck.length > 0) {
                                if (currentSelectedMethod == 'novaposhtahome') {

                                    var city_field = $('input[name="city"]');
                                    var street_fields = $('input[name*="street"]');
                                    /*if (city_field.length > 0){city_field.prop('readonly', true);}
                                    if (street_fields.length > 0){street_fields.prop('readonly', true);}*/

                                    var carrier_city_select_val = shippingForm.find('select#streets_carier_c').val();
                                    var carrier_delivery_select_val = shippingForm.find('select#carrier_street').val();
                                    if (carrier_city_select_val == '0' || carrier_delivery_select_val == '0') {
                                        shippingForm.find('button[type="submit"]').prop('disabled', true);
                                    }
                                }
                            }
                            var shippingMethods = shippingForm.find('.table-checkout-shipping-method tbody');
                            shippingMethods.on('click', 'tr.row', function () {
                                var carrier_city_select_val;
                                var carrier_delivery_select_val;
                                var city_field; var street_fields;
                                if (currentSelectedMethod == 'novaposhtahome') {
                                    city_field = $('input[name="city"]');
                                    street_fields = $('input[name*="street"]');
                                    /*if (city_field.length > 0){city_field.prop('readonly', true);}
                                    if (street_fields.length > 0){street_fields.prop('readonly', true);}*/
                                    carrier_city_select_val = shippingForm.find('select#streets_carier_c').val();
                                    carrier_delivery_select_val = shippingForm.find('select#carrier_street').val();
                                    if (carrier_city_select_val == '0'){
                                        if (city_field.length > 0){city_field.val('').trigger('keyup')}
                                    }
                                    else{
                                        if (city_field.length > 0){
                                            var selectedCityText = shippingForm.find('select#streets_carier_c option[value="'+carrier_city_select_val+'"]').text();
                                            city_field.val(selectedCityText).trigger('keyup')
                                        }
                                    }
                                    if (carrier_delivery_select_val == '0' || carrier_delivery_select_val == null){
                                        if (street_fields.length > 0){street_fields.val('').trigger('keyup')}
                                    }
                                    else{
                                        if (street_fields.length > 0) {
                                            street_fields.val('');
                                            street_fields[0].value = carrier_delivery_select_val;
                                            street_fields.trigger('keyup');
                                        }
                                    }
                                    if (carrier_city_select_val == '0' || carrier_delivery_select_val == '0' || carrier_delivery_select_val == null) {
                                        shippingForm.find('button[type="submit"]').prop('disabled', true);
                                    }
                                    else{
                                        shippingForm.find('button[type="submit"]').prop('disabled', false);
                                    }
                                }
                                else if (currentSelectedMethod == 'novaposhta'){
                                    city_field = $('input[name="city"]');
                                    street_fields = $('input[name*="street"]');
                                    /*if (city_field.length > 0){city_field.prop('readonly', true);}
                                    if (street_fields.length > 0){street_fields.prop('readonly', true);}*/
                                    carrier_city_select_val = shippingForm.find('select#carrier_city').val();
                                    carrier_delivery_select_val = shippingForm.find('select#carrier_departments').val();
                                    if (carrier_city_select_val == '0'){
                                        if (city_field.length > 0){city_field.val('').trigger('keyup')}
                                    }
                                    else{
                                        if (city_field.length > 0){
                                            var selectedCityText = shippingForm.find('select#carrier_city option[value="'+carrier_city_select_val+'"]').text();
                                            city_field.val(selectedCityText).trigger('keyup')
                                        }
                                    }
                                    if (carrier_delivery_select_val == '0' || carrier_delivery_select_val == null){
                                        if (street_fields.length > 0){street_fields.val('').trigger('keyup')}
                                    }
                                    else{
                                        if (street_fields.length > 0){
                                            street_fields.val('');
                                            street_fields[0].value = carrier_delivery_select_val;
                                            street_fields.trigger('keyup');
                                        }
                                    }
                                    if (carrier_city_select_val == '0' || carrier_delivery_select_val == '0' || carrier_delivery_select_val == null) {
                                        shippingForm.find('button[type="submit"]').prop('disabled', true);
                                    }
                                    else{
                                        shippingForm.find('button[type="submit"]').prop('disabled', false);
                                    }
                                }
                                else {
                                    city_field = $('input[name="city"]');
                                    street_fields = $('input[name*="street"]');
                                    /*if (city_field.length > 0){city_field.prop('readonly', false);}
                                    if (street_fields.length > 0){street_fields.prop('readonly', false);}*/
                                    shippingForm.find('button[type="submit"]').prop('disabled', false);
                                }
                            });
                        },
                        fail: function (xhr) {
                            alert("An error have occurred.");
                        }
                    })
                ))
        }
    };

    ko.bindingHandlers.streetAutoComplete = {

        init: function (element, valueAccessor) {
            var settings = valueAccessor();
            var selectedOption = settings.selected;
            var options = settings.options;

            var extensionAttributes = window.checkoutConfig.extension_attributes;

            var updateElementValueWithLabelSt = function (event, ui, el_val) {

                if (currentSelectedMethod == 'novaposhtahome') {

                    event.preventDefault();

                    var cityRef = $('#streets_carier_c').find("option:selected").text();
                    var selected_city_val = $('select#streets_carier_c').val();
                    selected_city_val++;
                    
                    if (typeof cityRef !== "undefined") {
                        $.ajax({
                            url: url.build('novaposhta/ajax/streets'),
                            type: "POST",
                            contentType: "application/json",
                            dataType: "json",
                            data: JSON.stringify({
                                city: cityRef
                            }),
                            success: function (result_list) {
                                var settings = valueAccessor();
                                var selectedOption = settings.selected;
                                var options = JSON.parse(result_list);
                                var right_ar = [];
                                $(element).empty();
                                for (var key in options) {
                                    right_ar.push(options[key]);
                                    if (el_val == options[key]) {
                                        $(element).append('<option value="' + options[key] + '" selected="selected">' + options[key] + '</option>');
                                    }
                                    else {
                                        $(element).append('<option value="' + options[key] + '">' + options[key] + '</option>');
                                    }
                                }
                                var selected_city_success = parseInt($('#streets_carier_c').val()) + 1;
                                if (selected_city_success != selected_city_val) {
                                    $('#streets_carier_c')
                                        .find('option[value="'+(selected_city_val-1)+'"]')
                                        .prop('selected', true);
                                    $('#streets_carier_c').trigger('change');
                                }
                                $(element).trigger("chosen:updated");

                                var street_fields = $('input[name*="street"]');
                                if ($(element).val() == '0' || $(element).val() == null) {
                                    if (street_fields.length > 0) {
                                        street_fields.val('');
                                        street_fields.attr('value', '');
                                        street_fields.each(function (index, el) {
                                            $(el).eq(index).value = '';
                                        });
                                        street_fields.trigger('keyup');
                                    }
                                }
                                else {
                                    if (street_fields.length > 0) {
                                        var current_street_text = $(element).val();
                                        street_fields.val('');
                                        street_fields.each(function (index, el) {
                                            $(el).eq(index).value = '';
                                        });
                                        street_fields.eq(0).attr('value', current_street_text);
                                        street_fields[0].value = current_street_text;
                                        street_fields.trigger('keyup');
                                    }
                                }

                                options = right_ar;
                            },
                            fail: function (xhr) {
                                alert("An error have occurred.");
                            }
                        });
                    }
                }
            };
            if (currentSelectedMethod == 'novaposhtahome') {
                $(element).trigger("chosen:updated");
            }

            $('select#streets_carier_c').on('change', function(){
                if (currentSelectedMethod == 'novaposhtahome') {
                    var shippingForm = $(this).parents('form.methods-shipping');
                    if ($(this).val() == '0') {
                        var city_field = $('input[name="city"]');
                        if (city_field.length > 0) {
                            city_field.val('').attr('value', '');
                            city_field[0].value = '';
                            city_field.trigger('keyup');
                        }
                        var street_fields = $('input[name*="street"]');
                        if (street_fields.length > 0) {
                            street_fields.val('').attr('value', '');
                            street_fields.each(function (index, el) {
                                $(el).eq(index).value = '';
                            });
                            street_fields.trigger('keyup');
                        }
                        shippingForm.find('button[type="submit"]').prop('disabled', true);
                    }
                    else {
                        var city_field = $('input[name="city"]');
                        if (city_field.length > 0) {
                            var current_city_text = $(this).find('option[value="'+parseInt($(this).val())+'"]').text();
                            city_field.val(current_city_text).attr('value', current_city_text);
                            city_field[0].value = current_city_text;
                            city_field.trigger('keyup');
                        }
                        shippingForm.find('button[type="submit"]').prop('disabled', false);
                    }
                }
            });

            $(element).on('change', function(event, ui) {
                if (currentSelectedMethod == 'novaposhtahome'){
                    if (firstLoad && extensionAttributes !== undefined && extensionAttributes.carrier_street !== undefined ){
                        var el_val = extensionAttributes.carrier_street.replace(/\"/g, '&quot;');
                        firstLoad = false;
                    }
                    else{
                        if ($(element).val()){
                            var el_val = $(element).val().replace(/\"/g, '&quot;');
                        }
                        else{
                            var el_val = '333asdzz';
                        }
                    }
                    var shippingForm = $(this).parents('form.methods-shipping');
                    var street_fields = $('input[name*="street"]');
                    if ($(element).val() == '0' || $(element).val() == null){
                        if (street_fields.length > 0){
                            street_fields.val('').attr('value', '');
                            street_fields.each(function(index, el){
                                $(el).eq(index).value = '';
                            });
                            street_fields.trigger('keyup');
                        }
                        shippingForm.find('button[type="submit"]').prop('disabled', true);
                    }
                    else {
                        if (street_fields.length > 0) {
                            var current_street_text = $(element).val();
                            street_fields.val('').attr('value', '');
                            street_fields.eq(0).attr('value', current_street_text);
                            street_fields[0].value = current_street_text;
                            street_fields.trigger('keyup');
                        }
                        shippingForm.find('button[type="submit"]').prop('disabled', false);
                    }
                    updateElementValueWithLabelSt(event, ui, el_val);
                }
            });
        }
    };

    return Abstract.extend({
        selectedCity2: ko.observable(''),
        selectedStreet: ko.observable(''),
        postCode: ko.observable(''),
        selectedMethod : ko.computed(function () {
            var method = quote.shippingMethod();
            var selectedMethod = method != null ? method.method_code : null;
            currentSelectedMethod = selectedMethod;
            return selectedMethod;
        }, this),
        getNotValidText: function () {
            var is_valid = window.checkoutConfig.is_valid;
            var notvalid_text = window.checkoutConfig.notvalid_text;

            switch (is_valid) {
                case 1:
                    $('.not_valid_block').hide();
                    $('.shipping-information-content').show();
                    break;
                case 2:
                    $('.not_valid_block').show();
                    $('.shipping-information-content').show();
                    break;
                default:
                    $('.not_valid_block').show();
                    $('.shipping-information-content').hide();
            }

            return notvalid_text;
        }
    });
});
