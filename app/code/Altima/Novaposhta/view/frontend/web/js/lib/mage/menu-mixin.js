/**
 * Altima Nova Poshta Extension
 *
 * Altima Web Systems.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is available through the world-wide-web at this URL:
 * https://shop.altima.net.au/tos
 * For this package used API from https://devcenter.novaposhta.ua/docs/services/
 *
 * @category   Altima
 * @package    Altima_Novaposhta
 * @author     Altima Web Systems https://altimawebsystems.com/
 * @license    https://shop.altima.net.au/tos
 * @email      support@altima.net.au
 * @copyright  Copyright (c) 2019 Altima Web Systems (https://altimawebsystems.com/)
 */

define([
    'jquery',
    'jquery/ui'
], function ($) {
    'use strict';

    return function (data) {

        $.widget('mage.menu', data.menu, {
            _create: function () {
                $(this.element).data('ui-menu', this);
                this._super();
            }
        });

        data.menu = $.mage.menu;

        return data;
    };
});