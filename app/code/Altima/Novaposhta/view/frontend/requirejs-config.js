/**
 * Altima Nova Poshta Extension
 *
 * Altima Web Systems.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is available through the world-wide-web at this URL:
 * https://shop.altima.net.au/tos
 * For this package used API from https://devcenter.novaposhta.ua/docs/services/
 *
 * @category   Altima
 * @package    Altima_Novaposhta
 * @author     Altima Web Systems https://altimawebsystems.com/
 * @license    https://shop.altima.net.au/tos
 * @email      support@altima.net.au
 * @copyright  Copyright (c) 2019 Altima Web Systems (https://altimawebsystems.com/)
 */

var config = {
    map: {
        '*': {
            'Magento_Checkout/js/view/summary/shipping': 'Altima_Novaposhta/js/view/summary/shipping'
        }
    },
    config: {
        mixins: {
            'mage/menu': {
                'Altima_Novaposhta/js/lib/mage/menu-mixin': true
            }
        }
    },
    paths: {
        'altima/jqueryselect2': 'Altima_Novaposhta/js/select2/select2.min',
        'altima/select2': 'Altima_Novaposhta/js/select2/init'
    }
};
