define([
    'jquery',
    'mage/utils/wrapper',
    'Amasty_Checkout/js/model/events',
    'Magento_Checkout/js/model/totals',
    'Magento_Checkout/js/model/quote'
], function ($, wrapper, events, totals, quote) {
    'use strict';

    return function (setShippingInformationAction) {
        return wrapper.wrap(setShippingInformationAction, function (original) {

            var shippingAddress = quote.shippingAddress();
            if (shippingAddress['extension_attributes'] === undefined) {
                shippingAddress['extension_attributes'] = {};
            }

            shippingAddress['extension_attributes']['carrier_city_dep']   = '';
            shippingAddress['extension_attributes']['carrier_department'] = '';
            shippingAddress['extension_attributes']['carrier_city_str']   = '';
            shippingAddress['extension_attributes']['carrier_street']     = '';

            if ($("#carrier_city").val() != 0) {
                shippingAddress['extension_attributes']['carrier_city_dep'] = $("#carrier_city option:selected").text();
            }
            if ($("#carrier_departments").val() != 0) {
                shippingAddress['extension_attributes']['carrier_department'] = $("#carrier_departments option:selected").text();
            }
            if ($("#streets_carier_c").val() != 0) {
                shippingAddress['extension_attributes']['carrier_city_str'] = $("#streets_carier_c option:selected").text();
            }
            if ($("#carrier_street").val() != 0) {
                shippingAddress['extension_attributes']['carrier_street'] = $("#carrier_street option:selected").text();
            }
            
            events.trigger('before_shipping_save');
            totals.isLoading(true);
            return original().always(function(){
                events.trigger('after_shipping_save');
                totals.isLoading(false);
            });
        });
    };
});
