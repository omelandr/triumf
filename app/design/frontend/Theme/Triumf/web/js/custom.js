requirejs([
    'jquery',
    'Magento_Ui/js/modal/modal',
    'mage/gallery/gallery',
    'mage/translate',
    "slick",
    'inputmask'

], function ($, modal, gallery, $t, slick, inputmask) {
    'use strict';

    if ($('ul.category-name-level').find('li').length > 7) {
        $('.view-home-category-custom').click(function (e) {
            e.preventDefault();
            $(this).prev().children('li:nth-child(n+7)').slideToggle({
                start: function () {
                    $(this).css('display', 'inline-block');
                }
            });
            $(this).toggleClass('opnd_g');
            if ($(this).hasClass('opnd_g')) {
                $(this).html('<span>' + $t('Hide') + '</span>');
            } else {
                $(this).html('<span>' + $t('Show All') + '</span>');
            }
        });
    } else {
        $('.view-home-category-custom').hide();
    }


    $('.products-related .products.list.items.product-items').slick({
        slidesToShow: 4,
        slidesToScroll: 4,
        infinite: true,
        dots: false,
        arrow: false,
        responsive: [
            {
                breakpoint: 1150,
                settings: {
                    slidesToShow: 4
                }
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 4
                }
            },
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    dots: false,
                    arrow: false,


                }
            },
            {
                breakpoint: 467,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    dots: false,
                    arrow: false,


                }
            }
        ]
    });


    //form feedback
    if ($('#product-contact-form').length > 0) {
        var optionsModalFeedback = {
            type: 'popup',
            responsive: true,
            innerScroll: true,
            title: $('#show-feedback-form').html(),
            buttons: [
                {
                    text: $.mage.__('Callback'),
                    class: 'send-feedback',
                    click: function () {
                        $('#product-contact-form').submit();
                        return false;
                    }
                }
            ]
        };

        $('#show-feedback-form').on('click', function () {
            $('#popup-form-request').removeClass('no-display');
            var popupFeedback = modal(optionsModalFeedback, $('#popup-form-request'));
            $('#phone_callback').inputmask("+38(099)-999-99-99").val('+38');
            $('#popup-form-request').modal('openModal');
        });

        var dataFormFeedback = $('#product-contact-form');
        var messageContainerFeedback = $('#feedbackform-message-container');
        dataFormFeedback.mage('validation', {
            submitHandler: function (form, e) {
                if (messageContainerFeedback.is(":visible")) {
                    messageContainerFeedback.slideToggle("slow", function () {
                        messageContainerFeedback.find('div.message').removeClass("error success");
                    });
                }
                dataFormFeedback.find(".send-feedback").prop('disabled', true);
                $.ajax({
                    url: form.action,
                    type: 'POST',
                    dataType: 'json',
                    data: $(form).serialize(),
                    showLoader: true,
                    context: dataFormFeedback
                }).done(function (data) {
                    var messageText = data.message;
                    var messageType = data.messageType;
                    messageContainerFeedback.show();
                    //messageContainerFeedback.find('div.message').addClass(messageType);
                    dataFormFeedback.find(".send-feedback").prop('disabled', false);
                    if (messageType == "success") {
                        // clear form
                        dataFormFeedback[0].reset();
                        $('.modal-footer').remove();
                        dataFormFeedback.remove();
                        messageContainerFeedback.find('div.message div').html('<span>' + $t('Thanks') + ' </span><span>' + messageText + '</span>');
                    } else {
                        messageContainerFeedback.find('div.message div').html(messageText);
                    }
                    $('.loading-mask').hide();

                    return true;
                });
            }
        });
    }

    //hover about
    $(".header-new-links-hover").hover(
        function () {
            $('ul.file_menu-about').stop(true, true).fadeIn('medium');
        },
        function () {
            $('ul.file_menu-about').stop(true, true).fadeOut('medium');
        }
    );


    $(document).on('gallery:loaded', function () {

        $('.fotorama').on('fotorama:ready', function (e, fotorama) {


            $('#ghg').remove();

            var info = $('<span class="info" id="ghg"/>');
            info.html((fotorama.activeIndex + 1) + ' <span class="info2"> ' + fotorama.size + '</span>');
            var rt = $(this).find('.fotorama__fullscreen-icon')

            $(this).find('.fotorama__fullscreen-icon').append(info);

        })

        /* .on('fotorama:show', function (e, fotorama, extra) {
         var info = $(this).find('.info');
         info.html((fotorama.activeIndex + 1) + ' <span class="info2"> ' + fotorama.size+'</span>');

        });*/

    });

    if (window.matchMedia('(max-width: 768px)').matches) {
        console.log('width 768');
        // category filtr mobile sort navigation

        $(".filter-options-content-sort-cls li").click(function (e) {
            debugger;
            $(".filter-options-content").removeClass("page-with-filter-pop-mob");
            $(".page-header, .filter-title strong").removeClass('page-with-filter-pop-mobs');
            $(".products.wrapper.grid.products-grid").removeClass('page-with-filter-pop-mobs-pod');
            $('body').removeClass('hidden-scrolling');
        });
        $(".sorter-label_list.filter-box-inner").click(function (e) {
            debugger;
            $(".page-header").removeClass('page-with-filter-pop-mobs');
            $(".filter-options-content").addClass('page-with-filter-pop-mob');
            $(".filter-title strong").addClass('page-with-filter-pop-mobs');
            $(".products.wrapper.grid.products-grid").addClass('page-with-filter-pop-mobs-pod');
            $('body').addClass('hidden-scrolling');
        });
    }

    // cms page diplomas
    $("#diplomas_popup img").click(function () {
        var src = $(this).attr("src");
        $(".show-diplomas").fadeIn();
        $(".img-show img").attr("src", src);
        $('body').addClass('hidden-scrolling');
    });

    $("span, .overlay").click(function () {
        $(".show-diplomas").fadeOut();
        $('body').removeClass('hidden-scrolling');
    });

    // cms page collective
    $(".image_data").click(function () {
        var image_director = $(this).find("img").attr("src");
        var director_name = $(this).find("h3").text();
        var D_name = $(this).find("h2").text();

        $('.name_employee').html(director_name);
        $('.names_director').html(D_name);
        $('#home_image img').attr('src', image_director);
        if (!$(this).hasClass('active')) {
            $(".image_data.active").removeClass("active");
            $(this).addClass("active");
        }
    });
    // color * required input
    $('.control').click(function () {
        $('.input-text', this).focus();
    })

    $('.input-text').on('input', function () {
        var len = ($(this).val()).length;
        if (len)
            $(this).next('label').hide();
        else
            $(this).next('label').show();
    })


    //inputmask popup form registration
    $('#telephone').inputmask("+38(099)-999-99-99").val('+38');

    //button next prev  pages actions
    $("#next_page").click(function () {
        if ($("span.paginator_active").next().length > 0) {
            $("span.paginator_active").removeClass("paginator_active").next("span").addClass("paginator_active");
            $(".paginator span").last().trigger("click");
        }
    });

    $("#prev_page").click(function () {
        if ($("span.paginator_active").prev().length > 0) {
            $("span.paginator_active").removeClass("paginator_active").prev("span").addClass("paginator_active");
            $(".paginator span").first().trigger("click");
        }
    });


    $(document).ajaxComplete(function () {
        let value = $("#sorter option:selected").attr('value')
        $('.sorter-label_list .items li').each(function () {
            if ($(this).attr('data-value') == value) {
                $(this).addClass('active');
            }
        });
    });

    $(document).on('click', '.sorter-label_list .items li', function () {
        let value = $(this).attr('data-value');
        $(this).addClass('active');

        $("#sorter option:selected").attr('value', value).change();

        $('.catalog-topnav.amasty-catalog-topnav').removeClass('active-mobile-filter');
        $('body').removeClass('overHidden');
    });

    //desktop menu scroll
    $("header").removeClass("menu-scroll");
    $(window).scroll(function () {
        if ($(this).scrollTop() > 0) {
            $("header").addClass("menu-scroll").fadeIn("fast");
        } else {
            $("header").removeClass("menu-scroll").fadeIn("fast");
        }
    });
});
//script in stock pages
var count = document.getElementsByClassName('num').length; //всего записей
if (count > 0) {
    var cnt = 9; //сколько отображаем сначала
    var cnt_page = Math.ceil(count / cnt); //кол-во страниц

    //выводим список страниц
    var paginator = document.querySelector(".paginator");
    var page = "";
    for (var i = 0; i < cnt_page; i++) {
        page += "<span data-page=" + i * cnt + "  id=\"page" + (i + 1) + "\">" + (i + 1) + "</span>";
    }
    paginator.innerHTML = page;

    //выводим первые записи {cnt}
    var div_num = document.querySelectorAll(".num");
    for (var i = 0; i < div_num.length; i++) {
        if (i < cnt) {
            div_num[i].style.display = "block";
        }
    }

    var main_page = document.getElementById("page1");
    main_page.classList.add("paginator_active");

    //листаем
    function pagination(event) {
        var e = event || window.event;
        var target = e.target;
        var id = target.id;

        if (target.tagName.toLowerCase() != "span") return;

        var num_ = id.substr(4);
        var data_page = +target.dataset.page;
        main_page.classList.remove("paginator_active");
        main_page = document.getElementById(id);
        main_page.classList.add("paginator_active");

        var j = 0;
        for (var i = 0; i < div_num.length; i++) {
            var data_num = div_num[i].dataset.num;
            if (data_num <= data_page || data_num >= data_page)
                div_num[i].style.display = "none";
        }

        for (var i = data_page; i < div_num.length; i++) {
            if (j >= cnt) break;
            div_num[i].style.display = "block";
            j++;
        }
    }
}
