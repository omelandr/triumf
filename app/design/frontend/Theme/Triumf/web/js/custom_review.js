require([
    'jquery',
    'Magento_Ui/js/modal/modal',
    'jquery/ui',
    'mage/translate'
], function($, modal){
    'use strict';

    $('#review-form').submit(function(e) {
        $("#review-form").validate();
        var validForm = $("#review-form").valid();
        e.preventDefault();

        if(validForm == true) {
            var dataForm = $('#review-form');
            var url = dataForm.attr('action');
            $.ajax({
                url: url,
                type: "POST",
                dataType: 'json',
                data: dataForm.serialize(),
                showLoader: true,
                success: function (data) {
                    var popup = modal(options, $('#popup-chart'));
                    $("#popup-chart").modal("openModal");
                    $("#review-form")[0].reset();
                },
                error: function () {
                    $('#popup-chart').html($.mage.__('Please try again.'));
                }
            });

            var options = {
                type: 'popup',
                responsive: true,
                innerScroll: true,
                buttons: false
            };
        }
    });
});
