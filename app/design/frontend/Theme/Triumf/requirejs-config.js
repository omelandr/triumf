var config = {
    deps: [
        "js/custom",
        "js/custom_review"
    ],
    paths: {
        'owlcarousel': "js/owlcarousel"
    },
    map: {
        '*': {
            'inputmask':   'js/jquery.inputmask.min',
            slick: 'js/slick.min'
           
        }
    },
    shim: {
        "inputmask": ["jquery"],
        'js/slick.min': { dept: ['jquery'] },
        'owlcarousel': {
            deps: ['jquery']
        }
    },
    config: {
        mixins: {
            'mage/collapsible': {
                'js/mage/collapsible-mixin': true
            }
        }
    }
};