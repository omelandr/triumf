define([
    'Magento_Ui/js/form/element/abstract',
    "uiRegistry",
    'ko',
    'mage/url',
    'mage/translate',
    'jquery',
    "Magento_Ui/js/lib/jquery.inputmask.min",
], function (Abstract, registry, ko, url, $t, $, inputmask) {
    'use strict';

    return Abstract.extend({
        initialize: function () {
            this._super();
        },
        inputMask: function (elem) {
            $(elem).inputmask({"mask": "+38(999)-999-9999"});
        }
    });
});